import asyncio
import datetime
import json
from typing import Dict, Any, List, Optional

import aiohttp
from aiohttp import client_exceptions
from aiohttp import web
from aiohttp.client import ClientTimeout

from backend.infra.base_server.app.base_server import BaseApplication
from backend.infra.base_server.app.endpoint import Endpoint
from backend.infra.base_server.config.config import ServerConfig
from backend.infra.base_server.utils.network_utils import *
from backend.infra.notifier import UpdateQueue, Notifier, Record
from backend.monitor.core.sse import SseConnection

LEVELS = ['L1', 'L2', 'L3']


async def check_connection(url: str) -> bool:
    """Функция проверки соединения сервера.

    :param url: url-адрес соединения.
    :return: установлено ли корректное соединение.
    """
    try:
        async with aiohttp.ClientSession(timeout=ClientTimeout(5)) as session:
            async with session.get(url) as r:
                return 200 <= r.status < 300
    except Exception as exc:
        print(f'Got exception when ping parser: {exc.with_traceback()}')
        return False


class Application(BaseApplication):
    """Класс, описывающий сущность сервера монитора.

    Attributes
    ----------
    _parsers : словарь "тип_парсера - словарь[данные - значение]"
    _updates : словарь "тип_парсера - очередь обновленных данных"
    _start_time : время начала работы сервера

    Methods
    -------
    __route_parser_signature(self, parser_signature: str)
        функция инициализации статуса парсера.
    routes(self)
        функция возвращает всевозможные запросы для сервера монитора.
    add_product_request(self, request: web.Request)
        функция-обработчик запроса добавления товара.
    remove_product_request(self, request: web.Request)
        функция-обработчик запроса удаления товара.
    move_product_request(self, request: web.Request)
        функция-обработчик запроса перемещения товара.
    get_all_products_handler(self, request: web.Request)
        функция-обработчик запроса получения всех товаров из монитора.
    get_parser_products_handler(self, request: web.Request)
        функция-обработчик запроса получения товаров парсера монитора.
    get_all_caches_handler(self, request: web.Request)
        функция-обработчик запроса получения всех кэшей из монитора.
    get_parser_caches_handler(self, request: web.Request)
        функция-обработчик запроса получения кэшей парсера монитора.
    get_parsers_handler(self, request: web.Request)
        функция-обработчик запроса получения парсеров монитора.
    parser_register_handler(self, request: web.Request)
        функция-обработчик запроса регистрации парсера.
    """

    _parsers: Dict[str, Dict[Any, Any]]
    _updates: Dict[str, UpdateQueue]
    _start_time: datetime.datetime
    _notifier: Notifier

    def __init__(self, *args: Any, **kwargs: Any):
        """Функция инициализации сервера монитора.

        :param args: аргументы для инициализации объекта web.Application.
        :param kwargs: именованные аргументы для инициализации объекта web.Application.
        """

        super().__init__(*args, **kwargs)
        self._parsers = dict()
        self._start_time = datetime.datetime.now()
        self._notifier = Notifier()

    async def _get_parser_routing(self, parser_signature: str) -> Optional[str]:
        """Функция инициализации статуса парсера с заданной сигнатурой.

        :param self: сервер.
        :param parser_signature: сигнатура парсера.
        :return: конфигурация, если парсер активен.
        """

        try:
            if not isinstance(parser_signature, str):
                raise KeyError('Wrong parser signature!')
            parser_info = self._parsers.get(parser_signature, None)
            if parser_info is None:
                raise KeyError('Wrong parser signature!')
            config = self._parsers[parser_signature]['config']
            url = f'http://{config.host}:{config.port}/ping'  # noqa
            connected = await check_connection(url)
            if connected:
                self._parsers[parser_signature]['status'] = ServerStatus.ACTIVE
                return f"http://{config.host}:{config.port}/"  # noqa
            else:
                self._parsers[parser_signature]['status'] = ServerStatus.DISABLED
                return None

        except ValueError as ve:
            self.logger.error(ve)
            return None
        except KeyError as ke:
            self.logger.error(ke)
            return None

    def routes(self) -> [Endpoint]:
        """Функция возвращает список всевозможных запросов для сервера монитора.

        :param self: сервер.
        :return: список запросов для сервера.
        """

        return [
            web.get('/capacity/{parser_signature}', self.get_parser_capacity_handler),
            web.post('/register', self.parser_register_handler),
            web.get('/cache', self.get_all_caches_handler),
            web.get('/cache/{parser_signature}', self.get_parser_caches_handler),
            web.post('/cache/{parser_signature}', self.add_product_request),
            web.delete('/cache/{parser_signature}', self.remove_product_request),
            web.put('/cache/{parser_signature}', self.move_product_request),
            web.get('/products', self.get_all_products_handler),
            web.get('/products/{parser_signature}', self.get_parser_products_handler),
            web.get('/updates/{parser_signature}', self.get_parser_updates_handler),
            web.post('/updates/{parser_signature}', self.add_parser_updates_handler),
            web.get('/parsers', self.get_parsers_handler)
        ]

    @query_requirements('level')
    @json_requirements('profile_name', 'name', 'url', 'sku', 'tags')
    async def add_product_request(self, request: web.Request):
        """Функция-обработчик запроса добавления продукта.

        :param self: сервер.
        :param request: запрос к странице на добавление.
        :return: ответ сервера, получилось ли добавить продукт.
        ---
        consumes:
          - application/json
        summary: Добавить product request в кэш
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
          - in: query
            name: level
            required: true
            type: string
            enum: [L1, L2, L3]
          - in: body
            name: body
            description: >
              profile_name - Название product request для пользователя \n
              name - Название продукта на сайте \n\n
              url - Ссылка на продукт на сайте \n
              sku - Артикул продукта на сайте\n
              tags - Тэги для Smart Parse \n
            required: true
            schema:
              type: object
              properties:
                profile_name:
                  type: string
                  example: Best shoes ever
                name:
                  type: string
                  example: Кеды REEBOK RESONATOR LOW
                url:
                  type: string
                  example: https://www.lamoda.ru/p/rtlabh112201/shoes-reebokclassic-kedy/
                sku:
                  type: string
                  example: rtlabh112201
                tags:
                  type: object
                  properties:
                    include:
                      type: array
                      example: ["REEBOK", "SHOES"]
                    exclude:
                      type: array
                      example: ["NIKE", "ADIDAS"]
        responses:
          200:
            description: Продукт добавлен в кэш
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """
        query = await request.json()
        try:
            async with aiohttp.ClientSession() as session:
                parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
                if parser_route is None:
                    raise Exception('Wrong parser routing!')
                add_product_request_url = parser_route + "cache"
                async with session.post(add_product_request_url,
                                        params={
                                            'level': request.query['level']},
                                        json={'profile_name': query['profile_name'],
                                              'name': query['name'],
                                              'url': query['url'],
                                              'sku': query['sku'],
                                              'tags': query['tags']}) as parser_response:
                    parser_response_text = await parser_response.text()
                    self.logger.debug(
                        f'Add product request! Got response from parser: {parser_response_text}!')
                    return web.Response(text=parser_response_text, status=200)
        except Exception as e:
            self.logger.error(
                f"Caught an exception while adding product request to parser {request.match_info['parser_signature']}")
            self.logger.error(e.__repr__())
            return web.Response(text='Error!', status=400)

    @json_requirements('id')
    async def remove_product_request(self, request: web.Request):
        """Функция-обработчик запроса удаления продукта.

        :param self: сервер.
        :param request: запрос к странице на удаление.
        :return: ответ сервера, получилось ли удалить продукт.
        ---
        consumes:
          - application/json
        summary: Удалить кэш продукта.
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
          - in: body
            name: body
            description: Id кэша продукта.
            required: true
            schema:
              type: object
              properties:
                id:
                  type: integer
                  example: 1234567890
        responses:
          200:
            description: Кэш продукта успешно удален
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        query = await request.json()
        try:
            async with aiohttp.ClientSession() as session:
                parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
                if parser_route is None:
                    raise Exception('Wrong parser routing!')
                rem_product_request_url = parser_route + "cache"
                async with session.delete(rem_product_request_url, json={'id': query['id']}) as parser_response:
                    parser_response_text = await parser_response.text()
                    self.logger.debug(
                        f'Remove product request! Got response from parser: {parser_response_text}!')
                    return web.Response(text=parser_response_text, status=200)
        except Exception as e:
            self.logger.error(
                f"Caught an exception while adding product request to parser {request.match_info['parser_signature']}")
            self.logger.error(e.__repr__())
            return web.Response(text='Error!', status=400)

    @json_requirements('id', 'to_level')
    async def move_product_request(self, request: web.Request):
        """Функция-обработчик запроса перемещения продукта.

        :param self: сервер.
        :param request: запрос к странице на перемещение.
        :return: ответ сервера, получилось ли переместить продукт.
        ---
        consumes:
          - application/json
        summary: Переместить продукт в другой кэш.
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
          - in: body
            name: body
            description: Id кэша продукта.
            required: true
            schema:
              type: object
              properties:
                id:
                  type: integer
                  example: 1234567890
                to_level:
                  type: string
                  enum: [L1, L2, L3]
                  example: L2
        responses:
          200:
            description: Продукт успешно перемещен
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        query = await request.json()
        try:
            async with aiohttp.ClientSession() as session:
                parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
                if parser_route is None:
                    raise Exception('Wrong parser routing!')
                move_product_request_url = parser_route + "cache"
                json_params = {'id': query['id'],
                               'to_level': query['to_level']}
                if 'from_level' in query:
                    json_params['from_level'] = query['from_level']
                async with session.put(move_product_request_url, json=json_params) as parser_response:
                    parser_response_text = await parser_response.text()
                    self.logger.debug(
                        f'Move product request! Got response from parser: {parser_response_text}!')
                    return web.Response(text=parser_response_text, status=200)
        except Exception as e:  # noqa
            self.logger.error(
                f"Caught an exception while adding product request to parser {request.match_info['parser_signature']}")
            self.logger.error(
                f"{str(e)}")
            return web.Response(text="Error!", status=400)

    async def get_all_products_handler(self, request: web.Request):
        """Функция-обработчик запроса получения всех продуктов в мониторе.

        :param self: сервер.
        :param request: запрос к странице на получение продуктов.
        :return: ответ сервера - список продуктов в JSON формате.
                ---
        consumes:
          - application/json
        summary: Получить все продукты.
        responses:
          200:
            description: Продукты успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        all_products: List[Dict[str, Any]] = []
        self.logger.debug("Trying to get all products!")
        for parser_signature, parser_info in self._parsers.items():
            if parser_info.get('config', None) is None:
                continue
            ping_url = f"http://{parser_info['config']}/ping"
            url = f"http://{parser_info['config']}/products"
            connected = check_connection(ping_url)
            if not connected:
                continue
            try:
                self.logger.debug(
                    f"Trying to get products of {parser_info['config']}!")
                async with aiohttp.ClientSession() as session:
                    async with session.get(url) as parser_response:
                        parser_response_text = await parser_response.json()
                        self.logger.debug(
                            f'Got response from parser {parser_response_text}!')
                        all_products.append({'parser_signature': parser_signature,
                                             'content': parser_response_text
                                             })
            except client_exceptions.ClientConnectionError:
                self.logger.error(
                    f"Lost connection with {parser_info['config']}!")
        return web.json_response(data=all_products, status=200)

    async def get_parser_products_handler(self, request: web.Request):
        """Функция-обработчик запроса получения продуктов конкретного парсера монитора.

        :param self: сервер.
        :param request: запрос к странице на получение продуктов парсера.
        :return: ответ сервера - список продуктов в JSON формате.
        ---
        consumes:
          - application/json
        summary: Получить все продукты для определенного парсера.
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
        responses:
          200:
            description: Продукты успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        try:
            self.logger.debug('Extracting parser id!')
            parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
            if parser_route is None:
                raise KeyError(
                    'No parser with such an id found! Probably its not registered yet.')
            url = parser_route + "products"
            async with aiohttp.ClientSession() as session:
                async with session.get(url) as parser_response:
                    parser_response_text = await parser_response.json()
                    self.logger.debug(f'Got response from parser {parser_response_text}!')
                    result = {'parser_signature': request.match_info['parser_signature'],
                              'content': parser_response_text
                              }
                    return web.json_response(data=result, status=200)
        except ValueError:
            response_text = 'Wrong parser id parameter!'
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)
        except KeyError as ke:
            response_text = str(ke)
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)

    async def get_all_caches_handler(self, request: web.Request):
        """Функция-обработчик запроса получения всех кэшей монитора.

        :param self: сервер.
        :param request: запрос к странице на получение кэшей.
        :return: ответ сервера - список кэшей.
        ---
        consumes:
          - application/json
        summary: Получить все кэши.
        responses:
          200:
            description: Кэши успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        results = []

        for parser_signature, parser_info in self._parsers.items():
            if parser_info.get('config', None) is None:
                continue
            parser_cache_url = f"http://{parser_info['config']}/cache"
            ping_url = f"http://{parser_info['config']}/ping"
            connected = check_connection(ping_url)
            if not connected:
                continue
                
            async with aiohttp.ClientSession() as session:
                async with session.get(parser_cache_url) as parser_response:
                    parser_response_text = await parser_response.json()
                    self.logger.debug(
                        f'Got response from parser {parser_response_text}!')
                    results.append({'parser_signature': parser_signature,
                                    'content': parser_response_text
                                    })
        return web.json_response(data=results, status=200)

    async def get_parser_updates_handler(self, request: web.Request):
        """
        ---
        summary: открыть SSE соединение для получения событий из парсеров.
        consumes:
          - application/text
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
        responses:
          200:
            description: Кэши успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """
        try:
            self.logger.debug("Extracting parser id!")
            parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
            if parser_route is None:
                raise KeyError("No parser with such an id found! Probably its not registered yet.")
            queue = UpdateQueue()
            self._notifier.subscribe(request.match_info['parser_signature'], queue)
            async with SseConnection(request,
                                     on_disconnect=self._notifier.unsubscribe,
                                     args=(request.match_info['parser_signature'], queue)) as sse:
                while True:
                    if queue.has_updates:
                        msg = queue.call()
                        data = f'{msg}'
                        await sse.send(data)
                    await asyncio.sleep(0.1)
        except ValueError:
            response_text = "Wrong parser id parameter!"
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)
        except KeyError as ke:
            response_text = str(ke)
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)

    async def add_parser_updates_handler(self, request: web.Request):
        """
        ---
        summary: отправить событие в монитор.
        consumes:
          - application/text
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
        responses:
          200:
            description: Кэши успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """
        try:
            self.logger.debug("Extracting parser id!")
            parser_signature = request.match_info['parser_signature']
            parser_route = await self._get_parser_routing(parser_signature)
            if parser_route is None:
                raise KeyError("No parser with such an id found! Probably its not registered yet :).")
            json_query = await request.json()
            record = Record(json.dumps(json_query))
            self._notifier.send(request.match_info['parser_signature'], record)
            return web.Response(text="OK!", status=200)
        except ValueError:
            response_text = "Wrong parser id parameter!"
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)
        except KeyError as ke:
            response_text = str(ke)
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)

    async def get_parser_caches_handler(self, request: web.Request):
        """Функция-обработчик запроса получения всех кэшей конкретного парсера монитора.

        :param self: сервер.
        :param request: запрос к странице на получение кэшей.
        :return: ответ сервера - список кэшей парсера.
        ---
        consumes:
          - application/json
        summary: Получить все кэши для определенного парсера.
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
        responses:
          200:
            description: Кэши успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        try:
            self.logger.debug('Extracting parser id!')
            parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
            if parser_route is None:
                raise KeyError('No parser with such an id found! Probably its not registered yet.')
            parser_cache_url = parser_route + "cache"
            async with aiohttp.ClientSession() as session:
                async with session.get(parser_cache_url) as parser_response:
                    parser_response_text = await parser_response.json()
                    self.logger.debug(f'Got response from parser {parser_response_text}!')
                    result = {
                        'parser_signature': request.match_info['parser_signature'],
                        'content': parser_response_text
                    }
                    return web.json_response(data=result, status=200)
        except ValueError:
            response_text = 'Wrong parser id parameter!'
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)
        except KeyError as ke:
            response_text = str(ke)
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)

    async def get_parser_capacity_handler(self, request: web.Request):
        """Функция-обработчик запроса получения вместимости кешей парсера.

        :param self: сервер.
        :param request: запрос к серверу на получение парсеров монитора.
        :return: ответ сервера - словарь парсеров в JSON формате.
        ---
        consumes:
          - application/json
        summary: Получить вместимость кэша парсера.
        parameters:
          - in: path
            name: parser_signature
            required: true
            type: string
            example: lamoda
        responses:
          200:
            description: Вместимости кэшей успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """
        try:
            parser_route = await self._get_parser_routing(request.match_info['parser_signature'])
            if parser_route is None:
                raise KeyError('No parser with such an id found! Probably its not registered yet.')
            parser_cache_url = parser_route + "capacity"
            async with aiohttp.ClientSession() as session:
                async with session.get(parser_cache_url) as parser_response:
                    parser_response = await parser_response.json()
                    return web.json_response(data=parser_response, status=200)
        except ValueError:
            response_text = 'Wrong parser id parameter!'
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)
        except KeyError as ke:
            response_text = str(ke)
            self.logger.error(response_text)
            return web.Response(text=response_text, status=400)

    async def get_parsers_handler(self, request: web.Request):
        """Функция-обработчик запроса получения всех парсеров монитора.

        :param self: сервер.
        :param request: запрос к серверу на получение парсеров монитора.
        :return: ответ сервера - словарь парсеров в JSON формате.
        ---
        consumes:
          - application/json
        summary: Получить все парсеры монитора.
        responses:
          200:
            description: Парсеры успешно получены
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        for parser_signature in self._parsers.keys():
            url = f'http://{self._parsers[parser_signature]["config"]}/ping'
            state = await check_connection(url)
            self._parsers[parser_signature]['status'] = ServerStatus.ACTIVE if state else ServerStatus.DISABLED
        return web.json_response(
            data={key: str(value) for (key, value) in self._parsers.items()}, status=200)

    @json_requirements('host', 'port', 'signature')
    async def parser_register_handler(self, request: web.Request) -> web.Response:
        """Функция-обработчик запроса регистрации парсера в мониторе.

        :param self: сервер.
        :param request: запрос к серверу на регитсрацию парсера.
        :return: ответ сервера - удалось ли зарегистрировать парсер в мониторе.
        ---
        consumes:
          - application/json
        summary: Добавить product request в кэш
        parameters:
          - in: body
            name: body
            description: >
              host - Адрес парсера \n
              port - Порт приложения \n
              signature - Имя парсера \n
            required: true
            schema:
              type: object
              properties:
                host:
                  type: string
                  example: 0.0.0.0
                port:
                  type: string
                  example: 9001
                signature:
                  type: string
                  example: basketsshop_test
        responses:
          200:
            description: Продукт добавлен в кэш
          400:
            description: Некорректный ввод
          500:
            description: Произошла внутренняя ошибка
        """

        json_query = await request.json()
        status_code = 200
        try:

            host = json_query["host"]
            port = int(json_query["port"])
            if not isinstance(json_query["signature"], str):
                raise AttributeError("Parser signature was not string")
            signature = json_query["signature"]
            self._parsers[signature] = {"config": ServerConfig(host=host, port=port),
                                        "status": ServerStatus.ACTIVE}
            self.logger.debug(f"Got register request from port: {port}! Type: {signature}.")
            response = 'OK'
            # self._notifier.subscribe(signature, UpdateQueue())
        except ValueError:
            response = f"Incorrect value! Port: {json_query['port']}. Type: {json_query['type']}."
            self.logger.error(response)
            status_code = 400
        except AttributeError as ae:
            response = str(ae)
            status_code = 400
        self.logger.debug(
            f'Sending response: {response}! Status: {status_code}.')
        return web.Response(text=response, status=status_code)
