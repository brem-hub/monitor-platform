from asyncio import CancelledError
import uuid
from typing import Callable, Any, Tuple
from aiohttp import web
from aiohttp_sse import sse_response


class SseConnection(object):
    """Класс sse соединения.

        Attributes
        ----------
        _connection_id идентификатор соединения.
        _request запрос.
        _context_manager контекстный менеджер.
        _on_disconnect функция, которая будет выполнена при отключении sse соединения.
        _on_disconnect_args аргументы, которые будут переданы _on_disconnect

        Methods
        -------
        _disconnect(self)
            вызвать _on_disconnect с _on_disconnect_args в качестве аргументов.
        __aenter__
            вызывается при входе в контекстный менеджер.
        __aexit__
            вызывается при выходе из контекстного менеджера.
    """
    _connection_id: int
    _request: web.Request
    _context_manager: Any = None
    _on_disconnect: Callable = None
    _on_disconnect_args: Tuple = tuple()

    def __init__(self, request: web.Request, on_disconnect: Callable = None, args: Tuple = None):
        self._request = request
        self._connection_id = uuid.uuid4().int
        if on_disconnect is not None:
            self._on_disconnect = on_disconnect
            if args:
                self._on_disconnect_args = args

    async def _disconnect(self):
        """
            Вызывает _on_disconnect с _on_disconnect_args в качестве аргументов.
        """
        if self._on_disconnect:
            self._on_disconnect(*self._on_disconnect_args)

    async def __aenter__(self):
        self._context_manager = await sse_response(self._request)
        return self._context_manager

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        await self._context_manager.__aexit__(self, exc_type, exc_val, exc_tb)
        if isinstance(exc_val, CancelledError):
            await self._disconnect()
        else:
            raise exc_val
