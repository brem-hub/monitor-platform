import click
import os

from aiohttp import web

from backend.infra.base_server.commands.root import root_cmd
from backend.infra.base_server.config.config import parse_host
from backend.infra.base_server.config.config import GLOBAL_CONTEXT, get_version
from backend.monitor.application.application import Application


GLOBAL_CONTEXT.ApplicationName = "Monitor"
GLOBAL_CONTEXT.Application = Application
GLOBAL_CONTEXT.ApplicationVersion = get_version(os.getenv('VERSION_FILE'))


@click.command(name='runserver')
@click.option('--host', default='0.0.0.0:8999', help='States the port for future parser server. Default: 8999',
              required=False)
@click.option('--hot-swagger', is_flag=True, default=False, help='Reload swagger at startup', required=False)
@click.option('--swagger-path', default='./package/swagger.json', help='Swagger specification path', required=False)
def runserver(host: str, hot_swagger: bool, swagger_path: str):
    """Функция запуска сервера монитора с заданными параметрами.

    :param: хост запуска сервера.
    """

    app = Application(hot_swagger=hot_swagger, swagger_path=swagger_path)
    server_config = parse_host(host)
    web.run_app(app, host=server_config.host, port=server_config.port)


root_cmd.add_command(runserver)

# Непосредственный запуск сервера монитора.
if __name__ == '__main__':
    root_cmd()
