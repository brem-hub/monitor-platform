import asyncio
import http
import json

from aiohttp import web

from backend.infra.base_server.app.base_server import BaseApplication
from backend.infra.base_server.utils.network_utils import query_requirements
from backend.monitor.core.sse import SseConnection
from sseclient import SSEClient


class GatewayAPIResponse(web.Response):

    def __init__(self, message: str, status: int):
        data = {
            'message': message,
            'status': status,

        }
        super().__init__(text=json.dumps(data), status=status)


class Application(BaseApplication):
    MONITOR_UPDATES_HANDLER_TEMPLATE = 'http://{}/updates/{}'

    def routes(self):
        return [
            web.get('/v1/updates', self.get_updates)
        ]

    def prepare_response(self, response: dict):
        return json.dumps(response)

    @query_requirements('monitor', 'parser')
    async def get_updates(self, request: web.Request):
        monitor = request.query['monitor']
        parser = request.query['parser']

        self.api_logger.debug('monitor %s', monitor)
        self.api_logger.debug('parser %s', parser)

        kwargs = {
            'timeout': 15
        }

        try:
            async with SseConnection(request) as sse:
                request_url = self.MONITOR_UPDATES_HANDLER_TEMPLATE.format(monitor, parser)
                self.api_logger.debug('starting gateway for %s', request_url)
                while True:
                    try:
                        self.api_logger.debug('monitor %s parser %s', monitor, parser)
                        messages = SSEClient(request_url, **kwargs)
                        for message in messages:
                            self.api_logger.debug('message: %s', message)
                            await sse.send(message.data)
                            self.api_logger.debug('sent message')
                            await asyncio.sleep(0.5)
                    except Exception as inner_exc:
                        self.api_logger.debug('connection error: %s', inner_exc)
                        await asyncio.sleep(0.5)
        except Exception as exc:
            self.api_logger.error('error occurred: %s', exc)
            return GatewayAPIResponse(
                message='Error occurred, cannot support SSE connection with monitor {}; parser {}: {}'.format(monitor, parser, exc),
                status=http.HTTPStatus.INTERNAL_SERVER_ERROR)
