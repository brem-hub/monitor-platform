import json

from backend.parsers.core.products import BaseProduct
from backend.parsers.core.products_requests import ProductRequest


class BaseEvent:
    """Базовый класс события.

        Attributes
        ----------
        represent строковое название типа события.
        __attrs__ аттрибуты класса, подлежащие сериализации.

        Methods
        -------
        json(self)
            метод сериализует объект в jsonObject.
    """
    represent: str = "BASE"

    __attrs__ = ("represent",)

    def __str__(self):
        result = {}
        for attr in self.__attrs__:
            value = self.__getattribute__(attr)
            if "as_dict" in dir(value):
                value = value.as_dict()
            result[attr] = value
        return json.dumps(result)

    def json(self):
        return json.loads(self.__str__())


class ProductUpdateEvent(BaseEvent):
    """Класс события перемещения productRequest на новый уровень.

        Attributes
        ----------
        represent строковое название типа события.
        __attrs__ аттрибуты класса, подлежащие сериализации.
        product обновленный продукт.
        product_request обновленнный productRequest.
    """
    represent: str = "UPDATE"
    product: BaseProduct
    product_request: ProductRequest

    __attrs__ = ("represent", "product", "product_request")

    def __init__(self, product: BaseProduct, product_request: ProductRequest):
        self.product = product
        self.product_request = product_request


class CacheMoveEvent(BaseEvent):
    """Класс события перемещения productRequest на новый уровень.

        Attributes
        ----------
        represent строковое название типа события.
        __attrs__ аттрибуты класса, подлежащие сериализации.
        product_request_id uid перемещаемого productRequest.
        to_level уровень, на который этот productRequest перемещается.
    """
    represent: str = "MOVE"
    product_request_id: int
    to_level: str

    __attrs__ = ("represent", "product_request_id", "to_level")

    def __init__(self, product_request_id: int, to_level: str):
        self.product_request_id = product_request_id
        self.to_level = to_level


class CacheAddEvent(BaseEvent):
    """Класс события добавления нового productRequest.

        Attributes
        ----------
        represent строковое название типа события.
        __attrs__ аттрибуты класса, подлежащие сериализации.
        product_request добавленный productRequest.
        level уровень, на который этот productRequest добавлен.
    """
    represent: str = "ADD"
    product_request: ProductRequest
    level: str

    __attrs__ = ("represent", "product_request", "level")

    def __init__(self, product_request: ProductRequest, level: str):
        self.product_request = product_request
        self.level = level


class CacheDeleteEvent(BaseEvent):
    """Класс события перемещения productRequest на новый уровень.

        Attributes
        ----------
        represent строковое название типа события.
        __attrs__ аттрибуты класса, подлежащие сериализации.
        product_request_id uid удаленных продукта и productRequest.
    """
    represent: str = "DELETE"
    product_request_id: int

    __attrs__ = ("represent", "product_request_id")

    def __init__(self, product_request_id: int):
        self.product_request_id = product_request_id
