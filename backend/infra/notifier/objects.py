from typing import Dict
from typing import Collection, Optional, List, Any
from collections import deque


class Record:
    """Базовый класс записи в очередь.

        Attributes
        ----------
        _record сама запись.

        Methods
        -------
        record
            свойство для _record.
    """
    _record: Any = None

    def __init__(self, record: Any):
        self._record = record

    def __str__(self):
        return str(self._record)

    def __repr__(self):
        return str(self._record)

    @property
    def record(self):
        return self._record


class Listener:
    """Базовый класс 'прослушивателя' событий.

        Attributes
        ----------
        _has_updates - индикатор наличия изменений.

        Methods
        -------
        call(self, *args, **kwargs)
            обработать события.
        send(self, record: Record)
            принять новую запись.
        has_updates
            свойство для _has_updates
    """
    _has_updates: bool

    def __init__(self):
        self._has_updates = False

    def call(self, *args, **kwargs):
        self._has_updates = False

    def send(self, record: Record):
        self._has_updates = True

    @property
    def has_updates(self) -> bool:
        return self._has_updates


class Notifier(object):
    """Базовый класс менеджера уведомлений.

        Attributes
        ----------
        _subscribers: подписчики уведомлений.

        Methods
        -------
        send(self, signature: str, record: Any)
            принять новую запись.
        clear(self, signature: str = None)
            очистить подписчиков
        has_updates
            свойство для _has_updates
        subscribe(self, signature: str, subscriber: Listener)
            подписать на уведомления.
        unsubscribe(self, signature: str, subscriber: Listener)
            отписать от уведомлений.
    """
    _subscribers: Dict[str, List[Listener]] = {}

    def send(self, signature: str, record: Any):
        if signature not in self._subscribers.keys():
            raise ValueError("No such a signature in notifier!")
        else:
            for listener in self._subscribers[signature]:
                listener.send(record)

    def clear(self, signature: str = None):
        if signature is None:
            self._subscribers.clear()
        elif signature in self._subscribers.keys():
            self._subscribers[signature].clear()

    def subscribe(self, signature: str, subscriber: Listener):
        if signature not in self._subscribers.keys():
            self._subscribers[signature] = list()
        self._subscribers[signature].append(subscriber)

    def unsubscribe(self, signature: str, subscriber: Listener):
        if signature in self._subscribers.keys():
            for i in range(len(self._subscribers[signature]) - 1, -1, -1):
                if subscriber == self._subscribers[signature][i]:
                    del self._subscribers[signature][i]


class UpdateQueue(Listener):
    """Базовый класс менеджера уведомлений.

        Attributes
        ----------
        _deque: дек записей.
        _max_size максимальная длина дека.

        Methods
        -------
        add(self,
            *elements,
            elements_collection: Collection[Record] = None,
            queue: Optional['UpdateQueue'] = None,
            element: Optional[Record] = None)
            принять новую запись.
        size(self)
            получить размер дека.
        pop(self, amount: int = 1)
            вытянуть amount записей.
        pop_all(self)
            вытянуть все записи.
        clear(self)
            очистить все записи
        is_empty(self)
            проверяет, пуст ли дек.
        call(self, *args, **kwargs)
            вызвать всё
        (self, record: Any)
            отправить новую запись.
    """
    _deque: deque[Record]
    _max_size: int = 200

    def __init__(self, queue: Optional['UpdateQueue'] = None):
        super().__init__()
        self._deque = deque()
        if queue:
            self.add(queue=queue)

    def add(self,
            *elements,
            elements_collection: Collection[Record] = None,
            queue: Optional['UpdateQueue'] = None,
            element: Optional[Record] = None) -> bool:
        if element and isinstance(element, Record):
            if len(self._deque) == self._max_size:
                return False
            self._deque.append(element)
        elif elements:
            for element in elements:
                if not self.add(element=element):
                    return False
        elif elements_collection:
            for element in elements_collection:
                if not self.add(element=element):
                    return False
        elif queue:
            elements = queue.pop_all()
            for element in elements:
                if not self.add(element=element):
                    return False
        return True

    def size(self) -> int:
        return len(self._deque)

    def pop(self, amount: int = 1) -> List[Record]:
        if amount > self.size() or amount < 0:
            raise ValueError(f"Wrong pop elements amount. Expected: 0<amount<={self.size()}, got: {amount}")
        return [self._deque.popleft() for i in range(amount)]

    def pop_all(self):
        return self.pop(amount=self.size())

    def clear(self):
        self._deque.clear()

    def is_empty(self) -> bool:
        return self.size() != 0

    def __str__(self):
        return self._deque.__str__()

    def __repr__(self):
        return self._deque.__repr__()

    def call(self, *args, **kwargs):
        super().call()
        result = self.pop_all()
        return result

    def send(self, record: Any):
        record = Record(record)
        super().send(record)
        self.add(record)
