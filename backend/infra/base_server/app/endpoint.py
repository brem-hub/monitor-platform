# -*- coding: utf-8 -*-

from typing import Iterable

from aiohttp.web import UrlDispatcher

# expand if needed
METHODS = ['GET', 'POST', '*']


class Endpoint:
    """
    Endpoint - кастомная ручка, позволяет более удобно создавать ручки.
    Необходимо переопределить router у Application, чтобы можно было пользоваться своими ручками.
    """

    def __init__(self, path: str, handler, l_name: str, method: str = '*'):
        self.path = path
        self.handler = handler
        self.name = l_name
        self.method = method


class EndpointDispatcher(UrlDispatcher):
    """
    EndpointDispatcher - кастомный роутер, который позволяет обрабатывать ручки типа Endpoint.
    """

    def add_routes(self, endpoints: Iterable[Endpoint]) -> None:
        """Переопределяем базовый add_routes, чтобы использовать ручки типа
        Endpoint.

        Args:
            endpoints: массив ручек типа Endpoint
        """
        for endpoint in endpoints:
            self.add_route(endpoint.method, endpoint.path,
                           endpoint.handler, name=endpoint.name)
