# -*- coding: utf-8 -*-
import json
from typing import Any, Iterable

from swagger_ui import api_doc
from aiohttp import web
from aiohttp.web_routedef import AbstractRouteDef

from backend.infra.base_server.handlers.ping_handler import PingHandler
from backend.infra.base_server.app.actions.swagger import generate_swagger_spec, DEFAULT_SWAGGER_DOCS_BASE_PATH
from backend.infra.logging.setup_logging import *
from backend.infra.base_server.handlers.middleware.logging_middleware import logging_middleware
from backend.infra.base_server.config.config import GLOBAL_CONTEXT


class BaseApplication(web.Application):
    api_logger: logging.Logger = logging.getLogger('api')

    DEFAULT_SWAGGER_CONFIG_PATH = './package/swagger.json'
    DEFAULT_SWAGGER_TITLE = 'API docs'

    """
    BaseApplication - пример сервера.
    Можно использовать как базовое приложение.

    Имеет одну ручку `GET /ping`
    """

    def additional_middlewares(self):
        return []

    def routes(self) -> Iterable[AbstractRouteDef]:
        return []

    def __init__(self, *args: Any, **kwargs: Any):
        hot_swagger = kwargs.pop('hot_swagger', False)
        swagger_path = kwargs.pop('swagger_path', './swagger.json')

        super().__init__(
            *args,
            **kwargs
        )

        # add default handlers.
        self.add_routes(
            [
                # дефолтный ping для health check.
                web.get('/ping', PingHandler, name='ping')
            ],
        )
        self.add_routes(self.routes())

        # add default middleware
        self.middlewares.append(
            logging_middleware
        )

        if hot_swagger:
            spec = generate_swagger_spec('2.0')

            if spec is None:
                raise Exception(
                    'Could not generate spec, check GLOBAL_CONTEXT')

            with open(swagger_path, 'w') as f:
                f.write(json.dumps(spec.to_dict(), indent=2))

        title = GLOBAL_CONTEXT.ApplicationName or 'API docs'
        api_doc(self, config_path=swagger_path,
                url_prefix=DEFAULT_SWAGGER_DOCS_BASE_PATH, title=title)
