import abc
import logging
from typing import Optional, Callable, Type
from types import FunctionType

from aiohttp import web

from apispec import APISpec, BasePlugin
from apispec.yaml_utils import load_yaml_from_docstring
from backend.infra.base_server.config.config import GLOBAL_CONTEXT

DEFAULT_SWAGGER_DOCS_BASE_PATH = '/docs'

DEFAULT_SWAGGER_DOCS_PATH = [
    DEFAULT_SWAGGER_DOCS_BASE_PATH,
    DEFAULT_SWAGGER_DOCS_BASE_PATH + '/',
    DEFAULT_SWAGGER_DOCS_BASE_PATH + '/swagger.json',
    DEFAULT_SWAGGER_DOCS_BASE_PATH + '/static'
]

DEFAULT_VERSION = 'v.0.0.0'


class AiohttpPlugin(BasePlugin):
    def path_helper(self, operations=None, route=None, doc=None, **kwargs):
        method = route.method.lower()
        operations[method] = load_yaml_from_docstring(doc)
        return route.resource.canonical


def _get_wrapped_method(handler):
    try:
        if (closures := handler.__closure__) is None:
            return handler
    except AttributeError:
        return None

    for closure in closures:
        if isinstance(closure.cell_contents, FunctionType):
            return _get_wrapped_method(closure.cell_contents)
        recovered_handler = _get_wrapped_method(closure.cell_contents)
        if recovered_handler is not None:
            return recovered_handler

    return None


def _handler2swagger_operation(method: str, handler) -> Optional[str]:
    """Get documentation from handler.

    Args:
        method: name of the method in lower key. Ex. get | post | put
        handler: handler itself. Can be web.View, class, function, wrapped function.
    Returns:
        documentation of the handler.
    """
    if isinstance(handler, abc.ABCMeta):
        # handler is class
        attr = handler(None).__getattribute__(method)

        # attr is function.
        # ex:
        # async def get():
        #   """
        #   Get something
        #   ---
        #   summary: Get something
        #   responses:
        #     200:
        #       ....
        if (closures := attr.__closure__) is None:
            return handler(None).__getattribute__(method).__doc__

        # attr is wrapper over function
        # ex:
        # @foo_wrapper(meta: "my_meta")
        # async def get():
        #   """
        #   Get something
        #   ---
        #   summary: Get something
        #   responses:
        #     200:
        #       ....
        # In this example attr is foo_wrapper and `get` function hides inside wrapper closures
        for closure in closures:
            if isinstance(closure.cell_contents, FunctionType) and closure.cell_contents.__name__ == method:
                return closure.cell_contents.__doc__
        return None

    if isinstance(handler, web.View):
        return handler.__getattribute__(method).__doc__

    fu = _get_wrapped_method(handler)

    if fu is not None:
        return fu.__doc__

    if isinstance(handler, Callable):
        return handler.__doc__

    return None


def generate_swagger_spec(open_api_version: str):
    if GLOBAL_CONTEXT.Application is None:
        logging.error('application is not set, cannot generate swagger')
        return

    spec = APISpec(
        title=GLOBAL_CONTEXT.ApplicationName or 'Swagger documentation',
        version=GLOBAL_CONTEXT.ApplicationVersion or DEFAULT_VERSION,
        openapi_version=open_api_version,
        plugins=[AiohttpPlugin()]
    )

    if isinstance(GLOBAL_CONTEXT.Application, Type):  # noqa
        app = GLOBAL_CONTEXT.Application()  # noqa
    elif not isinstance(GLOBAL_CONTEXT.Application, web.Application):
        raise ValueError('context application is not an instance of class or application')
    else:
        app = GLOBAL_CONTEXT.Application

    if app is None:
        raise ValueError('context application is not set')

    for route in app.router.routes():
        if route.handler is None:
            logging.warning(
                f'route `{route.name}` has incorrect method or handler is not set')
            continue

        # when create GET handler HEAD is created automatically, skip them.
        if route.method == 'HEAD':
            continue

        if route.resource.canonical in DEFAULT_SWAGGER_DOCS_PATH:
            continue

        doc = _handler2swagger_operation(route.method.lower(), route.handler)

        if doc is None:
            logging.warning(
                f'could not get proper docs for route `{route.name}`')
            continue

        spec.path(route=route, doc=doc)

    return spec
