import click
from backend.infra.base_server.commands.import_stack import print_import_stack
from backend.infra.base_server.commands.generate_swagger import generate_swagger


# Основная группа команд, является точкой входа в приложение.
# Все команды, которые должны быть доступны пользователю должны подключаться к этой группе.


@click.group()
def root_cmd():
    """Root command."""
    pass


# Дефолтные команды для сервера
root_cmd.add_command(print_import_stack)
root_cmd.add_command(generate_swagger)
