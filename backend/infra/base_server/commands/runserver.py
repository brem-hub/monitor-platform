import click
from aiohttp import web

from backend.infra.base_server.app.base_server import BaseApplication


@click.command(name='runserver')  # noqa
@click.option('--port', default=9000, help='server port')
def runserver_cmd(port: int):
    """Run server."""
    app = BaseApplication()
    web.run_app(app, port=port)
