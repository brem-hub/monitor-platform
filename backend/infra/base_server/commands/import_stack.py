import inspect

import click


@click.command(name='import-stack')
def print_import_stack():
    for el in inspect.stack():
        print(el.filename)
