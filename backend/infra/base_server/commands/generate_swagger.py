import json
import logging

import click

from backend.infra.base_server.app.actions.swagger import generate_swagger_spec


@click.command(name='generate-swagger')
@click.option('--path', default='./package/swagger.json', help='Save specification to json file', required=False)
@click.option('--stdout', '-o', is_flag=True, default=False, help='Print specification to console', required=False)
@click.option('--open-api-version', default='2.0', help='Generate specification for specified version', required=False)
def generate_swagger(path: str, open_api_version: str, stdout: bool) -> None:
    spec = generate_swagger_spec(open_api_version)
    if stdout:
        print(json.dumps(spec.to_dict(), indent=2))
        return

    if path == '':
        logging.error(f'path `{path}` is not valid')
        return

    with open(path, 'w') as f:
        f.write(json.dumps(spec.to_dict(), indent=2))
