import logging
import re

from aiohttp.web import middleware, Request, Response, FileResponse
from typing import Callable

BLACK_LIST = ['/ping', '/static/.*', '/api/token/.*', '/docs']

__logger = logging.getLogger('mw')


__matchers = [re.compile(matcher) for matcher in BLACK_LIST]


def parse_black_list(path: str):
    for matcher in __matchers:
        if matcher.match(path) is not None:
            return True
    return False


@middleware
async def logging_middleware(request: Request, handler: Callable) -> Response:
    blacked = parse_black_list(request.path)
    if not blacked:
        __logger.debug('[request start] [%s] [%s] [%s]',
                       request.method, request.path, request.query_string)

    response: Response = await handler(request)

    if not blacked:
        __logger.debug('[request finished] [%s] [%s]',
                       response.status, 'file' if isinstance(response, FileResponse) else response.body)

    return response
