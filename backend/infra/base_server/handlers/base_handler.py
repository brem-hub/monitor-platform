# -*- coding: utf-8 -*-

from __future__ import annotations

from typing import Any

from aiohttp import web


class BaseHandler(web.View):
    """BaseHandler наследуется от web.View, что позволяет переопределять методы
    get/post для обработки необходимых запросов.

    Каждый хэндлер имеет асинхронный метод action, который позволяет выполнять какую-либо работу,
     в идеале именно в нем происходит вся логика хэндлера.

    Так же из запроса можно получать ключи с помощью метода query_get.
    """

    def query_get(self, key: str) -> str | None:
        """
        Получить ключ из запроса.
        Args:
            key: ключ

        Returns:
            значение ключа или None, если такого ключа нет

        """
        return self.request.get(key)

    async def action(self, params: dict = None) -> Any:
        """
        Основной метод для выполнения работы хэндлера
        Args:
            params: параметры в формате словаря

        Returns:
            Необходимый ответ
        """
        raise NotImplementedError
