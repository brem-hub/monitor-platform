# -*- coding: utf-8 -*-

from aiohttp import web

from backend.infra.base_server.handlers.base_handler import BaseHandler


class PingAction:
    """Action, который выполняется при вызове ручки PingHandler.

    Action создается для удобства тестирования, конечно удобнее в
    PingHandler.get() сразу вернуть `pong`, но в данном случае это
    пример того, как можно делать.
    """

    def __init__(self, request: web.Request):
        self.request = request

    def __call__(self, *args, **kwargs):
        return 'pong'


class PingHandler(BaseHandler):
    async def get(self):
        """
        ---
        summary: Пингануть сервис
        responses:
          200:
            description: Pong
            examples:
              application/json:
                pong
        """
        action = PingAction(self.request)
        return web.Response(text=action())
