import pytest  # noqa
from handlers.ping_handler import PingAction  # noqa


def test_ping_handler():
    # Именно для тестов мы отделяем сущности Handler и Action.
    # Если бы мы оставили просто метод get(), который в PingHandler сразу отдавал бы web.Responce с `pong`,
    # это было бы очень трудно тестировать.
    action = PingAction(None)

    assert action() == 'pong'
