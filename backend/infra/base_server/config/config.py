import logging
import os.path
import re
from dataclasses import dataclass
from typing import Optional


@dataclass
class Context:
    Application: Optional[object]
    ApplicationName: Optional[str]
    ApplicationVersion: Optional[str]


GLOBAL_CONTEXT = Context(None, None, None)


def get_version(path: str = None) -> Optional[str]:
    _default_path = './package/version'
    _version_regex = r'v[0-9]+\.[0-9]+\.[0-9]+'

    version_file_path = path or _default_path

    if not os.path.exists(version_file_path):
        logging.error(f'Version file `{version_file_path}` does not exists')
        return None

    with open(version_file_path) as f:
        data = f.read()
        pattern = re.compile(_version_regex)

        matched_version = pattern.match(data)

        if matched_version is not None:
            return matched_version.string
        return None


@dataclass
class ServerConfig:
    """Класс конфига для сервера.

    Attributes
    ----------
    host    host string
    port    port
    """
    host: str
    port: int

    def __str__(self):
        return f"{self.host}:{self.port}"


def parse_host(data: str) -> ServerConfig:
    elements = data.split(':')
    if len(elements) != 2:
        raise AttributeError(
            f'Address must contain 2 parts <host>:<port>. Got: {data}. Ex: 127.0.0.1:9000')
    return ServerConfig(elements[0], int(elements[1]))
