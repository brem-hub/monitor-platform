# -*- coding: utf-8 -*-

from commands.root import root_cmd

# Точка входа для обычного запуска, не через модули.
# Вход через модули настраивается в setup.py
if __name__ == '__main__':
    root_cmd()
