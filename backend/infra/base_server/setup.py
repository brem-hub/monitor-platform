from setuptools import setup, find_packages

# Описание модуля, по которому создается модуль из сервиса, что нужно,
# чтобы одинаково запускать его как на Unix, так и на Windows.

setup(
    # название модуля
    name='base_server',
    # версия модуля
    version='0.1',
    # все модули, которые лежат внутри папок будут добавлены в исполняемый файл
    packages=find_packages(),
    # данные для отладки
    include_package_data=True,
    # при сборке модуля внутри venv, можно будет запускать сервер командой base_server. (см. README)
    entry_points={
        'console_scripts': [
            'base_server = main:root_cmd',
        ]
    }
)
