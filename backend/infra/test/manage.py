import click
from aiohttp import web
from backend.infra.base_server.config.config import parse_host
from backend.infra.test.application import Application
from backend.infra.base_server.commands.root import root_cmd


@click.command(name='runserver')
@click.option('--host', default='0.0.0.0:8080', help='Service host', required=False)
def runserver(host: str):
    config = parse_host(host)
    application = Application()
    web.run_app(application, host=config.host, port=config.port)


root_cmd.add_command(runserver)

if __name__ == '__main__':
    root_cmd()
