package app

import (
	"context"
	"github.com/gorilla/mux"
	"github.com/xfxdev/xlog"
	"monitor_manager/pkg/core/app/cache"
	application_context "monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/server/middleware"
	"net/http"
	"time"
)

type Application struct {
	interactions application_context.ApplicationContext
}

func NewApplication() *Application {
	iters, err := application_context.NewInteractionContext()
	if err != nil {
		xlog.Errorf("could not create interffactions context: %v", err)
		return nil
	}

	return &Application{interactions: *iters}
}

func (a *Application) router() *mux.Router {
	// setup handlers.
	router := mux.NewRouter()

	router.Use(middleware.LoggingMiddleware)
	router.Use(middleware.SwaggerMiddleware(a.interactions.Config().Swagger.Path, a.interactions.Config().Swagger.Url))

	router.Handle(a.interactions.Config().Swagger.Url, nil).Methods("GET", "OPTIONS")
	router.Handle("/swagger.json", nil).Methods("GET", "OPTIONS")

	for _, route := range a.Routes() {
		router.Handle(route.Name, route.Handler).Methods(route.Method)
	}

	router.NotFoundHandler = http.HandlerFunc(DefaultNotFoundHandler)
	router.MethodNotAllowedHandler = http.HandlerFunc(DefaultMethodNotAllowedHandler)

	return router
}

func (a *Application) Start(ctx context.Context, host string) {
	router := a.router()

	cacheManager := cache.NewManager()

	cacheManager.Append("yc_client", cache.NewCacheEntity(cache.YCApiRefresh{}, 1*time.Hour))
	cacheManager.Append("parsers_cache", cache.NewCacheEntity(a.interactions.ParsersCache(), 1*time.Hour))

	initError, cacheCtx := cacheManager.Init(a.interactions)

	defer func() {
		err := cacheManager.Stop()

		if err != nil {
			xlog.Errorf("could not stop cache correctly: %v", err)
		}
	}()

	if initError != nil {
		xlog.Errorf("could not warm cache. Reason: %v", initError)
		return
	}

	xlog.Info("Successfully warmed cache")
	xlog.Infof("Initiating server on host [%s]", host)

	server := &http.Server{Addr: host, Handler: router}

	errorChan := make(chan error)

	go func() {
		if err := server.ListenAndServe(); err != nil {
			errorChan <- err
		}
	}()

	select {
	case err := <-errorChan:
		// server error
		xlog.Errorf("server error occurred: %v", err)
	case <-cacheCtx.Done():
		// cache error
		xlog.Errorf("cache error occurred: %v", cacheCtx.Err())
		xlog.Debug("shutting down server")

		err := server.Shutdown(ctx)

		if err != nil {
			xlog.Errorf("could not shutdown server: %v", err)
		}
	}
}

func (a *Application) Adaptor(handler func(ctx application_context.ApplicationContext, w http.ResponseWriter, r *http.Request)) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		handler(a.interactions, writer, request)
	})
}
