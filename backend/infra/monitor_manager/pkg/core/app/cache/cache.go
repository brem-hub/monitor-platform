package cache

import (
	"context"
	"github.com/xfxdev/xlog"
	"golang.org/x/sync/errgroup"
	"sync"
	"time"
)

type Cache interface {
	Refresh(context.Context) error
}

type CacheEntity struct {
	c Cache
	d time.Duration
}

func NewCacheEntity(cache Cache, duration time.Duration) *CacheEntity {
	return &CacheEntity{
		c: cache,
		d: duration,
	}
}

type CacheManager struct {
	caches map[string]CacheEntity

	gr *errgroup.Group
}

func NewManager() *CacheManager {
	return &CacheManager{
		caches: make(map[string]CacheEntity, 0),
	}
}

func (m *CacheManager) Append(name string, cache *CacheEntity) {
	// Don't update already existing cache
	if _, ok := m.caches[name]; ok {
		return
	}

	m.caches[name] = *cache
}

func (m *CacheManager) Init(ctx context.Context) (initError error, cacheContext context.Context) {

	wg := sync.WaitGroup{}

	wg.Add(len(m.caches))

	m.gr, cacheContext = errgroup.WithContext(ctx)

	for cacheName, cache := range m.caches {
		lCacheName := cacheName
		lCache := cache

		initialRun := true

		m.gr.Go(func() error {
			timer := time.NewTicker(lCache.d)

		loop:
			for {
				xlog.Debugf("updating cache `%s`", lCacheName)
				err := lCache.c.Refresh(ctx)

				if initialRun {
					if err != nil {
						initError = err
						// Additional wg.Done() because if we do wg.Done() before setting initError we get data race
						//  because wg.Wait() can finish before we set initError and Init will return nil as no error is set.
						// initError can overwrite some other error - it does not matter, we handle ANY error at initial run.
						xlog.Errorf("could not warm cache `%s`. Reason: %v", lCacheName, err)
						wg.Done()
						return err
					}
					initialRun = false
					wg.Done()
				}

				if err != nil {
					xlog.Errorf("could not update cache `%s`. Reason: %v", lCacheName, err)
					return err
				}

				select {
				case <-timer.C:
				case <-ctx.Done():
					break loop
				}
			}

			return nil
		})
	}

	wg.Wait()
	return
}

func (m *CacheManager) Stop() error {
	return m.gr.Wait()
}
