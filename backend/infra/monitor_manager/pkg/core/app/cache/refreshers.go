package cache

import (
	"context"
	"fmt"
	applicationContext "monitor_manager/pkg/core/app/context"
)

type YCApiRefresh struct {
}

var _ Cache = (*YCApiRefresh)(nil)

func (r YCApiRefresh) Refresh(ctx context.Context) error {
	appCtx := ctx.(applicationContext.ApplicationContext)

	yc := appCtx.YcApi()

	containerRegistry := appCtx.RegistryClient()

	err := yc.Refresh(ctx)

	if err != nil {
		return fmt.Errorf("could not refresh token cache, error occurred: %v", err)
	}

	if err = containerRegistry.RefreshConnection(yc); err != nil {
		return fmt.Errorf("could not refresh connection: %v", err)
	}

	return nil
}
