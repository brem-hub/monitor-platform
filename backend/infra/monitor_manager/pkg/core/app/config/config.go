package config

import (
	"context"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
	"os"
)

const (
	defaultConfigPath = "./config/common.yaml"
)

type Authorization struct {
	Key     string `config:"key,required"`
	Account string `config:"account,required"`
	Pk      string `config:"pk,required"`
}

type Swagger struct {
	Url  string `config:"url"`
	Path string `config:"path,required"`
}

type Postgres struct {
	Url      string `config:"url,required"`
	Port     int    `config:"port,required"`
	Password string `config:"password,required"`
	User     string `config:"user,required"`
	Database string `config:"database,required"`
}

type Config struct {
	Authorization Authorization `config:"authorization,required"`
	//Parsers       []string      `config:"parsers,required"`
	Postgres Postgres `config:"postgres,required"`
	//Tokenizator   string        `config:"tokenizator,required"`
	Swagger Swagger `config:"swagger"`
}

func GetConfig() (Config, error) {

	configPath := defaultConfigPath

	if os.Getenv("MONITOR_MANAGER_CONFIG") != "" {
		configPath = os.Getenv("MONITOR_MANAGER_CONFIG")
	}

	loader := confita.NewLoader(
		file.NewBackend(configPath),
	)

	var cfg Config

	err := loader.Load(context.Background(), &cfg)

	return cfg, err
}
