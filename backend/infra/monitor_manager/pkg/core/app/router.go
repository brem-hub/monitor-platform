package app

import (
	"monitor_manager/pkg/server/response"
	v1 "monitor_manager/pkg/server/v1"
	"net/http"
)

type Route struct {
	Name    string
	Handler http.Handler
	Method  string
}

func (a *Application) Routes() []Route {
	return []Route{
		{"/ping", a.Adaptor(v1.PingHandler), "GET"},

		// manage monitor
		{"/v1/monitor", a.Adaptor(v1.CreateMonitor), "POST"},
		{"/v1/monitor", a.Adaptor(v1.DeleteMonitor), "DELETE"},

		// manage monitor parsers
		{"/v1/monitor/parsers", a.Adaptor(v1.UpdateParsers), "PUT"},
		{"/v1/monitor/parsers/full", a.Adaptor(v1.UpdateParsersFull), "PUT"},
		{"/v1/monitor/parsers", a.Adaptor(v1.DeleteParsers), "DELETE"},

		// manage available parsers
		{"/v1/parsers/available", a.Adaptor(v1.GetAvailableParsers), "GET"},
		{"/v1/parsers/refresh", a.Adaptor(v1.RefreshAvailableParsers), "GET"},

		// manage monitors for all users
		{"/v1/monitors", a.Adaptor(v1.UpdateMonitorVersion), "PUT"},
	}
}

func DefaultNotFoundHandler(w http.ResponseWriter, r *http.Request) {
	response.ErrorResponse(w, response.NotFound(r.URL.Path))
}

func DefaultMethodNotAllowedHandler(w http.ResponseWriter, r *http.Request) {
	response.ErrorResponse(w, response.MethodNotAllowed(r.URL.Path, r.Method))
}
