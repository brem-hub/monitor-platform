package context

import (
	"context"
	"monitor_manager/pkg/core/app/config"
	"monitor_manager/pkg/interactions/container_registry"
	"monitor_manager/pkg/interactions/k8s_client"
	"monitor_manager/pkg/interactions/parser_cache"
	"monitor_manager/pkg/interactions/tokenizator"
	"monitor_manager/pkg/interactions/yc_api_token"
)

type ApplicationContext struct {
	context.Context
	kubeClient        *k8s_client.KubeBackend
	registryClient    *container_registry.RegistryBackend
	tokenizatorClient *tokenizator.TokenizatorBackend
	ycApi             *yc_api_token.YCApiBackend
	parsersCache      *parser_cache.ParserCache

	config config.Config
}

func (c *ApplicationContext) RegistryClient() *container_registry.RegistryBackend {
	return c.registryClient
}

func (c *ApplicationContext) Tokenizator() *tokenizator.TokenizatorBackend {
	return c.tokenizatorClient
}

func (c *ApplicationContext) K8SClient() *k8s_client.KubeBackend {
	return c.kubeClient
}

func (c *ApplicationContext) YcApi() *yc_api_token.YCApiBackend {
	return c.ycApi
}

func (c *ApplicationContext) ParsersCache() *parser_cache.ParserCache {
	return c.parsersCache
}

func (c *ApplicationContext) Config() config.Config {
	return c.config
}

func NewInteractionContext() (*ApplicationContext, error) {
	client, err := k8s_client.NewKubeClient()

	if err != nil {
		return nil, err
	}

	registryClient := container_registry.NewRegistryClient()

	if err != nil {
		return nil, err
	}

	cfg, err := config.GetConfig()

	if err != nil {
		return nil, err
	}

	tokenizatorClient := tokenizator.NewTokenizatorClient()

	ycApi, err := yc_api_token.NewTokenCache(cfg)

	if err != nil {
		return nil, err
	}

	parsers, err := parser_cache.NewParserCache(cfg)

	if err != nil {
		return nil, err
	}

	return &ApplicationContext{kubeClient: client,
		registryClient:    registryClient,
		tokenizatorClient: tokenizatorClient,
		ycApi:             ycApi,
		parsersCache:      parsers,
		config:            cfg,
		Context:           context.Background(),
	}, nil
}
