package actions

import (
	"github.com/xfxdev/xlog"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"monitor_manager/pkg/core/app/context"
)

// DeleteMonitor action deletes monitor from cluster and makes user token inactive.
func DeleteMonitor(interactions context.ApplicationContext, monitor string) error {
	xlog.Debugf("deleting monitor: %s", monitor)

	deploymentClient := interactions.K8SClient().API().AppsV1().Deployments(mainNamespace)
	serviceClient := interactions.K8SClient().API().CoreV1().Services(mainNamespace)

	token, err := interactions.Tokenizator().GetByMonitor(monitor)

	if err != nil {
		return err
	}

	token.IsActive = false

	err = interactions.Tokenizator().Update(*token)

	if err != nil {
		xlog.Errorf("could not update token: %v", err)
		return err
	}

	xlog.Debug("changed monitor status to inactive")

	err = serviceClient.Delete(interactions, monitor, metav1.DeleteOptions{})
	if err != nil {
		return err
	}

	xlog.Debug("deleted monitor service")

	err = deploymentClient.Delete(interactions, monitor, metav1.DeleteOptions{})
	if err != nil {
		return err
	}

	xlog.Debug("deleted monitor deployment")

	return nil
}
