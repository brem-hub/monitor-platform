package actions

import (
	"errors"
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"monitor_manager/pkg/core/app/context"
	"time"
)

func updateMonitor(deployment *v1.Deployment, version string) (bool, error) {

	correctVersion := fmt.Sprintf(monitorImage, version)

	for index := range deployment.Spec.Template.Spec.Containers {

		if deployment.Spec.Template.Spec.Containers[index].Name == "monitor" {
			if deployment.Spec.Template.Spec.Containers[index].Image == correctVersion {
				return false, nil
			}

			deployment.Spec.Template.Spec.Containers[index].Image = correctVersion
			return true, nil
		}
	}

	return false, xerrors.New("could find monitor container")
}

func UpdateMonitorVersion(interactions context.ApplicationContext, version string, dryRun bool) error {
	xlog.Debugf("updating monitor versions")

	var monitorTag string
	var err error

	if version == "" {
		xlog.Debugf("monitor version is not set, getting the latest version")
		monitorTag, err = getLastTag(interactions, monitorRepo)
		if err != nil {
			return err
		}
		xlog.Debugf("got latest version %s", monitorTag)
	} else {
		xlog.Debugf("monitor version is set, checking it's validity")
		if !containsTag(interactions, monitorRepo, version) {
			return errors.New(fmt.Sprintf("Could not find version: %s", version))
		}

		monitorTag = version
		xlog.Debugf("monitor version `%s` is correct", monitorTag)
	}

	deploymentClient := interactions.K8SClient().API().AppsV1().Deployments(mainNamespace)

	data, err := deploymentClient.List(interactions, metav1.ListOptions{LabelSelector: "pType"})

	if err != nil {
		return err
	}

	if dryRun {
		xlog.Debugf("dry run: would update monitors to version %s", monitorTag)
		return nil
	}

	const threshold = 5

	handledMonitors := 0

	for _, el := range data.Items {
		if el.Labels["pType"] == "monitor" {

			needUpdate, err := updateMonitor(&el, monitorTag)

			if err != nil {
				xlog.Warnf("deployment `%s` was not updated. Reason: %v", el.Name, err)
				continue
			}

			if !needUpdate {
				continue
			}

			// wait so not to overflow quota with simultaneously deployed pods.
			if handledMonitors > 0 && handledMonitors%threshold == 0 {
				sleepGap := 2 + handledMonitors/threshold
				xlog.Debugf("waiting for %d seconds", sleepGap)
				time.Sleep(time.Duration(sleepGap) * time.Second)
			}

			_, err = deploymentClient.Update(interactions, &el, metav1.UpdateOptions{})

			if err != nil {
				return err
			}

			handledMonitors++

		}
	}

	return nil
}
