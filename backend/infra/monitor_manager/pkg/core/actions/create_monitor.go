package actions

import (
	"fmt"
	"github.com/xfxdev/xlog"
	api "github.com/yandex-cloud/go-genproto/yandex/cloud/containerregistry/v1"
	"golang.org/x/xerrors"
	appsApi "k8s.io/api/apps/v1"
	coreApi "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/apimachinery/pkg/util/uuid"
	"k8s.io/utils/pointer"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/interactions/tokenizator"
	"time"
)

const (
	mainNamespace = "monitor-boi"

	monitorRepo = "crpre0rkqibm7l9m3m3f/monitor"
	parserRepo  = "crpre0rkqibm7l9m3m3f/parser"

	monitorImage = "cr.yandex/crpre0rkqibm7l9m3m3f/monitor:%s"
	parserImage  = "cr.yandex/crpre0rkqibm7l9m3m3f/parser:%s"
	redisImage   = "redis:alpine"

	monitorPort = 8999
	redisPort   = 6379

	defaultParserPort int32 = 9000

	uuidSize = 6
)

// getLastTag searches through Docker images and returns the latest (by tag) image.
func getLastTag(ctx context.ApplicationContext, repositoryName string) (string, error) {
	service := ctx.RegistryClient().Client()

	data, err := service.List(ctx, &api.ListImagesRequest{RepositoryName: repositoryName})

	if err != nil {
		return "", err
	}

	if len(data.Images) == 0 {
		return "", fmt.Errorf("repository `%s` does not have images", repositoryName)
	}

	var tag string

	for _, image := range data.Images {
		if len(image.Tags) == 0 {
			xlog.Warnf("image `%s` does not have tags, skipping", image.Name)
			continue
		}
		if tag < image.Tags[0] {
			tag = image.Tags[0]
		}
	}

	if tag == "" {
		return "", fmt.Errorf("could not find latest tag for repository: %s", repositoryName)
	}

	return tag, nil
}

func containsTag(ctx context.ApplicationContext, repositoryName string, tag string) bool {
	service := ctx.RegistryClient().Client()

	data, err := service.List(ctx, &api.ListImagesRequest{RepositoryName: repositoryName})

	if err != nil {
		return false
	}

	if len(data.Images) == 0 {
		return false
	}

	for _, image := range data.Images {
		if len(image.Tags) == 0 {
			xlog.Warnf("image `%s`(%s) does not have tags, skipping", image.Name, image.Id)
			continue
		}
		for _, imTag := range image.Tags {
			if imTag == tag {
				return true
			}
		}
	}

	return false

}

// redisContainer returns service container with Redis.
func redisContainer(monitorId string) coreApi.Container {
	return coreApi.Container{
		Name:  "redis",
		Image: redisImage,
		Env: []coreApi.EnvVar{
			{
				Name:  "REDIS_APPLICATION_NAME",
				Value: "master",
			},
			{
				Name:  "MONITOR_ID",
				Value: monitorId,
			},
		},
		Ports: []coreApi.ContainerPort{
			{
				Name:          "api",
				ContainerPort: redisPort,
			},
		},
		VolumeMounts: []coreApi.VolumeMount{
			{
				Name:      "cache-storage",
				MountPath: "/cache",
				ReadOnly:  false,
			},
			{
				Name:      "cache-config",
				MountPath: "/configs/cache_setup.sh",
				SubPath:   "cache_setup.sh",
				ReadOnly:  false,
			},
		},
		Command: []string{"/bin/sh"},
		Args:    []string{"-c", fmt.Sprintf("sh /configs/cache_setup.sh; redis-server --dir /cache/%s", monitorId)},
	}
}

// monitorContainer returns monitor container.
// Tag defines monitor version.
func monitorContainer(monitorId string, tag string) coreApi.Container {
	return coreApi.Container{
		Name:            "monitor",
		Image:           fmt.Sprintf(monitorImage, tag),
		ImagePullPolicy: coreApi.PullIfNotPresent,
		Env: []coreApi.EnvVar{
			{
				Name:  "MONITOR_ID",
				Value: monitorId,
			},
		},
		Ports: []coreApi.ContainerPort{
			{
				Name:          "api",
				ContainerPort: monitorPort,
			},
		},
	}
}

// parserContainer returns Kubernetes Container for specific parser.
// Available parsers are set in Config.
// Parser will be assigned given port.
// Tag defines parser version.
func parserContainer(parserType string, port int32, tag string) coreApi.Container {
	return coreApi.Container{
		Name:            fmt.Sprintf("%s-parser", parserType),
		Image:           fmt.Sprintf(parserImage, tag),
		ImagePullPolicy: coreApi.PullIfNotPresent,
		Ports: []coreApi.ContainerPort{
			{
				Name:          "api",
				ContainerPort: port,
			},
		},
		Env: []coreApi.EnvVar{
			{
				Name:  "PARSER_TYPE",
				Value: parserType,
			},
			{
				Name:  "PARSER_PORT",
				Value: fmt.Sprintf("%d", port),
			},
			{
				Name:  "PARSER_HOST",
				Value: fmt.Sprintf("0.0.0.0:%d", port),
			},
			{
				Name:  "MONITOR_HOST",
				Value: fmt.Sprintf("localhost:%d", monitorPort),
			},
			{
				Name:  "REDIS_HOST",
				Value: fmt.Sprintf("localhost:%d", redisPort),
			},
		},
		Resources: coreApi.ResourceRequirements{
			Limits: coreApi.ResourceList{
				"cpu":    resource.MustParse("128m"),
				"memory": resource.MustParse("128Mi"),
			},
		},
	}
}

// createMonitorDeployment creates new Kubernetes deployment for monitor.
// Parsers can be added, mostly for debug purposes, but in production parsers are added with UpdateParsers action.
func createMonitorDeployment(monitorId string, monitorTag string, parserTag string, parsersRequest []string) *appsApi.Deployment {
	var parsers []coreApi.Container
	port := defaultParserPort

	for _, parser := range parsersRequest {
		parsers = append(parsers, parserContainer(parser, port, parserTag))
		port += 1
	}

	deployment := appsApi.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name: monitorId,
			Labels: map[string]string{
				"version": "v1",
				"pType":   "monitor",
			},
		},

		Spec: appsApi.DeploymentSpec{
			Replicas: pointer.Int32Ptr(1),

			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{
					"monitor_name": monitorId,
				},
			},

			Template: coreApi.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						"monitor_name":    monitorId,
						"monitor_version": monitorTag,
					},
				},
				Spec: coreApi.PodSpec{
					Containers: []coreApi.Container{
						monitorContainer(monitorId, monitorTag),
						redisContainer(monitorId),
					},
					Volumes: []coreApi.Volume{
						{
							Name: "cache-storage",
							VolumeSource: coreApi.VolumeSource{
								PersistentVolumeClaim: &coreApi.PersistentVolumeClaimVolumeSource{
									ClaimName: "cache-storage",
								},
							},
						},
						{
							Name: "cache-config",
							VolumeSource: coreApi.VolumeSource{
								ConfigMap: &coreApi.ConfigMapVolumeSource{
									LocalObjectReference: coreApi.LocalObjectReference{Name: "cache-config"},
									DefaultMode:          pointer.Int32Ptr(511),
								},
							},
						},
					},
				},
			},
		},
	}

	// add parsers
	deployment.Spec.Template.Spec.Containers = append(deployment.Spec.Template.Spec.Containers, parsers...)

	return &deployment
}

// createService creates new service for given monitor.
func createService(monitorName string) *coreApi.Service {
	return &coreApi.Service{

		ObjectMeta: metav1.ObjectMeta{
			Name: monitorName,
			Labels: map[string]string{
				"version": "v1",
			},
		},
		Spec: coreApi.ServiceSpec{
			Selector: map[string]string{
				"monitor_name": monitorName,
			},

			Type: coreApi.ServiceTypeNodePort,
			Ports: []coreApi.ServicePort{
				{
					Name:       "http",
					Port:       80,
					TargetPort: intstr.FromInt(8999),
					Protocol:   coreApi.ProtocolTCP,
				},
			},
		},
	}
}

// CreateMonitor creates monitor either empty or with parsers, also creates token for user in Tokenizator.
func CreateMonitor(interactions context.ApplicationContext, username string, parsers []string, dryRun bool) (string, error) {

	// Generate monitor hash.
	monitorId := uuid.NewUUID()
	monitorName := fmt.Sprintf("monitor-%s", monitorId[:uuidSize])

	xlog.Debugf("creating new monitor: %s", monitorName)

	if dryRun {
		xlog.Debug("monitor created in dry-run, not committing")
		return monitorName, nil
	}

	// Prepare clients.
	deploymentClient := interactions.K8SClient().API().AppsV1().Deployments(mainNamespace)
	serviceClient := interactions.K8SClient().API().CoreV1().Services(mainNamespace)

	// Client last version of monitor.
	monitorTag, err := getLastTag(interactions, monitorRepo)

	if err != nil || monitorTag == "" {
		return "", xerrors.New("Could not get last image for monitor")
	}

	xlog.Debugf("got latest monitor tag: %s", monitorTag)

	// Client last version of parser.
	parserTag, err := getLastTag(interactions, parserRepo)

	if err != nil || parserTag == "" {
		return "", xerrors.New("Could not get last image for parser")
	}

	xlog.Debugf("got latest parser tag: %s", parserTag)

	// Create deployment.
	deployment := createMonitorDeployment(monitorName, monitorTag, parserTag, parsers)
	res, err := deploymentClient.Create(interactions, deployment, metav1.CreateOptions{})

	if err != nil {
		return "", err
	}

	xlog.Debugf("created deployment: %s", deployment.Name)

	// Create service.
	service := createService(monitorName)
	serviceRes, err := serviceClient.Create(interactions, service, metav1.CreateOptions{})

	if err != nil {
		xlog.Debugf("deleting deployment %s", monitorName)
		clearErr := deploymentClient.Delete(interactions, monitorName, metav1.DeleteOptions{})
		if clearErr != nil {
			xlog.Errorf("could not delete deployment `%s` while cleaning: %v", monitorName, clearErr)
		}

		return "", err
	}

	xlog.Debugf("created service: %s", serviceRes.Name)

	// Add token with information.
	err = interactions.Tokenizator().CreateMonitor(tokenizator.Token{
		Username:   username,
		Monitor:    monitorName,
		Expiration: tokenizator.TokenizatorTime{Time: time.Now().AddDate(1, 0, 0)},
		IsActive:   true,
	})

	if err != nil {
		xlog.Debugf("deleting deployment %s", monitorName)
		clearErr := deploymentClient.Delete(interactions, monitorName, metav1.DeleteOptions{})
		if clearErr != nil {
			xlog.Errorf("could not delete deployment `%s` while cleaning: %v", monitorName, clearErr)
		}

		xlog.Debugf("deleting service %s", monitorName)
		clearErr = serviceClient.Delete(interactions, monitorName, metav1.DeleteOptions{})
		if clearErr != nil {
			xlog.Errorf("could not delete service `%s` while cleaning: %v", monitorName, clearErr)
		}

		return "", err
	}

	xlog.Debugf("created token for user: %s", username)
	return res.Name, nil
}
