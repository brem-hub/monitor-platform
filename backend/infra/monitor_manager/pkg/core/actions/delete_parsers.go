package actions

import (
	"fmt"
	"github.com/xfxdev/xlog"
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"monitor_manager/pkg/core/app/context"
)

func deleteParsers(monitor *v1.Deployment, parsers []string) ([]string, []string) {
	removedParsers := make([]string, 0)

	skippedParsers := make([]string, 0)

	for _, parser := range parsers {

		parserContainerName := fmt.Sprintf("%s-parser", parser)

		handled := false

		for index, container := range monitor.Spec.Template.Spec.Containers {
			if container.Name != parserContainerName {
				continue
			}

			handled = true

			tmp := monitor.Spec.Template.Spec.Containers[:index]
			monitor.Spec.Template.Spec.Containers = append(tmp, monitor.Spec.Template.Spec.Containers[index+1:]...)
			removedParsers = append(removedParsers, parser)
		}

		if !handled {
			skippedParsers = append(skippedParsers, parser)
			xlog.Warnf("parser %s was not found in monitor parsers", parser)
		}
	}

	return removedParsers, skippedParsers
}

func DeleteParsers(interactions context.ApplicationContext, monitor string, parsers []string) error {
	xlog.Debugf("deleting parsers from monitor: %s", monitor)

	// get monitor deployment

	deploymentClient := interactions.K8SClient().API().AppsV1().Deployments(mainNamespace)

	monitorD, err := deploymentClient.Get(interactions, monitor, metav1.GetOptions{})

	if err != nil {
		return err
	}

	removedParsers, skippedParsers := deleteParsers(monitorD, parsers)

	_, err = deploymentClient.Update(interactions, monitorD, metav1.UpdateOptions{})
	if err != nil {
		return err
	}

	xlog.Debugf("removed %d parsers. Skipped %d parsers", len(removedParsers), len(skippedParsers))

	return nil
}
