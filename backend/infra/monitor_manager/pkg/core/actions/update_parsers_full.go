package actions

import (
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"monitor_manager/pkg/core/app/context"
)

func UpdateParsersFull(interactions context.ApplicationContext, monitor string, parsers map[string]bool) error {

	xlog.Debugf("fully updating parsers for monitor `%s`", monitor)

	// get monitor deployment
	deploymentClient := interactions.K8SClient().API().AppsV1().Deployments(mainNamespace)

	monitorD, err := deploymentClient.Get(interactions, monitor, metav1.GetOptions{})

	if err != nil {
		return err
	}

	parsersToAppend := make([]string, 0)

	parsersToRemove := make([]string, 0)

	for parserName, parserStatus := range parsers {
		if parserStatus {
			parsersToAppend = append(parsersToAppend, parserName)
		} else {
			parsersToRemove = append(parsersToRemove, parserName)
		}
	}

	parserTag, err := getLastTag(interactions, parserRepo)

	if err != nil || parserTag == "" {
		return xerrors.New("Could not get last image for parser")
	}

	xlog.Debugf("got latest parser tag: %s", parserTag)

	removedParsers, skippedParsers := deleteParsers(monitorD, parsersToRemove)

	addedParsers, alreadyExistsParsers, err := updateParsers(monitorD, parsersToAppend, parserTag)

	if len(removedParsers) == 0 && len(addedParsers) == 0 {
		xlog.Warn("no parsers were modified, skipping")
		return nil
	}

	_, err = deploymentClient.Update(interactions, monitorD, metav1.UpdateOptions{})
	if err != nil {
		xlog.Errorf("could not update deployment: %v", err)
		return err
	}

	xlog.Debugf("added parsers: %v, removed parsers: %v, skipped parsers: %v and already exists parsers: %v", addedParsers, removedParsers, skippedParsers, alreadyExistsParsers)
	return nil
}
