package actions

import (
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/utils/strings/slices"
	"monitor_manager/pkg/core/app/context"
	"strconv"
)

var infrastructureContainers = []string{
	"monitor-manager",
	"redis",
}

func updateParsers(monitor *v1.Deployment, parsers []string, parserTag string) ([]string, []string, error) {
	monitorContainers := monitor.Spec.Template.Spec.Containers

	currentParsers := make([]string, 0)

	var port int32 = 0

	for _, container := range monitorContainers {

		// skip infra containers, we need only parsers
		if slices.Contains(infrastructureContainers, container.Name) {
			continue
		}

		currentParsers = append(currentParsers, container.Name)

		for _, env := range container.Env {
			if env.Name != "PARSER_PORT" {
				continue
			}

			tempPort, err := strconv.ParseInt(env.Value, 10, 32)

			if err != nil {
				return nil, nil, fmt.Errorf("could not handle port for parser: %s : %v", container.Name, err)
			}

			parserPort := int32(tempPort)

			if parserPort > port {
				port = parserPort
			}
		}

	}

	if port == 0 {
		port = defaultParserPort
	}

	addedParsers := make([]string, 0)
	skippedParsers := make([]string, 0)

	for _, parser := range parsers {
		port++
		parserName := fmt.Sprintf("%s-parser", parser)
		// skip parsers that are already created
		if slices.Contains(currentParsers, parserName) {
			skippedParsers = append(skippedParsers, parser)
			continue
		}

		monitor.Spec.Template.Spec.Containers = append(monitor.Spec.Template.Spec.Containers,
			parserContainer(parser, port, parserTag),
		)

		addedParsers = append(addedParsers, parser)
	}

	return addedParsers, skippedParsers, nil
}

func UpdateParsers(interactions context.ApplicationContext, monitor string, parsers []string) error {

	xlog.Debugf("updating parsers: %s", monitor)

	// get monitor deployment

	deploymentClient := interactions.K8SClient().API().AppsV1().Deployments(mainNamespace)

	monitorD, err := deploymentClient.Get(interactions, monitor, metav1.GetOptions{})

	if err != nil {
		return err
	}

	parserTag, err := getLastTag(interactions, parserRepo)

	if err != nil || parserTag == "" {
		return xerrors.New("Could not get last image for parser")
	}

	xlog.Debugf("got latest parser tag: %s", parserTag)

	addedParsers, skippedParsers, err := updateParsers(monitorD, parsers, parserTag)

	if err != nil {
		return err
	}

	if len(addedParsers) == 0 {
		xlog.Debug("no update required, skipping")
		return nil
	}

	_, err = deploymentClient.Update(interactions, monitorD, metav1.UpdateOptions{})
	if err != nil {
		xlog.Errorf("could not update deployment: %v", err)
		return err
	}

	xlog.Debugf("added parsers: %v, skipped parsers: %v", addedParsers, skippedParsers)

	return nil
}
