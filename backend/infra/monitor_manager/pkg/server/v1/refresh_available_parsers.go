package v1

import (
	"github.com/xfxdev/xlog"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// swagger:operation GET /v1/parsers/refresh v1_parsers_refresh
//
// Обновить доступные парсеры.
// Менеджер сам обновляет список доступных парсеров каждые 30 минут. Но можно форсированно обновить список парсеров.
//
// ---
// consumes:
// - application/json
//
// responses:
//   '200':
//     description: Список парсеров успешно обновлен
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//		     "status": 200,
//	         "data": null,
//	         "message": "Ok"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func RefreshAvailableParsers(interactions context.ApplicationContext, w http.ResponseWriter, _ *http.Request) {
	err := interactions.ParsersCache().Refresh(interactions)
	if err != nil {
		xlog.Errorf("could not refresh parsers: %v", err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}
	response.OkResponse(w, nil)
}
