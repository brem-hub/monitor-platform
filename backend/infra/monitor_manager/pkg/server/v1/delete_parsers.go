package v1

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/xfxdev/xlog"
	"monitor_manager/pkg/core/actions"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/interactions/parser_cache"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// deleteParsersRequest схема запроса для удаления парсеров из монитора
// swagger:model
type deleteParsersRequest struct {
	// Хэш монитора
	//
	// Required: true
	// Example: monitor-1d3a49
	Monitor string `json:"monitor"`
	// Названия парсеров, которые необходимо удалить
	//
	// Required: true
	// Example: ["basketshop", "lamoda"]
	Parsers []string `json:"parsers"`
}

// swagger:operation DELETE /v1/monitor/parsers v1_monitor_parsers_delete
//
// Удалить парсеры из монитора.
//
// ---
// consumes:
// - application/json
//
// parameters:
// - in: body
//   name: body
//   required: true
//   schema:
//     type: object
//     $ref: "#/definitions/deleteParsersRequest"
// responses:
//   '200':
//     description: Парсеры удалены.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 200,
//	         "data": null,
//	         "message": "Ok"
//         }
//   '400':
//     description: Некорректный ввод
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//           "status": 400,
//	         "data": {
//		       "error": "monitor hash is not set"
//	         },
//	         "message": "Bad request error"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func DeleteParsers(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var req deleteParsersRequest

	err := decoder.Decode(&req)
	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if req.Monitor == "" {
		err = errors.New("monitor hash is not set")
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if len(req.Parsers) == 0 {
		err = errors.New("parsers array is empty")
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	availableParsers := ctx.ParsersCache().GetParsers()

	for _, parser := range req.Parsers {
		if !parser_cache.Contains(availableParsers, parser) {
			err = fmt.Errorf("parser `%s` is not valid", parser)
			xlog.Error(err)
			response.ErrorResponse(w, response.NewBadRequestErrorWithParams(err, map[string]interface{}{
				"available_parsers": availableParsers,
			}))
			return
		}
	}

	err = actions.DeleteParsers(ctx, req.Monitor, req.Parsers)

	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}

	response.OkResponse(w, nil)
}
