package v1

import (
	"encoding/json"
	"github.com/xfxdev/xlog"
	"monitor_manager/pkg/core/actions"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// updateMonitorVersionRequest схема запроса для создания монитора
// swagger:model updateMonitorVersionRequest
type updateMonitorVersionRequest struct {
	// Версия монитора, который должен быть задеплоен. Параметр необходим для отката изменений.
	//
	// Required: false
	// Example: v0.0.5
	Version string `json:"version,omitempty"`
	// Флаг для тестирования ручки. Если указан, то монитор не будет создан в облаке, но monitor hash вернется.
	//
	// Required: false
	// Example: false
	DryRun bool `json:"dryRun,omitempty"`
}

// swagger:operation PUT /v1/monitors v1_monitors_update
//
// Обновить версию монитора для всех текущих мониторов
//
// ---
// consumes:
// - application/json
//
// parameters:
// - in: body
//   name: body
//   required: true
//   schema:
//     type: object
//     $ref: "#/definitions/updateMonitorVersionRequest"
// responses:
//   '200':
//     description: Мониторы обновлены.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 200,
//	         "data": "Updated monitors",
//	         "message": "Ok"
//         }
//   '400':
//     description: Некорректный ввод
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//           "status": 400,
//	         "data": {
//		       "error": "monitor hash is not set"
//	         },
//	         "message": "Bad request error"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func UpdateMonitorVersion(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var req updateMonitorVersionRequest

	err := decoder.Decode(&req)
	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	err = actions.UpdateMonitorVersion(ctx, req.Version, req.DryRun)

	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}

	response.OkResponse(w, "Updated monitors")
}
