package v1

import (
	"fmt"
	"monitor_manager/pkg/core/app/context"
	"net/http"
)

// swagger:operation GET /ping ping
//
// Ping
//
// ---
// responses:
//   '200':
//     description: Pong.
//     examples:
//       application/text:
//         pong
func PingHandler(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "pong")
}
