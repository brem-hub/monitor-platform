package v1

import (
	"encoding/json"
	"errors"
	"github.com/xfxdev/xlog"
	"monitor_manager/pkg/core/actions"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// deleteMonitorRequest схема запроса для удаления монитора
// swagger:model deleteMonitorRequest
type deleteMonitorRequest struct {
	// Хэш монитора
	//
	// Required: true
	// Example: monitor-1d3a49
	Monitor string `json:"monitor"`
}

// swagger:operation DELETE /v1/monitor v1_delete_monitor
//
// Удалить монитор. При успешном удалении монитора, обновляет поле is_active токена для данного монитора на false.
//
// ---
// consumes:
// - application/json
//
// parameters:
// - in: body
//   name: body
//   required: true
//   schema:
//     type: object
//     $ref: "#/definitions/deleteMonitorRequest"
// responses:
//   '200':
//     description: Монитор удален, запись в Tokenizator обновлена.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 200,
//	         "data": null,
//	         "message": "Ok"
//         }
//   '400':
//     description: Некорректный ввод
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//           "status": 400,
//	         "data": {
//		       "error": "monitor hash is not set"
//	         },
//	         "message": "Bad request error"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func DeleteMonitor(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var req deleteMonitorRequest

	err := decoder.Decode(&req)
	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if req.Monitor == "" {
		err = errors.New("monitor hash is not set")
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	err = actions.DeleteMonitor(ctx, req.Monitor)

	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}

	response.OkResponse(w, nil)
}
