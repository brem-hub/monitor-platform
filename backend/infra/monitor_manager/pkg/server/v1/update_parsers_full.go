package v1

import (
	"encoding/json"
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	"monitor_manager/pkg/core/actions"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/interactions/parser_cache"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// updateParsersFullRequest схема запроса для изменения состава парсеров в мониторе
// swagger:model
type updateParsersFullRequest struct {
	// Хэш монитора
	//
	// Required: true
	// Example: monitor-1d3a49
	Monitor string `json:"monitor"`
	// Массив парсеров в формате "name": bool. True для добавления парсера, False для удаления.
	// Если парсер уже существует, он не будет добавлен еще раз.
	// Если парсера нет в мониторе, изменений не произойдет.
	//
	// Required: true
	// Example: {"basketshop": true, "lamoda": false }
	Parsers map[string]bool `json:"parsers"`
}

// swagger:operation PUT /v1/monitor/parsers/full v1_monitor_parsers_update_full
//
// Изменить состав парсеров в мониторе. Является комбинацией добавления и удаления парсеров.
//
// ---
// consumes:
// - application/json
//
// parameters:
// - in: body
//   name: body
//   required: true
//   schema:
//     type: object
//     $ref: "#/definitions/updateParsersFullRequest"
// responses:
//   '200':
//     description: Парсеры обновлены.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 200,
//	         "data": "monitor parsers were updated",
//	         "message": "Ok"
//         }
//   '400':
//     description: Некорректный ввод
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//           "status": 400,
//	         "data": {
//		       "error": "monitor hash is not set"
//	         },
//	         "message": "Bad request error"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func UpdateParsersFull(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var req updateParsersFullRequest

	err := decoder.Decode(&req)
	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if req.Monitor == "" {
		err = xerrors.New("monitor name was not specified")
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if len(req.Parsers) == 0 {
		err = xerrors.New("parsers were not specified")
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	availableParsers := ctx.ParsersCache().GetParsers()

	for parserName := range req.Parsers {
		if !parser_cache.Contains(availableParsers, parserName) {
			err = xerrors.New(fmt.Sprintf("parser `%s` is not valid.", parserName))
			response.ErrorResponse(w, response.NewBadRequestErrorWithParams(err, map[string]interface{}{
				"available_parsers": availableParsers,
			}))
			return
		}
	}

	for _, parser := range availableParsers {
		if _, ok := req.Parsers[parser.Name]; !ok {
			err = xerrors.New(fmt.Sprintf("all parsers must be set. Parser `%s` is not present in request.", parser.Name))
			response.ErrorResponse(w, response.NewBadRequestErrorWithParams(err, map[string]interface{}{
				"available_parsers": availableParsers,
			}))
			return
		}
	}

	err = actions.UpdateParsersFull(ctx, req.Monitor, req.Parsers)

	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}

	response.OkResponse(w, "monitor parsers were updated")
}
