package v1

import (
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// swagger:operation GET /v1/parsers/available v1_parsers_available
//
// Получить доступные парсеры в платформе.
//
// ---
// consumes:
// - application/json
//
// responses:
//   '200':
//     description: Возвращает доступные парсеры.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//		     "status": 200,
//	         "data": {
//		       "parsers": [
//			     {
//				   "name": "basketshop",
//				   "active_version": "latest",
//				   "required_version": "",
//				   "is_enabled": true,
//			     }
//		       ]
//	         },
//	         "message": "Ok"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func GetAvailableParsers(interactions context.ApplicationContext, w http.ResponseWriter, _ *http.Request) {
	response.OkResponse(w, map[string]interface{}{
		"parsers": interactions.ParsersCache().GetParsers(),
	})

}
