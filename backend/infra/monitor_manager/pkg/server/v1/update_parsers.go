package v1

import (
	"encoding/json"
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	"monitor_manager/pkg/core/actions"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/interactions/parser_cache"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// updateParsersRequest схема запроса для добавления парсеров в монитор
// swagger:model
type updateParsersRequest struct {
	// Хэш монитора
	//
	// Required: true
	// Example: monitor-1d3a49
	Monitor string `json:"monitor"`
	// Массив парсеров, которые нужно добавить.
	// Если парсер уже существует, он не будет добавлен еще раз.
	//
	// Required: true
	// Example: ["basketshop", "lamoda"]
	Parsers []string `json:"parsers"`
}

// swagger:operation PUT /v1/monitor/parsers v1_monitor_parsers_update
//
// Добавить парсеры в монитор.
//
// ---
// consumes:
// - application/json
//
// parameters:
// - in: body
//   name: body
//   required: true
//   schema:
//     type: object
//     $ref: "#/definitions/updateParsersRequest"
// responses:
//   '200':
//     description: Парсеры добавлены.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 200,
//	         "data": "monitor parsers were updated",
//	         "message": "Ok"
//         }
//   '400':
//     description: Некорректный ввод
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//           "status": 400,
//	         "data": {
//		       "error": "monitor hash is not set"
//	         },
//	         "message": "Bad request error"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func UpdateParsers(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var req updateParsersRequest

	err := decoder.Decode(&req)
	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if len(req.Parsers) == 0 {
		err = xerrors.New("parsers were not specified")
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	availableParsers := ctx.ParsersCache().GetParsers()

	for _, parser := range req.Parsers {
		if !parser_cache.Contains(availableParsers, parser) {
			err = xerrors.New(fmt.Sprintf("parser `%s` is not valid.", parser))
			xlog.Error(err)
			response.ErrorResponse(w, response.NewBadRequestErrorWithParams(err, map[string]interface{}{
				"available_parsers": availableParsers,
			}))
			return
		}
	}

	err = actions.UpdateParsers(ctx, req.Monitor, req.Parsers)

	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}

	response.OkResponse(w, "monitor parsers were updated")
}
