package v1

import (
	"encoding/json"
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	"monitor_manager/pkg/core/actions"
	"monitor_manager/pkg/core/app/context"
	"monitor_manager/pkg/interactions/parser_cache"
	"monitor_manager/pkg/server/response"
	"net/http"
)

// createMonitorRequest схема запроса для создания монитора
// swagger:model createMonitorRequest
type createMonitorRequest struct {
	// Имя пользователя в платформе.
	//
	// Required: true
	// Example: Dilan
	Username string `json:"username"`
	// Массив парсеров, которые будут добавлены при создании монитора.
	//
	// Required: false
	// Example: []
	Parsers []string `json:"parsers,omitempty"`
	// Флаг для тестирования ручки. Если указан, то монитор не будет создан в облаке, но monitor hash вернется.
	//
	// Required: false
	// Example: false
	DryRun bool `json:"dryRun,omitempty"`
}

// swagger:operation POST /v1/monitor v1_create_monitor
//
// Создать новый монитор для пользователя. При успешном создании монитора, записывает результат в Tokenizator.
//
// ---
// consumes:
// - application/json
//
// parameters:
// - in: body
//   name: body
//   required: true
//   schema:
//     name: body
//     type: object
//     $ref: "#/definitions/createMonitorRequest"
// responses:
//   '200':
//     description: Монитор создан, запись добавлена в Tokenizator.
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 200,
//	         "data": {
//		       "monitor_id": "monitor-0db3cb"
//	         },
//	         "message": "Ok"
//         }
//   '400':
//     description: Некорректный ввод
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//           "status": 400,
//	         "data": {
//		       "error": "username is not set"
//	         },
//	         "message": "Bad request error"
//         }
//   '500':
//     description: Произошла внутренняя ошибка
//     schema:
//       $ref: "#/responses/Response"
//     examples:
//       application/json:
//         {
//	         "status": 500,
//	         "data": {
//		       "error": "Something went wrong"
//	         },
//	         "message": "Internal server error"
//         }
func CreateMonitor(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)

	var req createMonitorRequest

	err := decoder.Decode(&req)
	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if req.Username == "" {
		err = xerrors.New("username is not set")
		response.ErrorResponse(w, response.NewBadRequestError(err))
		return
	}

	if len(req.Parsers) == 0 {
		xlog.Warn("parsers were not specified, creating empty monitor")
	}

	availableParsers := ctx.ParsersCache().GetParsers()

	for _, parser := range req.Parsers {
		if !parser_cache.Contains(availableParsers, parser) {
			err = xerrors.New(fmt.Sprintf("parser `%s` is not valid.", parser))
			response.ErrorResponse(w, response.NewBadRequestErrorWithParams(err, map[string]interface{}{
				"available_parsers": availableParsers,
			}))
			return
		}
	}

	monitorManagerTag, err := actions.CreateMonitor(ctx, req.Username, req.Parsers, req.DryRun)

	if err != nil {
		xlog.Error(err)
		response.ErrorResponse(w, response.NewInternalError(err))
		return
	}

	responseData := map[string]interface{}{
		"monitor_id": monitorManagerTag,
	}

	xlog.Debugf("response: %s", responseData)
	response.OkResponse(w, responseData)

}
