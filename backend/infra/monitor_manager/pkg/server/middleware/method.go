package middleware

import (
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	"k8s.io/utils/strings/slices"
	"monitor_manager/pkg/server/response"
	"net/http"
)

var WHITELIST = []string{"/ping"}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, r *http.Request) {

		blacked := slices.Contains(WHITELIST, r.URL.String())

		if !blacked {
			xlog.Debugf("[req start] [%s] [%s]", r.Method, r.URL.String())
		}

		next.ServeHTTP(writer, r)

		if !blacked {
			xlog.Debugf("[req finish]")
		}
	})
}

func checkMethod(next http.Handler, allowedMethods []string) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, r *http.Request) {

		if !slices.Contains(allowedMethods, r.Method) {
			err := xerrors.New(fmt.Sprintf("Incorrect request method. Expected: %s, Got: %s", allowedMethods, r.Method))
			xlog.Error(err)
			response.ErrorResponse(writer, response.NewBadRequestError(err))
			return
		}
		next.ServeHTTP(writer, r)
	})
}

func Post(next http.Handler) http.Handler {
	return checkMethod(next, []string{"POST"})
}

func Get(next http.Handler) http.Handler {
	return checkMethod(next, []string{"GET"})
}

func Put(next http.Handler) http.Handler {
	return LoggingMiddleware(checkMethod(next, []string{"PUT"}))
}
