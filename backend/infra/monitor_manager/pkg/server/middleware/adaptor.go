package middleware

import (
	"monitor_manager/pkg/core/app/context"
	"net/http"
)

func Adaptor(api func(ctx context.ApplicationContext, w http.ResponseWriter, r *http.Request)) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, r *http.Request) {
		// invoke api
		//api(nil, writer, r)
	})
}
