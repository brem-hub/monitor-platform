package middleware

import (
	"encoding/json"
	"github.com/go-openapi/loads"
	"github.com/xfxdev/xlog"
	"net/http"
)

import "github.com/go-openapi/runtime/middleware"

func SwaggerMiddleware(filePath string, apiUrl string) func(http.Handler) http.Handler {

	specDoc, err := loads.Spec(filePath)

	if err != nil {
		xlog.Warnf("Could not initiate swagger: %v", err)
		return nil
	}

	data, err := json.MarshalIndent(specDoc.Spec(), "", "  ")
	if err != nil {
		xlog.Warnf("Could not marshal swagger spec: %v", err)
		return nil
	}

	return func(next http.Handler) http.Handler {
		handler := middleware.SwaggerUI(middleware.SwaggerUIOpts{
			BasePath: "/",
			SpecURL:  "swagger.json",
			Path:     apiUrl,
		}, next)

		return middleware.Spec("", data, handler)
	}
}
