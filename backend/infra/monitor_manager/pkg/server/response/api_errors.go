package response

import (
	"net/http"
)

type ApiError interface {
	StatusCode() int
	GetError() error
	Error() string
	Params() interface{}
	ErrorDescription() string
}

type BaseError struct {
	err    error
	params interface{}
}

func (e BaseError) StatusCode() int { return http.StatusMultiStatus }

func (e BaseError) GetError() error { return e.err }

func (e BaseError) Error() string { return e.err.Error() }

func (e BaseError) Params() interface{} { return e.params }

func (e BaseError) ErrorDescription() string { return "" }

type BadRequestError struct {
	BaseError
}

func NewBadRequestErrorWithParams(err error, params map[string]interface{}) *BadRequestError {
	return &BadRequestError{BaseError{err: err, params: params}}
}

func NewBadRequestError(err error) *BadRequestError {
	return &BadRequestError{BaseError{err: err, params: nil}}
}

func (e BadRequestError) StatusCode() int { return http.StatusBadRequest }

func (e BadRequestError) ErrorDescription() string { return "Bad request error" }

type InternalError struct {
	BaseError
}

func NewInternalError(err error, params ...interface{}) *InternalError {
	return &InternalError{BaseError{err: err, params: params}}
}

func (e InternalError) StatusCode() int { return http.StatusInternalServerError }

func (e InternalError) ErrorDescription() string { return "Internal server error" }

type NotFoundError struct {
	BaseError
}

func NewNotFoundError(params ...interface{}) *NotFoundError {
	return &NotFoundError{BaseError{params: params}}
}

func NotFound(path string) *NotFoundError {
	return NewNotFoundError("path", path)
}

func (e NotFoundError) StatusCode() int { return http.StatusNotFound }

func (e NotFoundError) ErrorDescription() string { return "Path not found" }

type MethodNotAllowedError struct {
	BaseError
}

func NewMethodNotAllowedError(params ...interface{}) *MethodNotAllowedError {
	return &MethodNotAllowedError{BaseError{err: nil, params: params}}
}

func MethodNotAllowed(path string, method string) *MethodNotAllowedError {
	return NewMethodNotAllowedError(map[string]string{"path": path, "method": method})
}

func (e MethodNotAllowedError) StatusCode() int { return http.StatusMethodNotAllowed }

func (e MethodNotAllowedError) ErrorDescription() string { return "Method is not allowed" }
