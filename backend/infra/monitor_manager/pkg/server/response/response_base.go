package response

import (
	"github.com/xfxdev/xlog"
	"k8s.io/apimachinery/pkg/util/json"
	"net/http"
)

// swagger:response
type Response struct {
	// Статус код ответа
	//
	// in: body
	// required: true
	Status int `json:"status"`
	// Данные ответа.
	//
	// in: body
	// required: true
	Data interface{} `json:"data"`
	// Сообщение о статусе ответа.
	//
	// in: body
	// Required: true
	Message string `json:"message,omitempty"`
}

func writeRequest(w http.ResponseWriter, resp Response) error {
	bytes, err := json.Marshal(resp)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(resp.Status)
	_, err = w.Write(bytes)
	return err
}

func OkResponse(w http.ResponseWriter, body interface{}) {
	err := writeRequest(w, Response{
		Status:  http.StatusOK,
		Data:    body,
		Message: "Ok",
	})

	if err != nil {
		xlog.Errorf("Could not write error: %v", err)
	}
}

func ErrorResponse(w http.ResponseWriter, err error) {

	resp := Response{
		Status: http.StatusInternalServerError,
		Data:   err,
		//Message: err.Error(),
	}

	if apiErr, ok := err.(ApiError); ok {
		resp.Status = apiErr.StatusCode()

		resp.Data = map[string]interface{}{}

		if apiErr.GetError() != nil {
			resp.Data.(map[string]interface{})["error"] = apiErr.Error()
		}

		if apiErr.Params() != nil {
			resp.Data.(map[string]interface{})["params"] = apiErr.Params()
		}

		resp.Message = apiErr.ErrorDescription()
	}

	err = writeRequest(w, resp)
	if err != nil {
		xlog.Errorf("Could not write request: %v", err)
	}
}
