package utils

import "net/url"

func RelativePathToUrl(path string) string {
	u, _ := url.Parse(path)

	if !u.IsAbs() {
		if len(u.String()) != 0 {
			return u.String()[1:]
		}
	}

	return u.String()
}

type Notify struct {
	ch chan interface{}
}

func NotifyOnce() *Notify {
	return &Notify{ch: make(chan interface{})}
}

func (n *Notify) Notify() {
	select {
	case n.ch <- struct{}{}:
	default:
	}
}

func (n *Notify) Wait() {
	<-n.ch
}

func (n *Notify) Done() <-chan interface{} {
	return n.ch
}
