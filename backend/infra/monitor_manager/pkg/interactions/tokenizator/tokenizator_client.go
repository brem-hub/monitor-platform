package tokenizator

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	"net/http"
	"net/url"
	"os"
	"path"
	"strings"
	"time"
)

const (
	defaultTokenizatorAPI = "http://tokenizator"
	layout                = "2006-01-02 15:04:05"
)

type TokenizatorBackend struct {
	client  *http.Client
	apiPath string
}

type TokenizatorTime struct {
	time.Time
}

func (t *TokenizatorTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	if s == "null" {
		return
	}
	t.Time, err = time.Parse(layout, s)
	return
}

func (t TokenizatorTime) MarshalJSON() ([]byte, error) {
	if t.Time.IsZero() {
		return nil, nil
	}
	return []byte(fmt.Sprintf("\"%s\"", t.Time.Format(layout))), nil
}

type Token struct {
	Username   string          `json:"username"`
	Monitor    string          `json:"monitor"`
	Expiration TokenizatorTime `json:"expiration"`
	IsActive   bool            `json:"is_active"`
}

type response struct {
	Message    string                 `json:"message"`
	Token      Token                  `json:"token"`
	Params     map[string]interface{} `json:"params"`
	StatusCode int                    `json:"status"`
}

func handleResponse(resp *http.Response, parserResponse response) error {

	if resp.StatusCode == http.StatusOK {
		return nil
	}

	if resp.StatusCode >= http.StatusInternalServerError {
		return xerrors.New(fmt.Sprintf("tokenizator internal error occurred: %s", resp.Status))
	}

	if resp.StatusCode <= http.StatusBadRequest {
		return xerrors.New(fmt.Sprintf("tokenizator bad request: %s, %s", resp.Status, parserResponse.Message))
	}

	return xerrors.New(fmt.Sprintf("tokenizator unknown error: [%d] %s", resp.StatusCode, resp.Status))
}

func (b *TokenizatorBackend) GetByUser(username string) (*Token, error) {
	body, _ := json.Marshal(map[string]string{"username": username})

	apiUrl, err := url.Parse(b.apiPath)

	if err != nil {
		return nil, err
	}

	apiUrl.Path = path.Join(apiUrl.Path, "tokenizator")

	r, err := http.NewRequest("GET", apiUrl.String(), bytes.NewBuffer(body))

	if err != nil {
		xlog.Error("Could not create new request")
		return nil, err
	}

	resp, err := b.client.Do(r)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(resp.Body)

	var res response

	err = decoder.Decode(&res)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(fmt.Sprintf("tokenizator error: %s", res.Message))
	}

	return &res.Token, nil
}

func (b *TokenizatorBackend) GetByMonitor(monitor string) (*Token, error) {
	body, _ := json.Marshal(map[string]string{"monitor": monitor})

	apiUrl, err := url.Parse(b.apiPath)

	if err != nil {
		return nil, err
	}

	apiUrl.Path = path.Join(apiUrl.Path, "tokenizator")

	r, err := http.NewRequest("GET", apiUrl.String(), bytes.NewBuffer(body))

	if err != nil {
		xlog.Error("Could not create new request")
		return nil, err
	}

	resp, err := b.client.Do(r)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(resp.Body)

	var res response

	err = decoder.Decode(&res)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, xerrors.New(fmt.Sprintf("tokenizator error: %s", res.Message))
	}

	return &res.Token, nil
}

func (b *TokenizatorBackend) Update(token Token) error {

	request, err := json.Marshal(map[string]interface{}{
		"username":   token.Username,
		"monitor":    token.Monitor,
		"expiration": token.Expiration,
		"is_active":  token.IsActive,
	})

	if err != nil {
		xlog.Errorf("could not create request: %v", err)
		return err
	}

	apiUrl, err := url.Parse(b.apiPath)

	if err != nil {
		return err
	}

	apiUrl.Path = path.Join(apiUrl.Path, "tokenizator")

	r, err := http.NewRequest("PUT", apiUrl.String(), bytes.NewReader(request))
	if err != nil {
		xlog.Errorf("could not create new request: %v", err)
		return err
	}

	httpResponse, err := b.client.Do(r)

	if err != nil {
		xlog.Errorf("could not make request to tokenizator: %v", err)
		return err
	}

	decoder := json.NewDecoder(httpResponse.Body)

	var apiResponse response

	err = decoder.Decode(&apiResponse)

	if err != nil {
		xlog.Errorf("could not parse request. Got: %v", err)
		return err
	}

	return handleResponse(httpResponse, apiResponse)
}

func (b *TokenizatorBackend) CreateMonitor(token Token) error {

	request, err := json.Marshal(map[string]interface{}{
		"username":   token.Username,
		"monitor":    token.Monitor,
		"expiration": token.Expiration,
		"is_active":  token.IsActive,
	})

	if err != nil {
		xlog.Errorf("could not create request: %v", err)
		return err
	}

	apiUrl, err := url.Parse(b.apiPath)

	if err != nil {
		return err
	}

	apiUrl.Path = path.Join(apiUrl.Path, "tokenizator")

	r, err := http.NewRequest("POST", apiUrl.String(), bytes.NewReader(request))
	if err != nil {
		xlog.Errorf("could not create new request: %v", err)
		return err
	}

	httpResponse, err := b.client.Do(r)

	if err != nil {
		xlog.Errorf("could not make request to tokenizator: %v", err)
		return err
	}

	decoder := json.NewDecoder(httpResponse.Body)

	var apiResponse response

	err = decoder.Decode(&apiResponse)

	if err != nil {
		xlog.Errorf("could not parse request. Got: %v", err)
		return err
	}

	return handleResponse(httpResponse, apiResponse)
}

func NewTokenizatorClient() *TokenizatorBackend {
	var tokenizatorAPI string

	if tokenizatorAPI = os.Getenv("TOKENIZATOR_API"); tokenizatorAPI == "" {
		tokenizatorAPI = defaultTokenizatorAPI
	}

	return &TokenizatorBackend{client: &http.Client{}, apiPath: tokenizatorAPI}
}
