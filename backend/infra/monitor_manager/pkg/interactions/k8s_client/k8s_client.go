package k8s_client

import (
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"os"
)

const KUBE_CONFIG = "./config/kubeconfig"

type KubeBackend struct {
	clientSet *kubernetes.Clientset
}

func (k *KubeBackend) API() *kubernetes.Clientset {
	return k.clientSet
}

func NewKubeClient() (*KubeBackend, error) {
	var kubeConfig string
	if os.Getenv("KUBERNETES_SERVICE_HOST") != "" {
		// Executing in the cluster
		kubeConfig = ""
	} else if kubeConfig = os.Getenv("KUBE_CONFIG"); kubeConfig == "" {
		return nil, xerrors.New("config variable is not set, exiting")
	}

	xlog.Infof("got config `%s`", kubeConfig)

	// temp
	kubeConfig = KUBE_CONFIG
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)

	if err != nil {
		return nil, err
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return &KubeBackend{clientSet: clientSet}, nil
}
