package container_registry

//go:generate protoc --go_out=. --go_opt=paths=source_relative -I=../../../grpc/cloudapi/ -I=../../../grpc/cloudapi/third_party/googleapis ../../../grpc/cloudapi/yandex/cloud/containerregistry/v1/*.proto

import (
	"crypto/x509"
	api "github.com/yandex-cloud/go-genproto/yandex/cloud/containerregistry/v1"
	"golang.org/x/oauth2"
	"golang.org/x/xerrors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/oauth"
	"monitor_manager/pkg/interactions/yc_api_token"
)

const (
	defaultAPI = "container-registry.api.cloud.yandex.net:443"
)

type RegistryBackend struct {
	client    api.ImageServiceClient
	authToken string
}

func (r *RegistryBackend) Client() api.ImageServiceClient {
	return r.client
}

func connection(oauthToken string) (api.ImageServiceClient, error) {
	token := &oauth2.Token{
		AccessToken: oauthToken,
		TokenType:   "Bearer",
	}

	creds := oauth.NewOauthAccess(token)

	pool, err := x509.SystemCertPool()
	if err != nil {
		return nil, err
	}

	tlsCreds := credentials.NewClientTLSFromCert(pool, "")

	client, err := grpc.Dial(defaultAPI, grpc.WithTransportCredentials(tlsCreds), grpc.WithPerRPCCredentials(creds))

	if err != nil {
		return nil, err
	}

	service := api.NewImageServiceClient(client)

	return service, nil
}

func (b *RegistryBackend) RefreshConnection(cache *yc_api_token.YCApiBackend) error {
	var err error
	if b.client, err = connection(cache.IAMToken); err != nil {
		return xerrors.New("Could not refresh connection")
	}

	return nil
}

func NewRegistryClient() *RegistryBackend {

	return &RegistryBackend{}
}
