package parser_cache

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"monitor_manager/pkg/core/app/config"
)

type Parser struct {
	Name            string `json:"name"`
	ActiveVersion   string `json:"active_version"`
	RequiredVersion string `json:"required_version"`
	IsEnabled       bool   `json:"is_enabled"`
}

type ParserCache struct {
	availableParsers []Parser
	con              *pgxpool.Pool
}

func (p *ParserCache) GetParsers() []Parser {
	return p.availableParsers
}

func (p ParserCache) Contains(parserName string) bool {
	for _, parser := range p.availableParsers {
		if parser.Name == parserName {
			return true
		}
	}
	return false
}

func Contains(parsers []Parser, parserName string) bool {
	for _, parser := range parsers {
		if parser.Name == parserName {
			return true
		}
	}
	return false
}

func (p *ParserCache) Refresh(ctx context.Context) error {

	parsers := make([]Parser, 0)

	rows, err := p.con.Query(ctx, "select * from parsers")

	if err != nil {
		return err
	}

	defer rows.Close()

	for rows.Next() {
		values, err := rows.Values()

		if err != nil {
			return err
		}

		name := values[0].(string)
		activeVersion := values[1].(string)
		requiredVersion := values[2].(string)
		isActive := values[3].(bool)

		parsers = append(parsers, Parser{
			Name:            name,
			ActiveVersion:   activeVersion,
			RequiredVersion: requiredVersion,
			IsEnabled:       isActive,
		})

	}

	if rows.Err() != nil {
		return rows.Err()
	}

	p.availableParsers = parsers

	return nil
}

func NewParserCache(config config.Config) (*ParserCache, error) {
	// Example: postgres://jack:secret@pg.example.com:5432/mydb?sslmode=verify-ca&pool_max_conns=10
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%d/%s", config.Postgres.User, config.Postgres.Password, config.Postgres.Url, config.Postgres.Port, config.Postgres.Database)

	con, err := pgxpool.Connect(context.Background(), connectionString)

	if err != nil {
		return nil, err
	}

	return &ParserCache{con: con}, nil
}
