package yc_api_token

import (
	"context"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"github.com/golang-jwt/jwt/v4"
	"io"
	"io/ioutil"
	"monitor_manager/pkg/core/app/config"
	"net/http"
	"strings"
	"sync"
	"time"
)

type YCApiBackend struct {
	IAMToken string

	AuthorizationToken string

	keyID            string
	serviceAccountID string
	keyFile          string

	keyMutex sync.Mutex
}

func NewTokenCache(config config.Config) (*YCApiBackend, error) {
	return &YCApiBackend{
		keyID:            config.Authorization.Key,
		serviceAccountID: config.Authorization.Account,
		keyFile:          config.Authorization.Pk,
	}, nil

}

func (c *YCApiBackend) GetIAMToken() string {
	c.keyMutex.Lock()
	defer c.keyMutex.Unlock()
	return c.IAMToken
}

func (c *YCApiBackend) loadPrivateKey() (*rsa.PrivateKey, error) {

	data, err := ioutil.ReadFile(c.keyFile)
	if err != nil {
		return nil, err
	}

	rsaPrivateKey, err := jwt.ParseRSAPrivateKeyFromPEM(data)

	if err != nil {
		return nil, err
	}

	return rsaPrivateKey, nil
}

func (c *YCApiBackend) getIAMToken() (string, error) {
	c.keyMutex.Lock()
	defer c.keyMutex.Unlock()

	jot, err := c.signedToken()

	if err != nil {
		return "", err
	}

	//fmt.Println(jot)
	resp, err := http.Post(
		"https://iam.api.cloud.yandex.net/iam/v1/tokens",
		"application/json",
		strings.NewReader(fmt.Sprintf(`{"jwt":"%s"}`, jot)),
	)
	if err != nil {
		return "", err

	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)
	if resp.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(resp.Body)
		panic(fmt.Sprintf("%s: %s", resp.Status, body))
	}
	var data struct {
		IAMToken string `json:"iamToken"`
	}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		panic(err)
	}
	return data.IAMToken, nil
}

func (c *YCApiBackend) signedToken() (string, error) {

	claims := jwt.RegisteredClaims{
		Issuer:    c.serviceAccountID,
		ExpiresAt: jwt.NewNumericDate(time.Now().UTC().Add(1 * time.Hour)),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		NotBefore: jwt.NewNumericDate(time.Now()),
		Audience:  []string{"https://iam.api.cloud.yandex.net/iam/v1/tokens"},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodPS256, claims)
	token.Header["kid"] = c.keyID

	privateKey, err := c.loadPrivateKey()

	if err != nil {
		return "", err
	}

	signed, err := token.SignedString(privateKey)
	if err != nil {
		return "", err
	}
	return signed, nil
}

func (c *YCApiBackend) Refresh(_ context.Context) error {
	var err error

	if c.IAMToken, err = c.getIAMToken(); err != nil {
		return err
	}

	return nil
}
