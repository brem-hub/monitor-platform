#!/bin/bash

#(kubectl -n monitor-boi get deployments.apps | grep monitor)


echo 'Deleting monitor deployments'

kubectl -n monitor-boi delete deployments.apps \
    $(kubectl -n monitor-boi get deployments.apps \
        | grep monitor | grep -v monitor-manager  \
        |  awk '{split($0, a, " "); print a[1]}')


echo 'Deleting services'

kubectl -n monitor-boi delete service \
    $(kubectl -n monitor-boi get service \
        | grep monitor | grep -v monitor-manager  \
        |  awk '{split($0, a, " "); print a[1]}')

