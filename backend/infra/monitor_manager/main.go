package main

import (
	"monitor_manager/pkg/commands"
)

func main() {
	if err := commands.MainCmd.Execute(); err != nil {
		panic(err)
	}
}
