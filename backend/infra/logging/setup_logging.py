import importlib
import sys
import logging
from logging import Handler
from typing import Optional

from backend.infra.logging.logging_config import LOGGING

_logging_keys = [
    'version',
    'debug',
    'formatters',
    'handlers',
]

default_formatter_config = {
    'default': '[%(name)s][%(levelname)s][%(asctime)s] !!!! %(message)s'
}

default_handler_config = {
    'default': {
        'class': 'logging.StreamHandler',
        'out': 'stdout',
        'formatter': 'default'
    }
}

_streams = {
    'stdin': sys.stdin,
    'stdout': sys.stdout,
    'stderr': sys.stderr
}


_handlers = {}


def create_handler(name: str, config: dict) -> Optional[Handler]:
    if any([required_field not in config for required_field in ['formatter', 'class']]):
        return None

    if name in _handlers:
        return _handlers[name]

    module_name, class_name = config['class'].split('.')
    module = importlib.import_module(module_name)
    ctor = getattr(module, class_name)

    handler: Handler = ctor()

    if class_name == 'StreamHandler':
        if 'out' in config:
            stream = _streams.setdefault(config['out'], sys.stderr)
            handler.setStream(stream)

    formatter_name = config['formatter']
    if formatter_name not in LOGGING['formatters']:
        formatter_name = 'default'

    formatter = logging.Formatter(LOGGING['formatters'][formatter_name])
    handler.setFormatter(formatter)

    _handlers[name] = handler

    return handler


def setup_logging():
    # parse handlers.
    if 'formatters' not in LOGGING:
        logging.error(
            'config does not have formatters section, using default formatter for all.')
        LOGGING['formatters'] = default_formatter_config
    elif 'default' not in LOGGING['formatters']:
        LOGGING['formatters'].update(default_formatter_config)
    else:
        logging.debug('default logging formatter was overwritten')

    if 'handlers' not in LOGGING:
        logging.error(
            'config does not have handlers section, using default logger for all.')
        LOGGING['handlers'] = default_handler_config

    if 'default' not in LOGGING['handlers']:
        LOGGING['handlers'].update(default_handler_config)

    for entity_name, entity in LOGGING.items():
        if entity_name in _logging_keys:
            continue

        logger = logging.getLogger(entity_name)

        # clear old handlers.
        logger.handlers.clear()

        # parse logging level.
        level = logging.getLevelName(entity.setdefault('level', 'DEBUG'))

        if not isinstance(level, int):
            logging.error(
                f'could not parse logger level for {entity_name}, using DEBUG')
            level = logging.DEBUG

        logger.setLevel(level)

        for name in entity.setdefault('handlers', []):
            logger.addHandler(create_handler(name, LOGGING['handlers'][name]))


setup_logging()


logging.getLogger('aiohttp.access').setLevel(logging.CRITICAL)
# logging.getLogger('aiohttp.server').setLevel(logging.WARNING)
