
LOGGING = {
    'version': 1,
    'debug': False,

    'formatters': {
        'verbose': '[%(name)s][%(levelname)s][%(asctime)s] %(message)s',
    },

    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'out': 'stdout',
            'formatter': 'verbose'
        },

        'kafka': {
            # TODO
        },
    },

    'api': {
        'handlers': ['console'],
        'level': 'DEBUG',
    },

    'mw': {
        'handlers': ['console'],
        'level': 'DEBUG',
    },

    'process_manager': {
        'handlers': ['console'],
        'level': 'DEBUG',
    },

    'parser': {
        'handlers': ['console'],
        'level': 'DEBUG'
    },

    # 'root': {
    #     'handlers': ['console'],
    #     'level': 'DEBUG'
    # },

    # TODO: add stderr handler for errors.
    'aiohttp.server': {
        'handlers': ['console'],
        'level': 'DEBUG'
    }

}
