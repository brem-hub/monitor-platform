import click
from aiohttp import web
from backend.infra.base_server.config.config import parse_host
from backend.infra.tokenizator.application.application import Application
from backend.infra.base_server.commands.root import root_cmd
from backend.infra.base_server.config.config import get_version, GLOBAL_CONTEXT

GLOBAL_CONTEXT.ApplicationName = "Tokenizator"
GLOBAL_CONTEXT.Application = Application
GLOBAL_CONTEXT.ApplicationVersion = get_version()

@click.command(name='runserver', help='Run Tokenizator')
@click.option('--host', default='0.0.0.0:8080', help='Service host', required=False)
@click.option('--hot-swagger', is_flag=True, default=False, help='Reload swagger at startup', required=False)
@click.option('--swagger-path', default='./package/swagger.json', help='Swagger specification path', required=False)
def runserver(host: str, hot_swagger: bool, swagger_path: str):
    config = parse_host(host)
    application = Application(hot_swagger=hot_swagger, swagger_path=swagger_path)
    web.run_app(application, host=config.host, port=config.port)


root_cmd.add_command(runserver)


if __name__ == '__main__':
    root_cmd()
