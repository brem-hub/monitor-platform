from aiohttp.web import Response


def json_requirements(*args):
    """Декоратор для Handler'ов post запросов. Требует, чтобы каждый элемент
    args присутствовал в request.json.

    :param args: перечисление требуемых параметров в виде набора строк.
    :return: Response(status=500) в случае отсутвия хотя бы одного из аргументов. Или результат исполнения
    Handler'а в случае их наличия.
    """

    def decorator(func):
        async def wrapper(self):
            request = self.request
            satisfactory_list = []

            if not request.body_exists:
                return await func(self)

            query = await request.json()
            for arg in args:
                if arg not in query:
                    satisfactory_list.append(arg)
            if len(satisfactory_list):
                return Response(text=f'Not enough parameters in POST request JSON! Missing {str(satisfactory_list)}',
                                status=500)
            return await func(self)

        return wrapper

    return decorator
