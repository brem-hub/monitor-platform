import datetime
from aiohttp import web

from backend.infra.base_server.app.base_server import BaseApplication
from backend.infra.tokenizator.application.views import Tokenizator


class Application(BaseApplication):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_time = datetime.datetime.now()

    def routes(self):
        return [
            web.get('/tokenizator', Tokenizator, name='get_token'),
            web.post('/tokenizator', Tokenizator, name='create_token'),
            web.put('/tokenizator', Tokenizator, name='update_token'),
            web.delete('/tokenizator', Tokenizator, name='delete_token'),
        ]
