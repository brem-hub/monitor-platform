import http
import json
import logging
from dataclasses import dataclass
from backend.infra.tokenizator.application.model import User, DATABASE
from aiohttp import web
from psycopg2 import Error
from rest_framework import status
from backend.infra.tokenizator.utils.network_utils import json_requirements


@dataclass
class TokenizerResponse(web.Response):
    def __init__(self, message, status, token=None, **kwargs):
        data = {
            'message': message,
            'status': status,
            'token': token,
            'params': kwargs
        }
        super().__init__(text=json.dumps(data), status=status)


class Tokenizator(web.View):
    logger = logging.getLogger('api')

    async def get(self):
        """
        Позволяет получить имя пользователя с его монитором и сроком
        подписки.

        ---
        consumes:
          - application/json
        summary: Получить токен пользователя по имени пользователя или по хэшу его монитора.
        parameters:
          - in: body
            name: body
            description: >
              username - логин пользователя. \n
              monitor - хэш монитора. \n\n
              Необходимо, чтобы было заполнено хотя бы 1 поле. Приоритет у username.
            required: true
            schema:
              type: object
              properties:
                username:
                  type: string
                  example: dilan-champion
                monitor:
                  type: string
                  example: monitor-1c35ba
        responses:
          200:
            description: Токен найден
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                   "message": "Got the user - test-user.",
                   "status": 200,
                   "token": {
                   "username": "test-user",
                   "monitor": "monitor-00f4e2",
                   "expiration": "2023-04-17 16:25:19",
                   "is_active": false
                   },
                   "params": {}
                }
          400:
            description: Некорректный ввод
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Required JSON parameters are not set. [username | monitor]",
                  "status": 400,
                  "token": null,
                  "params": {}
                }
          500:
            description: Произошла внутренняя ошибка
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Internal error occurred",
                  "status": 500,
                  "token": null,
                  "params": {}
                }
        """
        request = self.request

        if not request.body_exists:
            return TokenizerResponse(message='Got empty request body',
                                     status=http.HTTPStatus.BAD_REQUEST)

        data = await request.json()

        name = data.get('username')
        monitor = data.get('monitor')

        if not any((name, monitor)):
            return TokenizerResponse(message='Required JSON parameters are not set. [username | monitor]',
                                     status=http.HTTPStatus.BAD_REQUEST)

        try:
            with DATABASE:
                if name:
                    user = User.get_or_none(User.username == name)
                    if not user:
                        self.logger.error(
                            f"Failed get data. Could not find such a user - \"{name}\".")
                        return TokenizerResponse(message=f"Failed get data. Could not find such a user - \"{name}\".",
                                                 status=http.HTTPStatus.NOT_FOUND)
                elif monitor:
                    user = User.get_or_none(User.monitor == monitor)
                    if not user:
                        self.logger.error(
                            f"Failed get data. Could not find such a monitor - \"{monitor}\".")
                        return TokenizerResponse(
                            message=f"Failed get data. Could not find such a monitor - \"{monitor}\".",
                            status=http.HTTPStatus.NOT_FOUND)

        except (Exception, Error) as err:
            message = f'Failed get data. Error: {err}'
            self.logger.error(message)
            return TokenizerResponse(message=message, status=http.HTTPStatus.INTERNAL_SERVER_ERROR)

        token = user.__data__
        token['expiration'] = str(token['expiration'])
        self.logger.debug(f"Got the user - \"{name}\".")
        return TokenizerResponse(message=f"Got the user - \"{name}\".",
                                 token=token,
                                 status=status.HTTP_200_OK)

    @json_requirements('username', 'monitor', 'expiration', 'is_active')
    async def post(self):
        """
        Позволяет создавать нового пользователя в базе.

        ---
        consumes:
          - application/json
        summary: Создать токен для пользователя и его монитора.
        parameters:
          - in: body
            name: body
            description: >
             Токен для записи в токенизатор. \n\n
             username - логин пользователя. Должен быть уникальным. \n
             monitor - хэш монитора. Одному пользователю пренадлежит один монитор. \n
             expiration - дата, когда доступ пользователя к монитору будет просрочен. \n
             is_active - флаг, указывающий активен ли монитор.
            required: true
            schema:
              type: object
              properties:
                username:
                  type: string
                  example: dilan-champion
                monitor:
                  type: string
                  example: monitor-1c35ba
                expiration:
                  type: dateTime
                  example: "2022-04-24 MSK"
                is_active:
                  type: boolean
                  example: False
        responses:
          200:
            description: Токен создан
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Созданный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "The user - Dilan was added to the data base.",
                  "status": 200,
                  "token": {
                  "username": "asd",
                  "monitor": "monitorxx",
                  "expiration": "2022-03-11 MSK",
                  "is_active": true
                  },
                  "params": {}
                }
          400:
            description: Некорректный ввод
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Required JSON parameters are not set.",
                  "status": 400,
                  "token": null,
                  "params": {}
                }
          500:
            description: Произошла внутренняя ошибка
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Internal error occurred",
                  "status": 500,
                  "token": null,
                  "params": {}
                }
        """
        request = self.request

        if not request.body_exists:
            return TokenizerResponse(
                message='Got empty request body', status=http.HTTPStatus.BAD_REQUEST
            )

        data = await request.json()

        name = data['username']
        monitor = data['monitor']
        expiration = data['expiration']
        is_active = data['is_active']

        try:
            with DATABASE:
                user = User.get_or_none(User.username == name)
                if user:
                    message = f"Failed to create the user - \"{name}\". The user already exists."
                    self.logger.error(message)
                    return TokenizerResponse(message=message,
                                             status=status.HTTP_400_BAD_REQUEST)
                user = User.get_or_none(User.monitor == monitor)
                if user:
                    message = f"Failed to create the user - \"{name}\". The monitor - \"{monitor}\" already exists."
                    self.logger.error(message)
                    return TokenizerResponse(message=message,
                                             status=status.HTTP_400_BAD_REQUEST)
                User.create(username=name, monitor=monitor,
                            expiration=expiration, is_active=is_active)
        except (Exception, Error) as err:
            message = f"Failed to create the user - \"{name}\". Error: {err}"
            self.logger.error(message)
            return TokenizerResponse(message=message,
                                     status=http.HTTPStatus.INTERNAL_SERVER_ERROR)

        self.logger.debug(f"The user - \"{name}\" was added to the data base.")
        return TokenizerResponse(message=f"The user - \"{name}\" was added to the data base.",
                                 token={'username': name,
                                        'monitor': monitor,
                                        'expiration': expiration,
                                        'is_active': is_active},
                                 status=status.HTTP_200_OK)

    @json_requirements('username')
    async def delete(self):
        """Позволяет удалять пользователя по имени из базы.

        ---
        consumes:
          - application/json
        summary: Удалить токен пользователя.
        parameters:
          - in: body
            name: body
            description: >
             username - логин пользователя. Должен быть уникальным. \n
            required: true
            schema:
              type: object
              properties:
                username:
                  type: string
                  example: dilan-champion
        responses:
          200:
            description: Токен удален
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Созданный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "The user - Dilan was removed.",
                  "status": 200,
                  "token": {
                    "username": "qwerty123"
                  },
                  "params": {}
                }
          400:
            description: Некорректный ввод
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Required JSON parameters are not set.",
                  "status": 400,
                  "token": null,
                  "params": {}
                }
          500:
            description: Произошла внутренняя ошибка
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Internal error occurred",
                  "status": 500,
                  "token": null,
                  "params": {}
                }
        """
        request = self.request

        if not request.body_exists:
            return TokenizerResponse(message='Got empty request body',
                                     status=http.HTTPStatus.BAD_REQUEST)

        data = await request.json()

        name = data['username']

        try:
            with DATABASE:
                user = User.get_or_none(User.username == name)
                if not user:
                    self.logger.error(
                        f"Failed delete the user - \"{name}\". Could not find such a user - \"{name}\".")
                    return TokenizerResponse(
                        message=f"Failed delete the user - \"{name}\". Could not find such a user - \"{name}\".",
                        status=http.HTTPStatus.NOT_FOUND)
                else:
                    user.delete_instance()
        except (Exception, Error) as err:
            message = f"Failed to delete the user - \"{name}\". Error: {err}"
            self.logger.error(message)
            return TokenizerResponse(message=message,
                                     status=http.HTTPStatus.INTERNAL_SERVER_ERROR)

        self.logger.debug(f"The user - \"{name}\" was removed.")
        return TokenizerResponse(message=f"The user - \"{name}\" was removed.",
                                 token={'username': name},
                                 status=status.HTTP_200_OK)

    @json_requirements('username', 'expiration', 'is_active')
    async def put(self):
        """Позволяет изменять дату подписки или ее статус.

        ---
        consumes:
          - application/json
        summary: Обновить токен пользователя.
        parameters:
          - in: body
            name: body
            description: >
             username - логин пользователя. Должен быть уникальным. \n
             expiration - дата, когда доступ пользователя к монитору будет просрочен. \n
             is_active - флаг, указывающий активен ли монитор.
            required: true
            schema:
              type: object
              properties:
                username:
                  type: string
                  example: dilan-champion
                expiration:
                  type: dateTime
                  example: "2022-04-25 MSK"
                is_active:
                  type: boolean
                  example: true
        responses:
          200:
            description: Токен обновлен
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Созданный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "The data was changed - Dilan.",
                  "status": 200,
                  "token": {
                  "username": "Dilan",
                  "expiration": "2023-05-17T16:25:19",
                  "is_active": false
                  },
                  "params": {}
                  }
          400:
            description: Некорректный ввод
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Required JSON parameters are not set.",
                  "status": 400,
                  "token": null,
                  "params": {}
                }
          500:
            description: Произошла внутренняя ошибка
            schema:
              type: object
              properties:
                message:
                  type: string
                  description: Сообщение о статусе операции
                status:
                  type: integer
                  description: Статус код ответа
                token:
                  type: object
                  description: Полученный токен
                params:
                  type: object
                  description: Дополнительные параметры
              example:
                {
                  "message": "Internal error occurred",
                  "status": 500,
                  "token": null,
                  "params": {}
                }
        """
        request = self.request

        if not request.body_exists:
            return TokenizerResponse(
                message='Got empty request body',
                status=http.HTTPStatus.BAD_REQUEST)

        data = await request.json()

        name = data['username']
        expiration = data['expiration']
        is_active = data['is_active']

        try:
            with DATABASE:
                user = User.get_or_none(User.username == name)

                if not user:
                    self.logger.error(
                        f"Failed change the user data - \"{name}\". Could not find such a user.")
                    return TokenizerResponse(
                        message=f"Failed change the user data - \"{name}\". Could not find such a user.",
                        status=http.HTTPStatus.NOT_FOUND)
                else:
                    if expiration:
                        user.expiration = expiration
                    if isinstance(is_active, bool):
                        user.is_active = is_active
                    user.save()

        except (Exception, Error) as err:
            message = f"Failed to change the user data - \"{name}\". Error: {err}"
            self.logger.error(message)
            return TokenizerResponse(message=message,
                                     status=http.HTTPStatus.INTERNAL_SERVER_ERROR)

        self.logger.debug(f"The data was changed - \"{name}\".")
        return TokenizerResponse(message=f"The data was changed - \"{name}\".",
                                 token={'username': name,
                                        'expiration': user.expiration,
                                        'is_active': is_active},
                                 status=status.HTTP_200_OK)
