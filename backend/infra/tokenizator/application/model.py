from peewee import *

DATABASE = PostgresqlDatabase(database='tokenizator',
                              user='db-boi',
                              password='3j2RqiqWGVauLue',
                              host='c-c9qp8uil4snkiptbtpf0.rw.mdb.yandexcloud.net',
                              port='6432')


class User(Model):
    username = TextField(unique=True, primary_key=True)
    monitor = TextField()
    expiration = DateTimeField()
    is_active = BooleanField()

    class Meta:
        database = DATABASE
        table_name = 'tokenizators'
