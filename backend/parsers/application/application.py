import datetime
import json
import uuid
from http import HTTPStatus
from typing import Any, Type
from urllib.parse import urljoin

import requests
from aiohttp import web
from requests.adapters import Retry, HTTPAdapter

from backend.infra.base_server.app.base_server import BaseApplication
from backend.infra.base_server.config.config import ServerConfig
from backend.infra.base_server.utils.network_utils import query_requirements, json_requirements
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.parser_process_manager import ParserProcessManager
from backend.parsers.core.parser_utils import LEVELS
from backend.parsers.core.products_requests import ProductRequest


class Application(BaseApplication):
    """Класс сервера для парсинга, описывающий API для взаимодействия с парсером.

    Attributes
    ---------
    REGISTRY_RETRIES    количество попыток регистрации

    Methods
    -------
    routes(self)
        функция возвращает всевозможные запросы к серверу.
    _register(self)
        функция регистрации парсера.
    cache_add_product_handler(self, request: web.Request)
        функция-обработчик добавления продукта.
    cache_remove_product_handler(self, request: web.Request)
        функция-обработчик удаления продукта.
    cache_move_product_handler(self, request: web.Request)
        функция-обработчик перемещения продукта.
    products_base_handler(self, request: web.Request)
        функция возвращает список продуктов.
    cache_base_handler(self, request: web.Request)
        функция возвращает список запросов на парсинг товаров.
    switch_cache_level_handler(self, request: web.Request)
        функция-обработчик изменения уровня для продукта.
    ping_handler(self, request: web.Request)
        функция-обработчик веб-запроса.
    """

    REGISTRY_RETRIES = 5
    event_listener: EventListener

    def __init__(self, config: ServerConfig, monitor_config: ServerConfig, redis_config: ServerConfig,
                 parser_class: Type['BaseParser'], *args: Any, **kwargs: Any):
        """Функция, инициализирующая сервер.

        :param self: данный сервер.
        :param config: конфигурация сервера.
        :param monitor_config: конфигурация монитора.
        :param redis_config: конфигурация Redis.
        :param parser_class: тип парсера.
        :param args: аргументы для инициализации экземпляра web.Application.
        :param kwargs: именованные аргументы для инициализации экземпляра web.Application.
        """

        super().__init__(*args, **kwargs)

        self.config = config
        self._redis_config = redis_config
        self._parser_class = parser_class
        self.monitor_address = f'http://{str(monitor_config)}'
        self.start_time = datetime.datetime.now()
        self.event_listener = EventListener(urljoin(self.monitor_address, f"updates/{parser_class.signature}"))
        self.parser_process_manager = ParserProcessManager(parser_class, redis_config, self.event_listener)

    def routes(self):
        """Функция возвращает список возможных запросов для данного сервера.

        :return: список запросов.
        """

        return [web.get('/ping', self.ping_handler),
                web.get('/cache', self.cache_base_handler),
                web.post('/cache', self.cache_add_product_handler),
                web.delete('/cache', self.cache_remove_product_handler),
                web.put('/cache', self.cache_move_product_handler),
                web.get('/products', self.products_base_handler),
                web.get('/capacity', self.get_capacity_handler)]

    def pre_run(self):
        self.parser = self._parser_class(self._redis_config, self.event_listener)  # noqa
        self.parser_process_manager.run()
        self._register()

    def _register(self):
        """Функция регистрации парсера на мониторе.

        :param self: сервер.
        """

        register_url = urljoin(self.monitor_address, 'register')

        register_request_body = {
            'signature': self.parser.signature,
            'host': self.config.host,
            'port': self.config.port
        }

        session = requests.Session()

        retries = Retry(
            total=self.REGISTRY_RETRIES,
            backoff_factor=0.5,
            status_forcelist=[500, 501, 502, 503]
        )

        session.mount('http://', HTTPAdapter(max_retries=retries))

        self.api_logger.debug(
            f'Trying to register in monitor: { {"url": register_url, "request": register_request_body} }')

        r = session.post(url=register_url, json=register_request_body)

        if r.status_code != HTTPStatus.OK:
            raise ConnectionError(r.content)

        self.api_logger.debug('Successfully registered')

    @query_requirements('level')
    @json_requirements('profile_name', 'name', 'url', 'sku', 'tags')
    async def cache_add_product_handler(self, request: web.Request):
        """Функция-обработчик для добавления продукта.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера в формате JSON.
        """

        query = await request.json()
        if request.query['level'] not in LEVELS:
            self.logger.error(f"Level {request.query['level']} does not exists!")
            return web.Response(text=f"Level {request.query['level']} does not exists!", status=400)
        if not query['profile_name'].strip():
            self.logger.error("Profile name cannot be empty!")
            return web.Response(text="Profile name cannot be empty!", status=400)
        # Формирование запроса на парсинг продукта.
        product_request = ProductRequest(profile_name=query['profile_name'],
                                         name=query['name'],
                                         url=query['url'],
                                         sku=query['sku'],
                                         tags=query['tags'],
                                         uid=-1)
        if product_request.optimal_method() is None:
            self.logger.error('All fields were empty!')
            return web.Response(text='All fields were empty!', status=400)
        response = self.parser.add_product_request(
            request.query['level'], product_request)
        return web.json_response(data={'response': response})

    @json_requirements('id')
    async def cache_remove_product_handler(self, request: web.Request):
        """Функция-обработчик для удаления продуктов.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера.
        """

        query = await request.json()
        return web.Response(text=str(self.parser.delete_product_request(query['id'])))

    @json_requirements('id', 'to_level')
    async def cache_move_product_handler(self, request: web.Request):
        """Функция-обработчик для перемещения продукта.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера.
        """

        query = await request.json()
        return web.Response(text=str(self.parser.move_product_request(query['id'], query['to_level'])))

    async def products_base_handler(self, request: web.Request) -> web.Response:
        """Базовая функция-обработчик для передачи информации о продуктах.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера - продукты.
        """

        if 'id' in request.query:
            product_id = -1
            if request.query['id'].isdigit():
                product_id = int(request.query['id'])
            product = self.parser.get_product(product_id)
            if product:
                result = json.loads(str(product))
                return web.json_response(data=result, status=200)
            else:
                return web.json_response(data=None, status=400)
        else:
            result = {level: [] for level in LEVELS}
            all_products = self.parser.get_all_products()
            for product in all_products:
                result[self.parser.get_level(product.uid)].append(
                    json.loads(str(product)))
            return web.json_response(data=result, status=200)

    async def cache_base_handler(self, request: web.Request) -> web.Response:
        """Базовая функция-обработчик для передачи информации о кэшах.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера - кэши.
        """

        if 'id' in request.query:
            product_request_id = -1
            if request.query['id'].isdigit():
                product_request_id = int(request.query['id'])
            product_request = self.parser.get_product(product_request_id)
            if product_request:
                result = json.loads(str(product_request))
                return web.json_response(data=result, status=200)
            else:
                return web.json_response(data=None, status=400)
        else:
            result = {level: [] for level in LEVELS}
            all_product_requests = self.parser.get_all_product_requests()
            # Проход по всем запросам на парсинг продуктов.
            for product_request in all_product_requests:
                result[self.parser.get_level(product_request.uid)].append(
                    json.loads(str(product_request)))
            return web.json_response(data=result, status=200)

    async def get_capacity_handler(self, request: web.Request):
        """Функция-обработчик веб-запроса.
        Возвращает объем хранилища кешей парсера.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера.
        """

        return web.json_response(data=self.parser.capacity, status=200)

    async def ping_handler(self, request: web.Request):
        """Функция-обработчик веб-запроса.
        Выполняет проверку целостности соединения сервера.

        :param self: сервер.
        :param request: веб-запрос к странице.
        :return: ответ сервера.
        """

        return web.Response(text=f'start_time: {self.start_time}\n{request.query_string}', status=200)
