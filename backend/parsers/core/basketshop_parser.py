import redis
import requests
from bs4 import BeautifulSoup

import backend.parsers.core.parser_utils as parser_utils
from backend.infra.base_server.config.config import ServerConfig
from backend.parsers.core.base_parser import BaseParser
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.products import ProductStatus, BasketshopProduct

DEBUG = False


class BasketshopParser(BaseParser):
    """Парсер товаров интернет-магазина Basketshop.

    Attributes
    ----------
    HOME_URL    строка формирования url-адреса с домашней страницы интернет магазина.
    SEARCH_URL  строка формирования url-адреса через страницу поиска товаров (для умного поиска).

    Methods
    -------
    product_by_sku(sku)
        функция извлечения товара по его артикулу.
    smart_search(pattern, pages=1)
        функция поиска товаров, соответствующих данному шаблону характеристик товара.
    parse_product(url, short_url=False)
        функция парсинга продукта по заданному url-адресу страницы продукта.
    """

    HOME_URL = 'https://www.basketshop.ru{0}'.format
    SEARCH_URL = 'https://www.basketshop.ru/catalog/search/?&s[q]={0}&p={1}'.format
    signature = "basketshop"

    def __init__(self, redis_config: ServerConfig, event_listener: EventListener):
        super().__init__(event_listener)
        self._db = redis.StrictRedis(
            host=redis_config.host, port=redis_config.port, db=parser_utils.DB_SIGNATURES[self.signature])
        self._db.ping()

        if DEBUG:
            self._db.flushdb()

    @staticmethod
    def product_by_sku(sku):
        """Функция получения товара по данному артикулу.

        :param sku: артикул продукта.
        :return: объект продукта, которому соответствует данный артикул.
        """

        try:
            page = requests.get(BasketshopParser.SEARCH_URL(sku, 1))
            soup = BeautifulSoup(page.text, 'html.parser')
            product_link = BasketshopParser.HOME_URL(
                soup.find('a', class_='product-card__name')['href'])
            return BasketshopParser.parse_product(product_link)
        except BaseException:
            return BasketshopProduct(uid=-1,
                                     brand=None,
                                     name=None,
                                     sku=BasketshopProduct.make_proper_sku(sku),
                                     image_link=None,
                                     status=ProductStatus.OUT_OF_STOCK,
                                     sizes=list(),
                                     link='',
                                     price=None)

    @staticmethod
    def smart_search(pattern, pages=1):
        """Функция поиска формирования списка товаров, соответствующих данному
        шаблону. Шаблон включает характеристики, которые обязательно (не)
        должны принадлежать товару.

        :param pattern: строковый шаблон с характеристиками.
        :param pages: количество страниц, участвующих в поиске.
        :return: список товаров, соответствующих шаблону.
        """

        result = []
        tag = parser_utils.generate_name_from_pattern(pattern)
        for page_number in range(1, pages + 1):
            page = requests.get(BasketshopParser.SEARCH_URL(tag, page_number))
            # Проверка того, найдена ли страница, соответствующая данному шаблону.
            if page.status_code == 200:
                soup = BeautifulSoup(page.text, 'html.parser')
                for product_card in soup.find_all('a', class_='product-card__name'):
                    product_name = product_card.find('span').text.strip()
                    product_link = BasketshopParser.HOME_URL(
                        product_card['href'])
                    # Проверка валидности объекта продукта.
                    if parser_utils.check_name(product_name, pattern):
                        parsed_product = BasketshopParser.parse_product(
                            product_link)
                        if parsed_product is not None:
                            result.append(parsed_product)
            else:
                return result
        return result

    @staticmethod
    def _get_sku_from_name(name: str):
        """Функция внутреннего пользования. Получение артикула товара по его
        данному наименованию.

        :param name: наименование продукта.
        :return: значение артикула продукта.
        """

        return name[name.rfind('(') + 1:name.rfind(')')]

    @staticmethod
    def parse_product(url: str):
        """Функция парсинга конкретного продукта по переданному url-адресу
        страницы, соответствующей данному продукту.

        :param url: url-адрес страницы, соответствующей продукту.
        :return: объект продукта, сформированный в результате парсинга.
        """

        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'html.parser')
        product_name = soup.find('h1', class_='product__title').text
        product_sku = BasketshopProduct.make_proper_sku(
            BasketshopParser._get_sku_from_name(product_name))
        product_link = url
        sizes = []
        chart = 'None'
        # Парсинг размеров.
        for p_size_list in soup.find_all('ul', class_='product__sizes-list'):
            chart = p_size_list['data-size-chart']
            sizes = [x.text.strip() for x in p_size_list.find_all(
                'button', class_='product__sizes-button')]
            if len(sizes):
                break
        # Инициализация полей в зависимости от массива размеров.
        if not len(sizes):
            product_status = ProductStatus.OUT_OF_STOCK
            product_price = 0.0
            p_sizes = []
        else:
            p_sizes = list([f'{chart}:{x}' for x in sizes])
            product_status = ProductStatus.IN_STOCK
            price = soup.find('div', class_='product__price-value').text
            product_price = float(''.join(filter(str.isdigit, price)))

        product_image = soup.find(
            'div', class_='product__gallery-slider-slide-cell js-zoom').find('img')['src']

        product = BasketshopProduct(uid=-1,
                                    brand='?',
                                    name=product_name,
                                    sku=BasketshopProduct.make_proper_sku(product_sku),
                                    image_link=product_image,
                                    status=product_status,
                                    sizes=p_sizes,
                                    link=product_link,
                                    price=product_price)
        return product
