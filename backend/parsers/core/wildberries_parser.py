import redis
import requests
from bs4 import BeautifulSoup

from backend.infra.base_server.config.config import ServerConfig
from backend.parsers.core import parser_utils
from backend.parsers.core.base_parser import BaseParser
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.parser_utils import generate_name_from_pattern, check_name
from backend.parsers.core.products import ProductStatus, WilberriesProduct

DEBUG = False


class WilberriesParser(BaseParser):
    """Парсер товаров интернет-магазина Wildberries.

    Attributes
    ----------
    HOME_URL    строка формирования url-адреса через домашнюю страницу интернет магазина.
    SEARCH_URL  строка формирования url-адреса через страницу поиска товаров (для умного поиска).
    PRODUCT_URL строка формирования url-адреса через артикул.

    Methods
    -------
    product_by_sku(sku)
        функция извлечения товара по его артикулу.
    smart_search(pattern, pages=1)
        функция поиска товаров, соответствующих данному шаблону характеристик товара.
    parse_product(url : str)
        функция парсинга продукта по заданному url-адресу страницы продукта.
    """

    HOME_URL = 'https://www.wildberries.ru{0}'.format
    SEARCH_URL = 'https://www.wildberries.ru/catalog/0/search.aspx?page={0}&search={1}'.format
    PRODUCT_URL = 'https://www.wildberries.ru/catalog/{0}/detail.aspx'.format
    parser_type = "signature"

    def __init__(self, redis_config: ServerConfig, event_listener: EventListener):
        """Инициализация парсера для интернет-магазина Lamoda.

        :param redis_config: объект конфигурации для сервера.
        """

        super().__init__(event_listener)
        self._db = redis.StrictRedis(
            host=redis_config.host, port=redis_config.port, db=parser_utils.DB_SIGNATURES[self.signature])
        self._db.ping()
        if DEBUG:
            self._db.flushdb()

    @staticmethod
    def product_by_sku(sku):
        """Функция получения товара по данному артикулу.

        :param sku: артикул продукта.
        :return: объект продукта, которому соответствует данный артикул.
        """

        try:
            product_link = WilberriesParser.PRODUCT_URL(sku)
            return WilberriesParser.parse_product(product_link)
        # Конкретика исключения.
        except BaseException:
            return WilberriesProduct(uid=-1,
                                     brand=None,
                                     name=None,
                                     sku=WilberriesProduct.make_proper_sku(
                                         sku),
                                     image_link=None,
                                     status=ProductStatus.OUT_OF_STOCK,
                                     sizes=[],
                                     link='',
                                     price=None)

    @staticmethod
    def smart_search(pattern, pages=1):
        """Функция поиска формирования списка товаров, соответствующих данному
        шаблону. Шаблон включает характеристики, которые обязательно (не)
        должны принадлежать товару.

        :param pattern: строковый шаблон с характеристиками.
        :param pages: количество страниц, участвующих в поиске.
        :return: список товаров, соответствующих шаблону.
        """

        result = []
        tag = generate_name_from_pattern(pattern)
        for page_number in range(1, pages + 1):
            page = requests.get(WilberriesParser.SEARCH_URL(page_number, tag))
            # Проверка того, найдена ли страница, соответствующая данному шаблону.
            if page.status_code == 200:
                soup = BeautifulSoup(page.text, 'html.parser')
                for product_card in soup.find_all('div', class_='product-card__wrapper'):
                    product_name = product_card.find(
                        'div', class_='product-card__brand-name').find('span').text.strip()
                    product_link = product_card['href']
                    # Проверка валидности объекта продукта.
                    if check_name(product_name, pattern):
                        parsed_product = WilberriesParser.parse_product(
                            product_link)
                        if parsed_product is not None:
                            result.append(parsed_product)
            else:
                return result
        return result

    @staticmethod
    def parse_product(url: str):
        """Функция парсинга конкретного продукта по переданному url-адресу
        страницы, соответствующей данному продукту.

        :param url: url-адрес страницы, соответствующей продукту.
        :return: объект продукта, сформированный в результате парсинга.
        """

        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'html.parser')
        name_str = soup.find(
            'h1', class_='same-part-kt__header').find_all('span')
        product_brand = name_str[0].text
        product_name = name_str[1].text

        product_link = url
        product_sku = url.replace('https://', '').split('/')[2]

        product_image = soup.find(
            'div', class_='same-part-kt__slider').find('img')['src']
        product_image = 'https:' + product_image

        price_str = soup.find(
            'div', class_='price-block').find('span', 'price-block__final-price')
        product_price = float(
            ''.join(filter(str.isalnum, price_str.text.strip())))

        # Получение массива размеров товара.
        product_sizes = []
        try:
            sizes_str = soup.find(
                'ul', 'same-part-kt__sizes-list').find_all('label')
            for size_info in sizes_str:
                if 'disabled' not in size_info.__str__():
                    current_size = size_info.find(
                        'span', class_='sizes-list__size-ru')
                    if current_size.text != '':
                        product_sizes.append(current_size.text)
        except BaseException:
            product_sizes = []

        # Получение статуса наличия товара.
        order_text = soup.find(
            'div', class_='same-part-kt__order').find('button', 'btn-main')
        order_status = order_text.find('span').text
        if order_status == 'Добавить в корзину':
            product_status = ProductStatus.IN_STOCK
        else:
            product_status = ProductStatus.OUT_OF_STOCK

        product = WilberriesProduct(uid=-1,
                                    brand=product_brand,
                                    name=product_name,
                                    sku=WilberriesProduct.make_proper_sku(
                                        product_sku),
                                    image_link=product_image,
                                    status=product_status,
                                    sizes=product_sizes,
                                    link=product_link,
                                    price=product_price)
        return product
