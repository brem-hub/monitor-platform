import datetime
import json
import uuid
from typing import Optional, List
import redis
from backend.infra.notifier import Record
from backend.infra.notifier.events import CacheDeleteEvent, CacheMoveEvent, CacheAddEvent
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.parser_utils import DB_TABLES, generate_pattern_from_name, generate_pattern_from_dict, LEVELS
from backend.parsers.core.products import BaseProduct, ProductStatus
from backend.parsers.core.products_requests import ProductRequest, ParseMethod

MODULE_DIV = 2**53 - 1


class BaseParser:
    """Базовый класс, описывающий функциональность парсера товаров.

    Attributes
    ----------
    start_time  время начала работы конкретного парсера
    _db         клиент Redis, взаимодесйтвующий с сервером
    parser_type тип парсера

    Methods
    -------
    add_product_request(self, level: str, product_request: ProductRequest)
        функция добавления запроса и его товара в базу данных.
    delete_product_request(self, request_id: int)
        функция удаления запроса и его товаров из базы данных.
    is_product_request_exist(self, request_id: int)
        функция проверки существования запроса в базе данных.
    is_product_exist(self, product_id: int)
        функция проверки существования продукта в базе данных.
    save_product_request(self, product_request: ProductRequest)
        функция сохранения запроса в базе данных.
    save_product(self, product: BaseProduct)
        функция сохранения продукта в базе данных.
    get_all_product_requests(self)
        функция получения всех запросов из базы данных.
    get_all_products(self)
        функция получения всех товаров из базы данных.
    get_product(self, product_id)
        функция получения товара из базы данных по его идентификатору.
    get_product_request(self, request_id)
        функция получения запроса из базы данных по его идентификатору.
    move_product_request(self, request_id: int, to_level: str)
        функция перемещения запроса на другой уровень приоритета парсинга.
    get_level(self, uid: int)
        функция получения уровня по идентификатору запроса.
    product_by_sku(sku)
        функция извлечения товара по его артикулу.
    smart_search(pattern, pages=1)
        функция поиска товаров, соответствующих данному шаблону характеристик товара.
    parse_product(url : str)
        функция парсинга продукта по заданному url-адресу страницы продукта.
    parse_request(self, request: ProductRequest)
        функция парсинга товара по запросу.
    """

    start_time: Optional[datetime.datetime]
    _db: redis.StrictRedis
    _listener: EventListener
    signature: str

    def __init__(self, event_listener: EventListener):
        """Конструктор парсинга, который инициализирует: дату и время создания
        парсинга, списки товаров, разделенных по приоритету обработки."""
        self.start_time = datetime.datetime.now()
        self._listener = event_listener
        self._cache_capacity = {"L1": 20, "L2": 40, "L3": 100}

    def add_product_request(self, level: str, product_request: ProductRequest) -> bool:
        """Функция передачи клиенту Redis информации о товаре из запроса.

        :param self: данный парсер.
        :param level: уровень приоритета парсинга.
        :param product_request: запрос парсинга товара.
        :return: удалось ли добавить информацию в базу данных Redis.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        if level not in LEVELS:
            return False
        if not product_request:
            return False
        while product_request.uid == -1 or self.is_product_request_exist(product_request.uid):
            product_request.uid = uuid.uuid4().int % MODULE_DIV

        level_current_size = len(self._db.keys(DB_TABLES[level].format('*')))
        if level_current_size >= self._cache_capacity[level]:
            return False

        # Добавление в Redis пары "ключ уровня (из DB_TABLES) - запрос"
        self._db.set(DB_TABLES['L'].format(
            level, product_request.uid), str(product_request))
        # Добавление в Redis пары "ключ списка продуктов (из DB_TABLES) - продукт"
        self._db.set(DB_TABLES['PRODUCTS'].format(product_request.uid), str(BaseProduct(uid=product_request.uid,
                                                                                        brand='',
                                                                                        name='',
                                                                                        sku='',
                                                                                        image_link='',
                                                                                        status=ProductStatus.OUT_OF_STOCK,
                                                                                        sizes=list(),
                                                                                        link='',
                                                                                        price=-1)))
        event = CacheAddEvent(product_request, level)
        self._listener.send(Record(event))
        return True

    def delete_product_request(self, request_id: int) -> bool:
        """Функция удаления из базы данных Redis информации о товаре по запросу.

        :param self: данный парсер.
        :param request_id: идентификатор запроса.
        :return: получилось ли удалить информацию из базы данных Redis.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        if not self.is_product_request_exist(request_id):
            return False
        keys_patterns = [DB_TABLES[level] for level in LEVELS]
        # Удаление запроса и соответствующего товара для всех ключей.
        for key_pattern in keys_patterns:
            self._db.delete(key_pattern.format(request_id))
            self._db.delete(DB_TABLES['PRODUCTS'].format(request_id))
        event = CacheDeleteEvent(request_id)
        self._listener.send(Record(event))
        return True

    def is_product_request_exist(self, request_id: int) -> bool:
        """Функция проверки существования запроса в базе данных Redis.

        :param self: данный парсер.
        :param request_id: идентификатор запроса.
        :return: существует ли данный запрос в базе данных Redis.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        return len(self._db.keys(DB_TABLES['L'].format('*', request_id))) > 0

    def is_product_exist(self, product_id: int) -> bool:
        """Функция проверки существования товара в базе данных Redis.

        :param self: данный парсер.
        :param product_id: идентификатор товара.
        :return: существует ли данный товар в базе данных Redis.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        return self._db.exists(DB_TABLES['PRODUCTS'].format(product_id)) > 0

    def save_product_request(self, product_request: ProductRequest) -> bool:
        """Функция сохранения информации о запросе в базе данных.

        :param self: данный парсер.
        :param product_request: запрос.
        :return: получилось ли сохранить информацию о запросе.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        if not self.is_product_request_exist(product_request.uid):
            return False
        # Массив ключей, соответствующих запросу.
        satisfying_keys = self._db.keys(
            DB_TABLES['L'].format('*', product_request.uid))
        for key in satisfying_keys:
            self._db.set(key, str(product_request))
        return True

    def save_product(self, product: BaseProduct) -> bool:
        """Функция сохранения информации о товаре в базе данных.

        :param self: данный парсер.
        :param product: товар.
        :return: получилось ли сохранить информацию о товаре.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        if not self.is_product_exist(product.uid):
            return False
        satisfying_keys = self._db.keys(
            DB_TABLES['PRODUCTS'].format(product.uid))
        for key in satisfying_keys:
            self._db.set(key, str(product))
        return True

    def get_all_product_requests(self) -> List[ProductRequest]:
        """Функция получения списка всех запросов из базы данных Redis.

        :param: данный парсер.
        :return: список запросов.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        # Шаблон общего ключа, удовлетворяющего всем запросам.
        key_pattern = DB_TABLES['L'].format('*', '*')
        # Список хранящихся в базе данных ключей для запросов.
        all_caches_keys = self._db.keys(key_pattern)
        try:
            # Извлекаем запросы по всем ключам.
            result = [ProductRequest(**json.loads(self._db.get(key)))
                      for key in all_caches_keys]
            return result
        except TypeError:
            return []

    @property
    def capacity(self):
        return self._cache_capacity

    @property
    def event_listener(self):
        return self._listener

    def get_all_products(self) -> List[BaseProduct]:
        """Функция получения списка всех товаров из базы данных Redis.

        :param self: данный парсер.
        :return: список товаров.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        # Шаблон ключа, подходящего для всех товаров.
        key_pattern = DB_TABLES['PRODUCTS'].format('*')
        # Все возможные ключи для товаров.
        all_products_keys = self._db.keys(key_pattern)
        try:
            # Извлечение товаров по их ключам.
            result = [BaseProduct(**json.loads(self._db.get(key)))
                      for key in all_products_keys]
            return result
        except TypeError:
            return []

    def get_product(self, product_id) -> Optional[BaseProduct]:
        """Функция получения товара из базы данных по его идентификатору.

        :param self: данный парсер.
        :param product_id: идентификатор товара.
        :return: искомый товар.
        """

        if not self._db:
            raise TypeError('Redis db was null!')
        product_pattern = DB_TABLES['PRODUCTS'].format(product_id)
        # Получение строкового представления продукта в формате JSON.
        product_serialized = self._db.get(product_pattern)
        try:
            if product_serialized:
                return BaseProduct(**json.loads(product_serialized))
            else:
                return None
        except TypeError:
            return None

    def get_product_request(self, request_id) -> Optional[ProductRequest]:
        """Функция получения запроса из базы данных по его идентификатору.

        :param self: данный парсер.
        :param request_id: идентификатор запроса.
        :return: искомый запрос.
        """

        if not self._db:
            raise TypeError('Redis- db was null!')
        # Шаблон подходящих ключей и их извлечение.
        cache_pattern = DB_TABLES['L'].format('*', request_id)
        satisfying_keys = self._db.keys(cache_pattern)
        try:
            for key in satisfying_keys:
                return ProductRequest(**json.loads(self._db.get(key)))
            return None
        except:  # noqa
            return None

    def move_product_request(self, request_id: int, to_level: str) -> bool:
        """Функция перемещения запроса на новый уровень приоритета парсинга.

        :param self: данный парсер.
        :param request_id: идентификатор запроса.
        :param to_level: новый уровень.
        :return: удалось ли переместить запрос.
        """

        if to_level not in LEVELS:
            return False
        product_request = self.get_product_request(request_id)
        request_level = self.get_level(request_id)

        if request_level is None or product_request is None:
            return False

        level_current_size = len(self._db.keys(DB_TABLES[to_level].format('*')))
        if level_current_size >= self._cache_capacity[to_level]:
            return False

        cache_pattern = DB_TABLES[request_level].format(request_id)
        new_cache_pattern = DB_TABLES[to_level].format(request_id)

        self._db.rename(cache_pattern, new_cache_pattern)

        event = CacheMoveEvent(request_id, to_level)
        self._listener.send(Record(event))

        return True

    def get_level(self, uid: int) -> Optional[str]:
        """Функция извлечения уровня по идентификатору.

        :param self: данный парсер.
        :param uid: идентификатор запроса.
        :return: уровень запроса.
        """

        if not self._db:
            raise TypeError('Redis- db was null!')
        # Шаблон подходящих ключей и их извлечение.
        pattern = DB_TABLES['L'].format('*', uid)
        satisfying_keys = self._db.keys(pattern)
        if satisfying_keys:
            # Извлечение строкового представления уровня.
            return satisfying_keys[0].decode('utf-8').split(':')[-2]
        return None

    @staticmethod
    def smart_search(pattern: str, pages=1):
        """Статический метод умного поиска для парсинга продукта.

        :param pattern: строковый шаблон поиска.
        :param pages: количество страниц, участвующих в поиске.
        :return: экземпляр продукта.
        """

        raise NotImplementedError(
            'Base parser has no realisation for this method!')

    @staticmethod
    def parse_product(url: str):
        """Статический метод парсинга продукта по его url.

        :param url: url-адрес страницы с продуктом.
        :return: экземпляр продукта.
        """

        raise NotImplementedError(
            'Base parser has no realisation for this method!')

    @staticmethod
    def product_by_sku(sku: str):
        """Статический метод парсинга продукта по его артикулу.

        :param sku: артикул продукта.
        :return: экземпляр продукта.
        """

        raise NotImplementedError(
            'Base parser has no realisation for this method!')

    def parse_request(self, request: ProductRequest) -> Optional[BaseProduct]:
        """Функция получения товара по запросу.

        :param self: данный парсер.
        :param request: запрос.
        :return: экземпляр товара.
        """
        try:
            if request:
                parse_method = request.optimal_method()
                # Получение товара в зависимости от способа парсинга.
                match parse_method:
                    case ParseMethod.SKU:
                        return self.product_by_sku(request.sku)
                    case ParseMethod.URL:
                        return self.parse_product(request.url)
                    case ParseMethod.NAME:
                        results = self.smart_search(
                            generate_pattern_from_name(request.name))
                        if results:
                            return results[0]
                        else:
                            return None
                    case ParseMethod.SMART:
                        results = self.smart_search(
                            generate_pattern_from_dict(request.tags))
                        if results:
                            return results[0]
                        else:
                            return None
                    case _:
                        raise ValueError('Null request or product!')
            raise ValueError('Null request or product!')
        except ValueError:
            return BaseProduct(uid=request.uid,
                               brand='',
                               name='',
                               sku='',
                               image_link='',
                               status=ProductStatus.OUT_OF_STOCK,
                               sizes=list(),
                               link='',
                               price=0)
