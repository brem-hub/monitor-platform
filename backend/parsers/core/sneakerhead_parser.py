import redis
from bs4 import BeautifulSoup
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import backend.parsers.core.parser_utils as parser_utils
from backend.infra.base_server.config.config import ServerConfig
from backend.parsers.core.base_parser import BaseParser
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.products import ProductStatus, SneakerheadProduct

DEBUG = False


class SneakerheadParser(BaseParser):
    """Парсер товаров интернет-магазина Sneakerhead.

    Attributes
    ----------
    HOME_URL    строка формирования url-адреса с домашней страницы интернет магазина.
    SEARCH_URL  строка формирования url-адреса через страницу поиска товаров (для умного поиска).

    Methods
    -------
    product_by_sku(sku)
        функция извлечения товара по его артикулу.
    smart_search(pattern, pages=1)
        функция поиска товаров, соответствующих данному шаблону характеристик товара.
    parse_product(url, short_url=False)
        функция парсинга продукта по заданному url-адресу страницы продукта.
    """

    SEARCH_URL = 'https://sneakerhead.ru/search/?q={0}&PAGEN_1={1}'.format
    signature = "sneakerhead"

    def __init__(self, redis_config: ServerConfig, event_listener: EventListener):
        super().__init__(event_listener)
        self._db = redis.StrictRedis(
            host=redis_config.host, port=redis_config.port, db=parser_utils.DB_SIGNATURES[self.signature])
        self._db.ping()

        if DEBUG:
            self._db.flushdb()

    @staticmethod
    def product_by_sku(sku):
        """Функция получения товара по данному артикулу.

        :param sku: артикул продукта.
        :return: объект продукта, которому соответствует данный артикул.
        """

        try:
            return SneakerheadParser.parse_product(SneakerheadParser.link_by_sku(sku))
        except BaseException:
            return SneakerheadProduct(uid=-1,
                                      brand=None,
                                      name=None,
                                      sku=SneakerheadProduct.make_proper_sku(sku),
                                      image_link=None,
                                      status=ProductStatus.OUT_OF_STOCK,
                                      sizes=list(),
                                      link='',
                                      price=None)

    @staticmethod
    def link_by_sku(sku):
        """Вспомогательная функция извлечения ссылки на продукт по его артикулу.

        :param sku: артикул продукта.
        :return: строковое представление ссылки на продукт.
        """

        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get(SneakerheadParser.SEARCH_URL(sku, 1))
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        return soup.find('div', class_="product__price").find('link')['href']

    @staticmethod
    def smart_search(pattern, pages=1):
        """Функция поиска формирования списка товаров, соответствующих данному
        шаблону. Шаблон включает характеристики, которые обязательно (не)
        должны принадлежать товару.

        :param pattern: строковый шаблон с характеристиками.
        :param pages: количество страниц, участвующих в поиске.
        :return: список товаров, соответствующих шаблону.
        """

        driver = webdriver.Chrome(ChromeDriverManager().install())
        result = []
        tag = parser_utils.generate_name_from_pattern(pattern)
        for page_number in range(1, pages + 1):
            driver.get(SneakerheadParser.SEARCH_URL(tag, page_number))
            try:
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                # Парсинг продукта.
                for product_card in soup.find_all('div', class_='product-cards__item'):
                    info = product_card.find('div', class_="product-card")
                    product_name = info.find('meta', itemprop='description')['content']
                    if parser_utils.check_name(product_name, pattern):
                        product_brand = info.find('meta', itemprop='manufacturer')['content']
                        product_sku = info.find('meta', itemprop='sku')['content']
                        product_image_info = info.find('div', class_='product-card__image-inner')
                        product_image = product_image_info.find('source', itemprop='image')['srcset'].split()[0]
                        product_image = 'https://sneakerhead.ru' + product_image
                        product_price = info.find('div', class_='product-card__price').find('meta',
                                                                                            itemprop='price')['content']
                        sizes = []
                        for size in info.find('dl', class_="product-card__sizes").findAll('dd'):
                            sizes.append(size.text)
                        driver.get(SneakerheadParser.SEARCH_URL(product_sku, 1))
                        soup_opt = BeautifulSoup(driver.page_source, 'html.parser')
                        product_link = soup_opt.find('div', class_="product__price").find('link')['href']
                        parsed_product = SneakerheadProduct(uid=-1,
                                                            brand=product_brand,
                                                            name=product_name,
                                                            sku=product_sku,
                                                            image_link=product_image,
                                                            status=ProductStatus.IN_STOCK,
                                                            sizes=sizes,
                                                            link=product_link,
                                                            price=product_price)
                        result.append(parsed_product)
            except Exception as ex:
                return result

    @staticmethod
    def parse_product(url: str):
        """Функция парсинга конкретного продукта по переданному url-адресу
        страницы, соответствующей данному продукту.

        :param url: url-адрес страницы, соответствующей продукту.
        :return: объект продукта, сформированный в результате парсинга.
        """

        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get(url)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        product_name = soup.find('h1', class_='product__title').text
        product_brand = soup.find('meta', itemprop='brand')['content']
        price_info = soup.find('div', class_='product__price')
        product_price = price_info.find('meta', itemprop='price')['content']
        product_sku = soup.find('p', class_="product__art").find('span', class_="text-uppercase").text
        product_image = 'https://sneakerhead.ru' + soup.find('a', class_="product__images-link")['href']
        product_link = url
        p_sizes = []
        size_list = soup.find('ul', class_="product-sizes__list")
        # Проверка наличия продукта.
        if soup.find('div', class_="product__badge").text.strip() == "В наличии":
            product_status = ProductStatus.IN_STOCK
            # Парсинг размеров продукта.
            for button in size_list.findAll('li', class_="product-sizes__item"):
                p_sizes.append(button.find('button', class_="product-sizes__button").text.strip())
        else:
            product_status = ProductStatus.OUT_OF_STOCK

        product = SneakerheadProduct(uid=-1,
                                     brand=product_brand,
                                     name=product_name,
                                     sku=SneakerheadProduct.make_proper_sku(
                                        product_sku),
                                     image_link=product_image,
                                     status=product_status,
                                     sizes=p_sizes,
                                     link=product_link,
                                     price=float(product_price))
        return product
