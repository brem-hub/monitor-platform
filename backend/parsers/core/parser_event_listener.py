import asyncio

import aiohttp
import nest_asyncio

from backend.infra.notifier import Listener, Record

from backend.infra.notifier.events import BaseEvent


class EventListener(Listener):
    _callback_address: str

    def __init__(self, callback_address):
        super().__init__()
        self._callback_address = callback_address

    def call(self, *args, **kwargs):
        if "record" in kwargs.keys() and isinstance(kwargs["record"].record, BaseEvent):
            try:
                nest_asyncio.apply()
                asyncio.run(self._notify(*args, **kwargs))
            except:  # noqa
                pass
        super().call(*args, **kwargs)

    async def _notify(self, *args, **kwargs):
        async with aiohttp.ClientSession() as session:
            await session.post(self._callback_address, json=kwargs["record"].record.json())

    def send(self, record: Record):
        super().send(record)
        self.call(record=record)

    @property
    def has_updates(self) -> bool:
        result = self._has_updates
        return result
