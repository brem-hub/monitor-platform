import re
from re import search
from urllib.parse import urljoin

import redis
import requests
from bs4 import BeautifulSoup

import backend.parsers.core.parser_utils as parser_utils
from backend.infra.base_server.config.config import ServerConfig
from backend.parsers.core.base_parser import BaseParser
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.products import ProductStatus, LamodaProduct

DEBUG = False


class LamodaParser(BaseParser):
    """Парсер товаров интернет-магазина Lamoda.

    Attributes
    ----------
    HOME_URL    строка формирования url-адреса через домашнюю страницу интернет магазина.
    SEARCH_URL  строка формирования url-адреса через страницу поиска товаров (для умного поиска).

    Methods
    -------
    product_by_sku(sku)
        функция извлечения товара по его артикулу.
    smart_search(pattern, pages=1)
        функция поиска товаров, соответствующих данному шаблону характеристик товара.
    parse_product(url : str)
        функция парсинга продукта по заданному url-адресу страницы продукта.
    """

    HOME_URL = 'https://www.lamoda.ru{0}'.format
    SEARCH_URL = 'https://www.lamoda.ru/catalogsearch/result/?q={0}&page={1}'.format
    signature = "lamoda"

    def __init__(self, redis_config: ServerConfig, event_listener: EventListener):
        """Инициализация парсера для интернет-магазина Lamoda.

        :param redis_config: объект конфигурации для сервера.
        """

        super().__init__(event_listener)
        self._db = redis.StrictRedis(
            host=redis_config.host, port=redis_config.port, db=parser_utils.DB_SIGNATURES[self.signature])
        self._db.ping()
        if DEBUG:
            self._db.flushdb()

    @staticmethod
    def product_by_sku(sku):
        """Функция получения товара по данному артикулу.

        :param sku: артикул продукта.
        :return: объект продукта, которому соответствует данный артикул.
        """

        try:
            product_link = 'https://www.lamoda.ru/p/{str_sku}/'.format(
                str_sku=sku)
            product = LamodaParser.parse_product(product_link)
            if product is not None:
                return product
            else:
                return LamodaProduct(uid=-1,
                                     brand='',
                                     name='',
                                     sku=LamodaProduct.make_proper_sku(sku),
                                     image_link='',
                                     status=ProductStatus.OUT_OF_STOCK,
                                     sizes=list(),
                                     link='',
                                     price=None)
        except Exception:  # noqa
            return LamodaProduct(uid=-1,
                                 brand='',
                                 name='',
                                 sku=LamodaProduct.make_proper_sku(sku),
                                 image_link='',
                                 status=ProductStatus.OUT_OF_STOCK,
                                 sizes=list(),
                                 link='',
                                 price=None)

    @staticmethod
    def smart_search(pattern, pages=1):
        """Функция поиска формирования списка товаров, соответствующих данному
        шаблону. Шаблон включает характеристики, которые обязательно (не)
        должны принадлежать товару.

        :param pattern: строковый шаблон с характеристиками.
        :param pages: количество страниц, участвующих в поиске.
        :return: список товаров, соответствующих шаблону.
        """

        result = []
        tag = parser_utils.generate_name_from_pattern(pattern)
        for page_number in range(1, pages + 1):
            page = requests.get(LamodaParser.SEARCH_URL(tag, page_number))
            # Проверка того, найдена ли страница, соответствующая данному шаблону.
            if page.status_code == 200:
                soup = BeautifulSoup(page.text, 'html.parser')
                # Нет подходящих товаров для данного шаблона.
                if search('Поиск не дал результатов', soup.text):
                    return result

                for product_card in soup.find_all('div', class_='x-product-card__card'):
                    product_name = product_card.find('div',
                                                     class_='x-product-card-description__product-name').text.strip()
                    if parser_utils.check_name(product_name, pattern):
                        try:
                            brand_name = product_card.find('div',
                                                           class_="x-product-card-description__brand-name").text.strip()
                            product_link = LamodaParser.HOME_URL(product_card.find('a')['href'])
                            price = float(product_card.find('span',
                                                            class_="x-product-card-description__price-single").text.strip()[
                                          :-2].replace(" ", ""))
                            sizes = []
                            img = product_card.find('img', class_="x-product-card__pic-img")['src']
                            parsed_product = LamodaProduct(uid=-1,
                                                           brand=brand_name,
                                                           name=product_name,
                                                           sku=None,
                                                           image_link=img,
                                                           status=ProductStatus.IN_STOCK,
                                                           sizes=sizes,
                                                           link=product_link,
                                                           price=price)
                            result.append(parsed_product)
                        except Exception as ex:  # noqa
                            result.append(LamodaProduct(uid=-1,
                                                        brand='',
                                                        name='',
                                                        sku=None,
                                                        image_link='',
                                                        status=ProductStatus.OUT_OF_STOCK,
                                                        sizes=list(),
                                                        link='',
                                                        price=None))

        return result

    @staticmethod
    def parse_product(url: str):
        """Функция парсинга конкретного продукта по переданному url-адресу
        страницы, соответствующей данному продукту.

        :param url: url-адрес страницы, соответствующей продукту.
        :return: объект продукта, сформированный в результате парсинга.
        """
        try:
            page = requests.get(url)
            soup = BeautifulSoup(page.text, 'html.parser')
            product_brand = soup.find(
                'h1', class_='product-title__brand-name')['title']
            product_name = soup.find(
                'span', class_='product-title__model-name').text
            product_sku = LamodaProduct.make_proper_sku(
                url.replace('https://www.lamoda.ru/', '').split('/')[1])
            product_link = url

            # Извлечение картинки с учетом того, что у товара их может быть несколько
            try:
                product_img = soup.find('img', class_='x-product-gallery__image x-product-gallery__image_first')['src']
            except TypeError:
                product_img = soup.find('img', class_='x-product-gallery__image x-product-gallery__image_single')['src']
            product_img = urljoin('https:', product_img)

            # Проверка наличия товара
            stock = soup.find('button')
            if stock.text.strip() == 'Добавить в корзину':  # Товар есть в наличии
                product_status = ProductStatus.IN_STOCK
                # Возможна ситуация, когда у товара несколько цен - старая и новая (при наличии скидки)
                # Извлекаем последнюю цену - наиболее актуальную
                price_text = soup.find_all(
                    'span', class_='product-prices__price')[-1].text
                product_price = float(
                    ''.join(list(filter(str.isalnum, price_text))))
            else:  # Товар отсутствует
                product_status = ProductStatus.OUT_OF_STOCK
                product_price = 0.0

            # Извлекаем массив имеющихся размеров
            try:
                sizes_text = re.findall('"sizes":\[[^\]]*\]', page.text)  # noqa
                str_sizes = str(sizes_text).replace(
                    """['"sizes":[""", '').replace("]']", '')
                product_sizes = parser_utils.parse_sizes(str_sizes)
            except TypeError:
                product_sizes = []

            product = LamodaProduct(uid=-1,
                                    brand=product_brand,
                                    name=product_name,
                                    sku=product_sku,
                                    image_link=product_img,
                                    status=product_status,
                                    sizes=product_sizes,
                                    link=product_link,
                                    price=product_price)
            return product
        except Exception as ex:  # noqa
            print(ex.__repr__())
            return None

    @staticmethod
    def make_proper_sku(sku: str):
        return sku
