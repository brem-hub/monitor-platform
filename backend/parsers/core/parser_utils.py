"""В ряде функций фигурирует строковая переменная pattern.

Это строка запроса, содержащая информацию о наличии тегов продукта.
Данные о характеристиках разделены знаком пробела. Если характеристика
должна принадлежать продукту, перед ним ставится знак "+". Если
характеристика не должна принадлежать продукту, перед ним ставится знак "-".
"""

import json
import re


class RedisMock:
    def flushdb(self):
        pass

    def add(self):
        pass

    def get(self):
        pass

    def delete(self):
        pass


def parse_sizes(sizes):
    """Вспомогательная функция для парсера Lamoda.

    :param sizes: строковое представление массива размеров товара.
    :return: список размеров с данными.
    """
    splitted = re.findall(r'{[^}]*}', sizes)
    result_wo_filter = [parse_size(parsed_size) for parsed_size in splitted]
    return list(filter(lambda x: x is not None, result_wo_filter))


def parse_size(size_data):
    """Вспомогательная функция для парсера Lamoda. Участвует в фильтрации
    исходного массива размеров товара. Возвращает только те размеры, которые
    есть в наличии.

    :param size_data: строковое (json) представление данных о размере.
    :return: размер, если он в наличии; None, если нет в наличии.
    """

    result = json.loads(size_data)
    if result['is_available']:
        return result['size']
    else:
        return None


def generate_name_from_pattern(pattern: str):
    """Утилитная функция получения условного тега по данному строковому шаблону
    для дальнейшего поиска страницы, url которой содержит данный тег. Страница
    содержит товары, соответствующие тегу.

    :param pattern: строковый шаблон.
    :return: значение сформированного тега.
    """

    return ' '.join(re.findall(r'\+\S*', pattern))


def generate_pattern_from_dict(dictionary: dict):
    """Утилитная функция получения строкового шаблона продукта по словарю с
    ключами включений и исключений (речь о наборе характеристик товара).

    :param dictionary: словарь с ключами exclude, include.
    :return: строковый шаблон.
    """

    return ' '.join(generate_pattern_from_name(key) for key in dictionary['include']) + ' ' + ' '.join(
        generate_pattern_from_name(key, sep='-') for key in dictionary['exclude'])


def generate_pattern_from_name(name: str, sep: str = '+'):
    """Утилитная функция получения строкового шаблона продукта по имени этого
    продукта.

    :param name: имя продукта.
    :param sep: сепаратор '+' - включение характеристик в шаблон.
    :return: строковый шаблон продукта.
    """
    pieces = name.split()
    return ' '.join([f'{sep}{piece}' for piece in pieces])


def get_forbidden(pattern: str):
    """Утилитная функция извлечения характеристик, которые не должны
    принадлежать продукту, из данного строкового шаблона.

    :param pattern: строковый шаблон продукта.
    :return: список характеристик, которые не должны принадлежать продукту.
    """

    temp = pattern.lower()
    forbidden = re.findall(r'\-\S*', temp)
    return list(map(lambda x: x.replace('-', ''), forbidden))


def get_required(pattern: str):
    """Утилитная функция извлечения характеристик, которые должны принадлежать
    продукту, из данного строкового шаблона.

    :param pattern: строковый шаблон.
    :return: список характеристик, которые должны принадлежать продукту.
    """

    temp = pattern.lower()
    required = re.findall(r'\+\S*', temp)
    return list(map(lambda x: x.replace('+', ''), required))


def check_name(tag: str, pattern: str):
    """Функция проверки на валидность наименования продукта в соответствии с
    данным строковым шаблоном.

    :param tag: наименование продукта.
    :param pattern: строковый шаблон продукта.
    :return: значение проверки на валидность.
    """

    temp_tag = tag.lower().replace(' ', '')
    # Список тегов, которые не должны присутствовать в имени.
    forbidden = get_forbidden(pattern)
    # Список тегов, которые не должны присутствовать в имени.
    required = get_required(pattern)
    for forbidden_piece in forbidden:
        if forbidden_piece in temp_tag:
            return False
    for required_piece in required:
        if required_piece not in temp_tag:
            return False
    return True


def get_sku_from_url(url: str):
    """Функция извлечения артикула товара из url-адреса страницы, которая
    ссылается на рассматриваемый продукт.

    :param url: url-адрес страницы продукта.
    :return: строковое представление артикула товара.
    """

    return get_proper_sku(url.split('/')[2])


def get_proper_sku(sku: str):
    """Функция получения корректного вида артикула. Корректным считается
    артикул, приведенный к верхнему регистру.

    :param sku: изначальный артикул товара.
    :return: корректный вид артикула.
    """

    return sku.upper()


def decode_payload(payload):
    return json.loads(payload)


# Уровни приоритета обработки.
# L1 - высокий, L2 - средний, L3 - низкий.
LEVELS = ['L1', 'L2', 'L3']

# Словарь ключей, используемых в базе данных Redis.
DB_TABLES = {'L': 'caches:{0}:{1}',
             'L1': 'caches:L1:{0}',
             'L2': 'caches:L2:{0}',
             'L3': 'caches:L3:{0}',
             'PRODUCTS': 'products:{0}'}

DB_SIGNATURES = {
    "basketshop": 0,
    "lamoda": 1,
    "basketshop-test": 2,
    "sneakerhead": 3
}
