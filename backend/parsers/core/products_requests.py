import json
from dataclasses import dataclass
from enum import IntEnum
from typing import Optional, List, Dict


class ParseMethod(IntEnum):
    """Класс на основе перечисления, описывающий методы парсинга.

    URL : парсинг через url-адрес страницы товара.
    SKU: парсинг через исходный артикул товара.
    NAME: парсинг через наименование товара.
    SMART: умный поиск на основании шаблона характеристик товара.
    """

    URL = 0,
    SKU = 1,
    NAME = 2,
    SMART = 3,


@dataclass
class ProductRequest:
    """Класс, описывающий запрос парсинга товара.

    Attributes
    ----------
    profile_name    условное имя запроса
    name            наименование товара
    url             url-адрес страницы товара
    sku             артикул товара
    tags            словарь тегов товара
    uid             идентификатор

    Methods
    -------
    optimal_method(self)
        функция определения оптимального способа парсинга данного запроса.
    update_with(self, product)
        функция обновления информации для запроса.
    """

    __attrs__ = ("profile_name", "name", "url", "sku", "tags", "uid")

    profile_name: str
    name: Optional[str]
    url: Optional[str]
    sku: Optional[str]
    tags: Optional[Dict[str, List[str]]]
    uid: int

    def __str__(self):
        """Функция строкового представления запроса
        с использованием сериализации в строку формата JSON.
        В качестве данных используется атрибут __dict__ объекта запроса.
        """
        return json.dumps(self.as_dict())

    def as_dict(self):
        result = {}
        for attr_name in self.__attrs__:
            attribute = getattr(self, attr_name)
            if isinstance(attribute, IntEnum):
                result[attr_name] = int(attribute)
            else:
                result[attr_name] = attribute
        return result

    def optimal_method(self) -> Optional[ParseMethod]:
        """Функция определения оптимального метода парсинга товара запроса.

        :param self: текущий объект запроса.
        :return: метод парсинга, который можно применить к товару запроса.
        """

        # У товара запроса непустая ссылка - парсинг по url.
        if self.url and self.url.strip():
            return ParseMethod.URL
        # У товара запроса непустой артикул - парсинг по артикулу.
        elif self.sku and self.sku.strip():
            return ParseMethod.SKU
        # У товара запроса непустое наименование - парсинг по наименованию.
        elif self.name and self.name.strip():
            return ParseMethod.NAME
        # У запроса есть теги - парсинг с помощью умного поиска.
        elif self.tags:
            return ParseMethod.SMART
        else:
            return None

    def update_with(self, product: 'BaseProduct'):  # noqa
        """Функция обновления данных запроса по данным продукта.

        :param self: данный запрос.
        :param product: товар, соответствующий данному запросу.
        """

        product_method = product.optimal_method()
        if product_method is not None:
            self.name = product.name
            self.sku = product.sku
            self.url = product.link
            self.uid = product.uid
