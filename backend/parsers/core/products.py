import copy
import enum
import json
from dataclasses import dataclass
from typing import Optional

from backend.parsers.core.products_requests import ParseMethod


class ProductStatus(enum.IntEnum):
    """Класс на основе перечислений, описывающий статусы товара.

    OUT_OF_STOCK : товар не находится на складе
    IN_STOCK : товар на складе
    """

    OUT_OF_STOCK = 0,
    IN_STOCK = 1


@dataclass
class BaseProduct:
    """Базовый класс, описывающий сущность товара.

    Attributes
    ----------
    uid         идентификатор товара (уникальный атрибут)
    brand       бренд товара
    name        полное наименование товара
    sku         артикул товара
    image_link  ссылка на изображение товара
    status      статус товара
    sizes       список доступных размеров товара
    link        ссылка на товар
    price       цена товара

    Methods
    -------
    make_proper_sku(sku: str)
        функция приведения артикула к корректному виду.
    set_sku(self, new_sku: str)
        функция присвоения товару значения его артикула.
    copy_data(self, copy_from)
        функция копирования характеристик товара.
    optimal_method(self)
        функция определения оптимального способа парсинга товара.
    """

    __attrs__ = ("uid", "brand", "name", "sku", "image_link", "status", "sizes", "link", "price")

    uid: int
    brand: Optional[str]
    name: Optional[str]
    sku: Optional[str]
    image_link: Optional[str]
    status: Optional[ProductStatus]
    sizes: Optional[list]
    link: Optional[str]
    price: Optional[float]

    @staticmethod
    def make_proper_sku(sku: str):
        """Функция приведения артикула к корректному виду. Корректным считается
        артикул, приведённый к верхнему регистру без начальных/ конечных пробелов.

        :param sku: исходный артикул товара.
        :return: значение артикула, приведённого к верхнему регистру.
        """

        return sku.strip().upper()

    def as_dict(self):
        """Функция представления товара в виде словаря "атрибут - значение".

        :return: сформированный словарь.
        """
        result = {}
        for attr_name in self.__attrs__:
            attribute = getattr(self, attr_name)
            if isinstance(attribute, enum.IntEnum):
                result[attr_name] = int(attribute)
            # elif isinstance(attribute, enum.StrEnum):
            #     result[attr_name] = str(attribute)
            else:
                result[attr_name] = attribute
        return result

    def __str__(self):
        """Функция строкового представления продукта
        с использованием сериализации в строку формата JSON.
        В качестве данных используется атрибут __dict__ объекта продукта.
        """

        return json.dumps(self.as_dict())

    def __eq__(self, other):
        """Функция сравнения данного товара с другим.

        :param self: данный товар.
        :param other: товар, с которым сравнивается данный.
        :return: результат сравнения.
        """
        if other is None and \
                self.sizes == list() and \
                self.status == ProductStatus.OUT_OF_STOCK and \
                self.image_link == "" and \
                self.link == "":
            return True
        if isinstance(other, BaseProduct):
            other_fields = other.as_dict()
            for field, value in self.as_dict().items():
                # Учет равенства атрибутов (за исключением идентификатора).
                if field != 'uid' and value != other_fields[field]:
                    return False
            return True
        return False

    def copy_data(self, copy_from: 'BaseProduct'):
        """Функция копирования значений характеристик
        из одного товара в данный.

        :param self: данный товар, принимающий скопированные значения.
        :param copy_from: товар, чьи атрибуты копируются.
        """
        if copy_from is None:
            self.sizes = list()
            self.status = ProductStatus.OUT_OF_STOCK
            self.image_link = ""
            self.link = ""
            return

        for field, value in copy_from.__dict__.items():
            # Полное копирование всех характеристик кроме идентификатора.
            if field != 'uid':
                self.__dict__[field] = copy.deepcopy(value)

    def optimal_method(self) -> Optional[ParseMethod]:
        """Функция определения оптимального метода парсинга товара.

        :param self: данный товар.
        :return: метод парсинга, который можно применить к данному товару.
        """

        # У товара непустая ссылка - парсинг по url.
        if self.link and self.link.strip():
            return ParseMethod.URL
        # У товара непустой артикул - парсинг по артикулу.
        elif self.sku and self.sku.strip():
            return ParseMethod.SKU
        # У товара непустое наименование - парсинг по наименованию.
        elif self.name and self.name.strip():
            return ParseMethod.NAME
        else:
            return None


class BasketshopProduct(BaseProduct):
    """Класс, описывающий парсер магазина Basketshop."""
    pass


class LamodaProduct(BaseProduct):
    """Класс, описывающий парсер магазина Lamoda."""
    pass


class WilberriesProduct(BaseProduct):
    """Класс, описывающий парсер магазина Wilberries."""
    pass


class SneakerheadProduct(BaseProduct):
    """Класс, описывающий парсер магазина Sneakerhead."""
    pass
