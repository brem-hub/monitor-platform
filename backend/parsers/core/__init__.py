from backend.parsers.core.basketshop_parser import BasketshopParser
from backend.parsers.core.lamoda_parser import LamodaParser
from backend.parsers.core.basketshop_test_parser import BasketshopTestParser
from backend.parsers.core.sneakerhead_parser import SneakerheadParser

# Словарь парсеров.
parser_table = {
    'basketshop': BasketshopParser,
    'lamoda': LamodaParser,
    'basketshop-test': BasketshopTestParser,
    'sneakerhead': SneakerheadParser
}
