import logging
from multiprocessing import Process
from typing import Dict, Optional, Type, Any

from backend.infra.base_server.config.config import ServerConfig
from backend.infra.notifier import Record
from backend.infra.notifier.events import ProductUpdateEvent
from backend.parsers.core.base_parser import BaseParser
from backend.parsers.core.parser_event_listener import EventListener
from backend.parsers.core.parser_utils import LEVELS
from backend.parsers.core.products_requests import ProductRequest

logger = logging.getLogger('process_manager')


class ParserProcessManager:
    """Класс, описывающий менеджер процесса работы парсера.

    Attributes
    ----------
    _parser_class : класс парсера
    _processes : словарь пар "уровень - процесс".

    Methods
    -------
    _run(self, level: str)
            функция запуска демона для заданного уровня.
    run(self, level: str = None)
            функция запуска процесса.
    kill(self, level: str = None)
            функция уничтожения процесса.
    """

    _parser_class: Any
    _processes: Dict[str, Optional[Process]]
    _event_listener: EventListener
    _redis_config: ServerConfig

    def __init__(self, parser_class: Type['BaseParser'], redis_config: ServerConfig, event_listener: EventListener):
        """Функция, инициализирующая менеджер.

        :param self: инициализируемый менеджер.
        :param parser_class: класс парсера.
        """

        self._processes = {level: None for level in LEVELS}
        self._parser_class = parser_class
        self._redis_config = redis_config
        self._event_listener = event_listener

    def _run(self, level: str):
        """Функция запуска процесса демона заданного уровня.

        :param self: данный менеджер.
        :param level: заданный уровень приоритета.
        """
        parser_obj = self._parser_class(self._redis_config, self._event_listener)  # noqa

        process = Process(target=ParserProcessManager.run_cycle, args=(level, parser_obj))
        process.daemon = True
        self._processes[level] = process

        process.start()

    def run(self, level: str = None):
        """Запуск процессов всех уровней по умолчанию.
        Запуск процесса определенного уровня, если он указан.

        :param self: данный менеджер.
        :param level: заданный уровень.
        """

        # Уровень указан.
        if level:
            if level not in LEVELS:
                raise ValueError(f'Wrong level requested for parser processor, available: {LEVELS}!')
            # Если процесс с заданным уровнем уже запущен, уничтожаем его.
            if self._processes.get(level, None):
                self.kill(level)
            self._run(level)
        else:
            # Запуск процессов всех уровней.
            for level in LEVELS:
                self.run(level)

    @staticmethod
    def run_cycle(level: str, parser: BaseParser):
        while True:
            product_requests = parser.get_all_product_requests()
            if not len(product_requests):
                continue
            try:
                for request in product_requests:
                    if request and parser.get_level(request.uid) == level:
                        ParserProcessManager.process_request(parser, request)
            except Exception as ex:  # noqa
                logger.error(f"Something went wrong on Process {parser.signature}:{level}!")
                logger.error(str(ex))
            # time.sleep((int(level[1:]) ** 15) * 0.0000001)

    @staticmethod
    def process_request(parser: BaseParser, request: ProductRequest):
        """Функция, имитирующая процесс запроса.
        Фактически она проверяет состояние товара в базе данных.
        При наличии изменений информация обновляется.

        :param parser: парсер.
        :param request: запрос.
        """

        # Получение товара по идентификатору запроса из базы данных.
        corresponding_product = parser.get_product(request.uid)
        # Получение продукта по запросу после непосредственного парсинга.
        parsed_product = parser.parse_request(request)
        # Произошли изменения в характеристиках товара.
        if corresponding_product != parsed_product:
            # Обновление характеристик товара.
            logger.info(corresponding_product)
            logger.info(parsed_product)
            corresponding_product.copy_data(parsed_product)
            # Обновление запроса.
            request.update_with(corresponding_product)
            parser.save_product(corresponding_product)
            parser.save_product_request(request)
            event = ProductUpdateEvent(product=corresponding_product, product_request=request)
            parser.event_listener.send(Record(event))

    def kill(self, level: str = None):
        """Функция уничтожения процесса (по умолчанию всех).
        При указании уровня удаляется конкретный процесс.

        :param self: данный менеджер.
        :param level: заданный уровень.
        """

        # Уровень указан.
        if level:
            if level not in LEVELS:
                raise ValueError(f'Wrong level requested for parser processor, available: {LEVELS}!')
            process = self._processes.get(level, None)
            # Если процесс с заданным уровнем найден.
            if process is not None:
                process.kill()
                self._processes[level] = None
        else:
            # Завершение процессов всех уровней.
            for level in LEVELS:
                self.kill(level)
