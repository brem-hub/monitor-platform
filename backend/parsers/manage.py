import os

import click
from aiohttp import web
from backend.parsers.core import parser_table
from backend.parsers.application.application import Application
from backend.infra.base_server.config.config import parse_host
from backend.infra.base_server.config.config import GLOBAL_CONTEXT, get_version
from backend.infra.base_server.commands.root import root_cmd


GLOBAL_CONTEXT.Application = Application(config=parse_host('localhost:1234'),
                                         monitor_config=parse_host('localhost:1234'),
                                         redis_config=parse_host('localhost:1234'),
                                         parser_class=parser_table['basketshop']
                                         )
GLOBAL_CONTEXT.ApplicationName = "Parsers"
GLOBAL_CONTEXT.ApplicationVersion = get_version(os.getenv('VERSION_FILE'))


@click.command(name='runserver')
@click.option('--redis', default='localhost:6379', help='States the port for existing redis_cfg server. Default: 6379',
              required=False)
@click.option('--host', default='localhost:9000', help='States the port for future parser server. Default: 9000',
              required=False)
@click.option('--type', help='Type of parser', type=click.Choice(['lamoda', 'basketshop', 'basketshop-test', 'sneakerhead']), required=True)
@click.option('--monitor', default='localhost:8999', help='Monitor port', required=False)
@click.option('--hot-swagger', is_flag=True, default=False, help='Reload swagger at startup', required=False)
@click.option('--swagger-path', default='./package/swagger.json', help='Swagger specification path', required=False)
def runserver(redis: str, host: str, type: str, monitor: str, hot_swagger: bool, swagger_path: str):
    """Функция запуска сервера парсера с заданными параметрами.

    :param redis: конфигурация Redis.
    :param host: хост запуска сервера.
    :param type: тип запускаемого парсера.
    :param monitor: конфигурация монитора.
    :param hot_swagger: обновлять Swagger spec файл, при запуске сервера.
    :param swagger_path: путь до Swagger spec файла.
    """

    redis_config = parse_host(redis)
    parser_config = parse_host(host)
    monitor_config = parse_host(monitor)
    server_config = parse_host(host)

    parser_class = parser_table[type]
    app = Application(
        config=parser_config,
        monitor_config=monitor_config,
        redis_config=redis_config,
        parser_class=parser_class,
        hot_swagger=hot_swagger,
        swagger_path=swagger_path
    )

    app.pre_run()
    web.run_app(app, host=server_config.host, port=server_config.port)


root_cmd.add_command(runserver)

# Непосредственный запуск сервера парсера.
if __name__ == '__main__':
    root_cmd()
