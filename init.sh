#!/bin/bash

# ======  Add root path to the PYTHONPATH ======
echo "Adding project root path: $PWD to PYTHONPATH"

export PYTHONPATH=$PYTHONPATH:$(echo $PWD)
