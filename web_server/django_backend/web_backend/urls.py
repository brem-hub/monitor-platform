from django.urls import path
from .views import *

urlpatterns = [
    path('ping', pingHandler),
    path('userMonitorManager', UserMonitorManager.as_view()),
    path('getProducts', GetProducts.as_view()),
    path('parserManager', ParserManager.as_view()),
    path('productManager', ProductManager.as_view())
]
