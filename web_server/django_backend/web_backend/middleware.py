import logging
import re


class LoggingMiddleware:
    BLACK_LIST = ['/ping', '/static/.*', '/api/token/.*', '/swagger/.*', r'/swagger\.*']

    def __init__(self, get_response):
        self.get_response = get_response
        self.logger = logging.getLogger('mw')

        self.matchers = [re.compile(matcher)
                         for matcher in LoggingMiddleware.BLACK_LIST]

    def parse_black_list(self, path: str) -> bool:
        for matcher in self.matchers:
            if matcher.match(path) is not None:
                return True
        return False

    def __call__(self, request):

        blacked = self.parse_black_list(request.path)
        if not blacked:
            self.logger.debug('[request start] [%s] [%s] [%s]',
                              request.method, request.path, request.body)

        response = self.get_response(request)

        if not blacked:
            self.logger.debug('[request finished] [%s] [%s]',
                              response.status_code, response.content)

        return response
