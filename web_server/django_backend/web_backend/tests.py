from django.test import TestCase
from .views import pingHandler


# Create your tests here.
class PingTestCase(TestCase):
    """Бесполезный тест, но без него джанго тесты ругаются, что тестов вообще
    нет Перед тем как запустить тесты, не забудьте поднять базу (docker-
    compose)"""

    def test_ping_handler(self):
        response = pingHandler(None)
        self.assertEqual(response.content, b'pong')
