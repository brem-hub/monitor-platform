import logging
import requests
import json

from .config import *
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework import permissions
from backend_core.api_response import *


def pingHandler(request):
    """
    Хэндлер для проверки состояния Джанго приложения.

    :param request: входящий запрос - не используется в хэндлере
    :return: ответ "pong"
    """
    return HttpResponse('pong')


class ParserManager(APIView):
    logger = logging.getLogger('api')
    permission_classes = [permissions.IsAuthenticated]

    PATH_BASE_MONITOR_MANAGER_API = os.getenv("BASE_MONITOR_MANAGER_API", "http://0.0.0.0:8000")
    PATH_MONITOR_MANAGER_API = PATH_BASE_MONITOR_MANAGER_API + "/v1/monitor"
    PATH_MONITOR_MANAGER_PARSERS_API = PATH_BASE_MONITOR_MANAGER_API + "/v1/monitor/parsers/full"
    PATH_AVAILABLE_PARSERS = PATH_BASE_MONITOR_MANAGER_API + "/v1/parsers/available"
    PATH_BASE_TOKENIZATOR_API = os.getenv("BASE_TOKENIZATOR_API", "http://0.0.0.0:8080/tokenizator")
    PATA_BASE_MONITOR_API = os.getenv("BASE_MONITOR_API", "http://localhost:8999") + "/parsers"

    def get(self, request):
        """
        Позволяет получать активные парсеры пользователя.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        data = {'username': username}

        tokenizator_response = requests.get(ParserManager.PATH_BASE_TOKENIZATOR_API, json=data).json()
        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])
        monitor = tokenizator_response['token']['monitor']

        monitor_manager_response = requests.get(ParserManager.PATH_AVAILABLE_PARSERS).json()

        if monitor_manager_response['status'] >= 400:
            self.logger.error(monitor_manager_response['message'])
            return DjangoResponse(message=monitor_manager_response['message'],
                                  status=monitor_manager_response['status'])

        parsers_enable = {}

        for parser in monitor_manager_response['data']['parsers']:
            parsers_enable[parser['name']] = False

        path = ParserManager.PATA_BASE_MONITOR_API.format(monitor=monitor)

        monitor_response = requests.get(path)
        if monitor_response.status_code >= 400:
            self.logger.error(f'Error with getting info from the monitor - {monitor}')
            return DjangoResponse(message=f'Error with getting info from the monitor - {monitor}',
                                  status=monitor_response.status_code)

        data = monitor_response.json()
        for parser in data.items():
            parsers_enable[parser[0]] = True

        self.logger.debug(f'Got enable parsers for the user - {username}')
        return DjangoResponse(message=f'Got enable parsers for the user - {username}',
                              token={'parsers': parsers_enable},
                              status=status.HTTP_200_OK)

    def put(self, request):
        """
        Позволяет добавлять парсеры в монитор.

        :param request: json с обязательным параметром parsers.
        :return: DjangoResponse.
        """
        data = json.loads(request.body)

        parsers = data

        self.logger.debug(f'got parsers: {parsers}')

        username = request.user.username
        data = {'username': username}

        tokenizator_response = requests.get(ParserManager.PATH_BASE_TOKENIZATOR_API, json=data).json()
        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        monitor = tokenizator_response['token']['monitor']
        data = {'monitor': monitor, 'parsers': parsers}
        monitor_response = requests.put(ParserManager.PATH_MONITOR_MANAGER_PARSERS_API, json=data).json()

        if monitor_response['status'] >= 400:
            self.logger.error(monitor_response['message'])
            return DjangoResponse(message=monitor_response['message'],
                                  token=monitor_response['data'],
                                  status=monitor_response['status'])

        self.logger.debug(f"Parsers - {parsers} were added to the monitor - {monitor}.")
        return DjangoResponse(message=f"Parsers - {parsers} were added to the monitor - {monitor}.",
                              token={"parsers": parsers,
                                     "monitor": monitor},
                              status=status.HTTP_200_OK)


class UserMonitorManager(APIView):
    logger = logging.getLogger('api')
    permission_classes = [permissions.IsAuthenticated]

    PATH_BASE_MONITOR_MANAGER_API = os.getenv("BASE_MONITOR_MANAGER_API", "http://0.0.0.0:8000")
    PATH_MONITOR_MANAGER_API = PATH_BASE_MONITOR_MANAGER_API + "/v1/monitor"
    PATH_BASE_TOKENIZATOR_API = os.getenv("BASE_TOKENIZATOR_API", "http://0.0.0.0:8080/tokenizator")

    def get(self, request):
        """
        Позволяет получить monitor пользователя из токенизатора.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        data = {'username': username}

        tokenizator_response = requests.get(UserMonitorManager.PATH_BASE_TOKENIZATOR_API, json=data).json()

        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        self.logger.debug(tokenizator_response['message'])
        return DjangoResponse(message=tokenizator_response['message'],
                              token=tokenizator_response['token'],
                              status=tokenizator_response['status'])

    def post(self, request):
        """
        Позволяет создать monitor для пользователя в токенизаторе.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        params = {"username": username}

        monitor_response = requests.post(UserMonitorManager.PATH_MONITOR_MANAGER_API, json=params).json()

        if monitor_response['status'] >= 400:
            self.logger.error(monitor_response['message'])
            return DjangoResponse(message=monitor_response['message'],
                                  status=monitor_response['status'])

        self.logger.debug(f"A monitor - \"{monitor_response}\" was added to the user \"{username}\"")
        return DjangoResponse(message=monitor_response['message'],
                              token=monitor_response['data'],
                              status=monitor_response['status'])

    def delete(self, request):
        """
        Позволяет удалить монитор у пользователя.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        data = {'username': username}

        monitor_response = requests.delete(UserMonitorManager.PATH_MONITOR_MANAGER_API, json=data).json()

        if monitor_response['status'] >= 400:
            self.logger.error(monitor_response['message'])
            return DjangoResponse(message=monitor_response['message'],
                                  status=monitor_response['status'])

        self.logger.debug(f"The user - \"{username}\" was removed from tokenizator.")
        return DjangoResponse(message=monitor_response['message'],
                              token=monitor_response['data'],
                              status=monitor_response['status'])


class GetProducts(APIView):
    logger = logging.getLogger('api')
    permission_classes = [permissions.IsAuthenticated]
    PATH_BASE_MONITOR_API = os.getenv("BASE_MONITOR_API", "http://localhost:8999")
    PATH_BASE_TOKENIZATOR_API = os.getenv("BASE_TOKENIZATOR_API", "http://0.0.0.0:8080/tokenizator")
    PATH_ALL_PRODUCTS = PATH_BASE_MONITOR_API + "/products"

    def get(self, request):
        """
        Позволяет получить все продукты пользователя.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        params = {'username': username}

        tokenizator_response = requests.get(GetProducts.PATH_BASE_TOKENIZATOR_API, json=params).json()
        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        monitor = tokenizator_response['token']['monitor']

        path = GetProducts.PATH_ALL_PRODUCTS.format(monitor=monitor)
        monitor_response = requests.get(path)

        if monitor_response.status_code >= 400:
            self.logger.error(f"Could not get data from the monitor - \"{monitor}\".")
            return DjangoResponse(message=f"Could not get data from the monitor - \"{monitor}\".",
                                  status=status.HTTP_400_BAD_REQUEST)

        products = monitor_response.json()
        self.logger.debug(f"Got all products for the user - \"{username}\".")
        return DjangoResponse(message=f"Products were getted for the user - \"{username}\".",
                              monitor=monitor,
                              token=products,
                              status=status.HTTP_200_OK)


class ProductManager(APIView):
    logger = logging.getLogger('api')
    permission_classes = [permissions.IsAuthenticated]

    PATH_BASE_MONITOR_API = os.getenv("BASE_MONITOR_API", "http://localhost:8999")
    PATH_GET_ALL_CACHE = PATH_BASE_MONITOR_API + "/cache"
    PATH_BASE_CACHE_API = PATH_BASE_MONITOR_API + "/cache/{parser_signature}"
    PATH_ADD_CACHE = PATH_BASE_CACHE_API + "?level={level}"
    PATH_BASE_TOKENIZATOR_API = os.getenv("BASE_TOKENIZATOR_API", "http://0.0.0.0:8080/tokenizator")


    def post(self, request):
        """
        Позволяет добавлять продукт в монитор.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        params = {'username': username}

        tokenizator_response = requests.get(ProductManager.PATH_BASE_TOKENIZATOR_API, json=params).json()

        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        monitor = tokenizator_response['token']['monitor']

        data = json.loads(request.body)
        parser_signature = data['params']['parser_signature']
        level = data['params']['level']
        product_info = data['product_info']

        path = ProductManager.PATH_ADD_CACHE.format(monitor=monitor, parser_signature=parser_signature, level=level)
        monitor_response = requests.post(path, json=product_info)

        if monitor_response.status_code >= 400:
            self.logger.error(monitor_response.content)
            return DjangoResponse(message=monitor_response.content,
                                  status=monitor_response.status_code)

        self.logger.debug(f"The product was added to the user - {username}.")
        return DjangoResponse(message=monitor_response.content,
                              token=product_info,
                              status=monitor_response.status_code)

    def delete(self, request):
        """
        Позволяет удалить продукт из монитора.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        params = {'username': username}

        tokenizator_response = requests.get(ProductManager.PATH_BASE_TOKENIZATOR_API, json=params).json()

        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        monitor = tokenizator_response['token']['monitor']

        data = json.loads(request.body)
        parser_signature = data['parser_signature']
        params = {'id': data['id']}

        path = ProductManager.PATH_BASE_CACHE_API.format(monitor=monitor, parser_signature=parser_signature)
        monitor_response = requests.delete(path, json=params)

        if monitor_response.status_code >= 400:
            self.logger.error(monitor_response.content)
            return DjangoResponse(message=monitor_response.content,
                                  status=monitor_response.status_code)

        self.logger.debug(f"The product was deleted from the user - {username}.")
        return DjangoResponse(message=monitor_response.content,
                              token=data,
                              status=monitor_response.status_code)

    def put(self, request):
        """
        Позволяет переместить ProductRequest на новый уровень кэша.

        :param request:
        :return: DjangoResponse.
        """
        username = request.user.username
        params = {'username': username}

        tokenizator_response = requests.get(ProductManager.PATH_BASE_TOKENIZATOR_API, json=params).json()

        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        monitor = tokenizator_response['token']['monitor']

        data = json.loads(request.body)
        parser_signature = data['parser_signature']
        params = {'id': data['id'], 'to_level': data['to_level']}

        path = ProductManager.PATH_BASE_CACHE_API.format(monitor=monitor, parser_signature=parser_signature)
        monitor_response = requests.put(path, json=params)

        if monitor_response.status_code >= 400:
            self.logger.error(monitor_response.content)
            return DjangoResponse(message=monitor_response.content,
                                  status=monitor_response.status_code)

        self.logger.debug(f"The product was changed for the user - {username}.")
        return DjangoResponse(message=monitor_response.content,

                              token=data,
                              status=monitor_response.status_code)

    def get(self, request):
        """
        Позволяет получить информацию о ProductRequest'ах монитора или конкретного парсера.

        :param request:
        :return: DjangoResponse.
        """
        
        username = request.user.username
        params = {'username': username}

        tokenizator_response = requests.get(ProductManager.PATH_BASE_TOKENIZATOR_API, json=params).json()

        if tokenizator_response['status'] >= 400:
            self.logger.error(tokenizator_response['message'])
            return DjangoResponse(message=tokenizator_response['message'],
                                  status=tokenizator_response['status'])

        monitor = tokenizator_response['token']['monitor']

        parser_signature = request.GET.get('parser_signature', None)
        if parser_signature is None:
            path = ProductManager.PATH_GET_ALL_CACHE.format(monitor=monitor)
        else:
            path = ProductManager.PATH_BASE_CACHE_API.format(monitor=monitor, parser_signature=parser_signature)


        monitor_response = requests.get(path)
        response_data = monitor_response.json()
        if monitor_response.status_code >= 400:
            self.logger.error(monitor_response.content)
            return DjangoResponse(message=monitor_response.content,
                                  status=monitor_response.status_code)

        self.logger.debug(f"The product was changed for the user - {username}.")
        return DjangoResponse(message="ok",
                            token=response_data,
                              status=monitor_response.status_code)
