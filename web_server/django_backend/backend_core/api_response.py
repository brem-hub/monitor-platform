from rest_framework.response import Response
from rest_framework import status


class NotFoundResponse(Response):
    def __init__(self, field):
        data = {
            'missing_field': field,
            'error': 'REQUIRED FIELD IS MISSING'
        }
        super().__init__(data=data, status=status.HTTP_404_NOT_FOUND)


class BadRequestResponse(Response):
    def __init__(self, error_message, **kwargs):
        data = {
            'error': error_message,
            'params': kwargs
        }
        super().__init__(data=data, status=status.HTTP_400_BAD_REQUEST)


class InternalErrorResponse(Response):
    def __init__(self, error_message, **kwargs):
        data = {
            'error': error_message,
            'params': kwargs
        }
        super().__init__(data=data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class DjangoResponse(Response):
    def __init__(self, message, status, token=None, **kwargs):
        data = {
            'message': message,
            'status': status,
            'token': token,
            'params': kwargs
        }
        super().__init__(data=data, status=status)
