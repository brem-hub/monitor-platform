from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from .views import ObtainTokenPairView, CreateCustomUser, GetCustomUser

urlpatterns = [
    path('user/create/', CreateCustomUser.as_view(), name='create_user'),
    path('user/me/', GetCustomUser.as_view(), name='user'),
    path('token/obtain/', ObtainTokenPairView.as_view(), name='token_create'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
