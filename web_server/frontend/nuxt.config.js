export default {
    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'MonitorBoi',
        htmlAttrs: {
            lang: 'ru'
        },
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''},
            {name: 'format-detection', content: 'telephone=no'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        `~/assets/styles/main`,
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/proxy',
        '@nuxtjs/auth-next'
    ],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {},

    axios: {
        proxy: true,
        headers:{
          'X-Accel-Buffering' : 'no',
      }
    },

    proxy: {
        // TODO: return to process.env.API_HOST
        // '/api/v1': {target: process.env.API_HOST, pathRewrite: {'^/api/v1': ''}},
      '/api/v1': {target: 'http://localhost:8000/api/', pathRewrite: {'^/api/v1': ''}},
      '/api/v2': {target: 'http://localhost:8000/', pathRewrite: {'^/api/v2': ''}},
      // '/api/mon': {target: 'http://localhost:80/', pathRewrite: {'^/api/mon': ''}},
/*        '/api/v1': {target: 'http://0.0.0.0:8002/api/', pathRewrite: {'^/api/v1': ''}},
        '/api/v2': {target: 'http://0.0.0.0:8002/', pathRewrite: {'^/api/v2': ''}},
        '/api/mon': {target: 'http://0.0.0.0:8999/', pathRewrite: {'^/api/mon': ''}},*/
        '/api/sse': {target: 'http://gateway/v1/', pathRewrite: {'^/api/sse': ''}},
    },

    auth: {
        localStorage: false,
        redirect: {
            home: '/home',
        },
        strategies: {
            local: {
                scheme: "refresh",
                token: {
                    property: 'access',
                    global: true,
                    maxAge: 1800,
                    type: 'JWT'
                },
                refreshToken: {
                    property: 'refresh',
                    data: 'refresh',
                    maxAge: 60 * 60 * 24 * 30,
                    //tokenRequired: true,
                },
                user: {
                    property: 'user'
                },
                endpoints: {
                    login: {url: '/api/v1/token/obtain/', method: 'post'},
                    // login: {url: 'http://localhost:8000/api/token/obtain/', method: 'post'},
                    logout: false,
                    user: {url: '/api/v1/user/me/', method: 'get'},
                    // user: {url: 'http://localhost:8000/api/user/me/', method: 'get'},
                    //user: false,
                    // refresh: {url: 'http://localhost:8000/api/token/refresh/', method: 'post'},
                    refresh:  {url: '/api/v1/token/refresh/', method: 'post'},
                },
            }
        }
    },

}
