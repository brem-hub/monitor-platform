import Vue from "vue";

export const state = () => ({
  counter: 0,
  monitor: "",
  modals: {
    ProductDescriptionModal: {
      status: false,
      parser: "",
      cache: "",
      data: {}
    },
    showAddProductModal: {
      status: false,
      parser: "",
      cache: "",
      data: {}
    },
    showAddParserModal: {
      status: false,
      parser: "",
      cache: "",
      data: {}
    },
    showDeleteParserModal: {
      status: false,
      parser: "",
      cache: "",
      data: {}
    },
    showContextMenuModal: {
      status: false,
      parser: "",
      cache: "",
      toCache: "",
      data: {}
    },
  },
  avaibleParsers: [
    "basketshop_test",
    "lamoda"
  ],
  parsers: {
    basketshop_test: {
      hide: true,
      parser_signature: "basketshop_test",
      content: {
        L1: [{
          uid: 0,
          brand: "",
          name: "",
          sku: "",
          image_link: "",
          status: 0,
          sizes: [
            "0",
          ],
          link: "",
          price: 0
        }
        ],
        L2: [{
          uid: 0,
          brand: "",
          name: "",
          sku: "",
          image_link: "",
          status: 0,
          sizes: [
            "0",
          ],
          link: "",
          price: 0
        }
        ],
        L3: [{
          uid: 0,
          brand: "",
          name: "",
          sku: "",
          image_link: "",
          status: 0,
          sizes: [
            "0",
          ],
          link: "",
          price: 0
        }]
      }
    },
    lamoda: {
      hide: true,
      parser_signature: "lamoda",
      content: {L1: [], L2: [], L3: []}
    },
    sneakerhead: {
      hide: true,
      parser_signature: "sneakerhead",
      content: {L1: [], L2: [], L3: []}
    },

    /*basketshop_test: {
      name: "basketshop_test",
      caches: {
        L1: [/!*{
                    name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
                    price: 22990,
                    status: 1,
                    sku: "sku",
                    resource: "bask",
                    image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
                },*!/ {
          name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
          brand: "basketshop",
          price: 22990,
          sku: "sku",
          status: 1,
          link: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
          image_link: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
          sizes: [37, 38, 38.5, 39.5],
          uid: 286378362832684,
        }
        ], L2: [], L3: []
      }
    },
    lamoda: {
      name: "lamoda",
      caches: {L1: [], L2: [], L3: []}
    },*/
    /*L1: [{
        name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
        price: 22990,
        status: 1,
        sku: "sku",
        resource: "bask",
        image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
    }],
    L2: [],
    L3: [],
    counter: 3,*/
  }

});


export const mutations = {
  changeAddProductModal(state, {status, parser, cache}) {
    state.modals.showAddProductModal.status = status;
    state.modals.showAddProductModal.parser = parser;
    state.modals.showAddProductModal.cache = cache;
  },
  AddDataToAddProductModal(state, data) {
    state.modals.showAddProductModal.data = data;
  },
  changeProductDescriptionModal(state, {status, parser, cache, data}) {
    console.log(data);
    state.modals.ProductDescriptionModal.status = status;
    state.modals.ProductDescriptionModal.parser = parser;
    state.modals.ProductDescriptionModal.cache = cache;
    state.modals.ProductDescriptionModal.data = data;
  },
  changeAddParserModal(state, {status}){
    state.modals.showAddParserModal.status = status;
  },
  changeDeleteParserModal(state, {status}){
    state.modals.showDeleteParserModal.status = status;
  },
  changeContextMenuModal(state, {status, parser, cache, toCache, data}) {
    console.log("[changeContextMenu]", status, parser, cache, toCache, data);


    if (status !== undefined) {
      state.modals.showContextMenuModal.status = status;
    }
    if (parser !== undefined) {
      state.modals.showContextMenuModal.parser = parser;
    }
    if (cache !== undefined) {
      state.modals.showContextMenuModal.cache = cache;
    }
    if (toCache !== undefined) {
      state.modals.showContextMenuModal.toCache = toCache;
    }
    if (data !== undefined) {
      state.modals.showContextMenuModal.data = data;
    }

    console.log("[changeContextMenu state]",
      state.modals.showContextMenuModal.status,
      state.modals.showContextMenuModal.parser,
      state.modals.showContextMenuModal.cache,
      state.modals.showContextMenuModal.toCache,
      state.modals.showContextMenuModal.data);
  },

  initStore(state, payload) {
    state.avaibleParsers = [];
    console.log("PAYLOAD", payload);
    for (let [id, parser] of Object.entries(payload)) {
      console.log("PARSER:  ", parser);
      state.parsers[parser.parser_signature] = parser;
      state.avaibleParsers.push(parser.parser_signature);
    }
    /*state.avaibleParsers = [];
    console.log("PAYLOAD", payload);
    for (let [id, parser] of Object.entries(payload)) {
      console.log("PARSER:  ", parser);
      for (const [cache, data] of Object.entries(state.parsers[parser.parser_signature].content)) {
        while (data.length > 0){
          data.pop();
        }
      }
      for (let [cache, data] of Object.entries(parser.content)) {
        console.log(">>>>>PARSER:  ", data);
        for (let [id, item] of Object.entries( data)){

          //state.parsers[parser.parser_signature].content[cache].push(item);
          Vue.set(state.parsers[parser.parser_signature].content[cache], id,  item)
        }
      }
      state.parsers[parser.parser_signature].hide = false;
      state.avaibleParsers.push(parser.parser_signature);
    }*/
  },

  setMonitor(state, payload) {
    state.monitor = payload
  },

  addProduct(state, {parser, cache, item}) {
    console.log("[mutation] [addProduct] ", parser, cache, item);
    // state.parsers[parser].caches[cache].push(item);
    state.parsers[parser].content[cache].push(item);
  },

  removeProduct(state, {parser, cache, item}) {
    console.log("[mutation] [removeProduct] ", parser, cache, item);

    state.parsers[parser].content[cache] = state.parsers[parser].content[cache]
      .filter(el => el.uid !== item.uid);
    console.log("RES: ", state.parsers[parser].content[cache]);
  },


  updateProduct(state, {parser, item}) {
    let index;
    // for (let [cache, data] of Object.entries(parsers['basketshop_test'].caches)) {
    for (let [cache, data] of Object.entries(parsers['basketshop_test'].content)) {
      index = data.findIndex(el => el.sku === item.sku);
      if (index > -1) {
        data[index] = item;
        break;
      }
    }
  },
  /*moveProduct(state, {parser, cache, toCache, id}) {
      console.log("[mutation] [moveProduct] ", parser, cache, id);
      state.parsers[parser].caches[cache];
  }*/
  updateProductByIndex(state, {parser, cache, index, product}) {
    console.log("---- ---- ----[UPDATE PRODUCT BY INDEX] ", parser, cache, index, product)
    console.log("---- ---- ----[UPDATE PRODUCT BY INDEX STATE] ", state.parsers[parser].content[cache][index])
    state.parsers[parser].content[cache][index] = product;
    Vue.set(state.parsers[parser.parser_signature].content[cache], index, product)
    console.log("---- ---- ----[UPDATE PRODUCT BY INDEX UPDATED] ", state.parsers[parser].content[cache][index])
  },
  removeProductByUID(state, {parser, cache, index}) {
    state.parsers[parser].content[cache].splice(index, 1);
  },

};

/*

[{name: "basketshop", caches:{L1:[], L2:[], L3:[]} ,
{name: "lamoda", caches:{L1:[], L2:[], L3:[]} ]
*/

export const actions = {
  async moveProductRequest({commit}, {parser, uid, cache, toCache, item}) {
    await this.$axios.$put(`api/v2/productManager`, {
      parser_signature: parser,
      id: uid,
      to_level: `L${toCache}`
    }).then(res => {
      console.log("***[movePoductRequest]")
      console.log(res);
      if (res.message == "True") {
        /*commit(`addProduct`,
          {
            parser: parser,
            cache: `L${toCache}`,
            item: item
          }
        );
        commit(`removeProduct`,
          {
            parser: parser,
            cache: `L${cache}`,
            item: item
          }
        );*/
      }
    });
  },
  addProduct({commit}, payload) {
    console.log("[action] [addProduct] ", payload);
    commit('addProduct', payload);
  },
  removeProduct({commit}, payload) {
    console.log("[action] [removeProduct] ", payload);
    commit('removeProduct', payload);
  },
  removeProductByUID({state, commit}, payload) {
    let flag = false;
    for (let [parser_signature, parser_data] of Object.entries(state.parsers)) {
      for (let [cache, data] of Object.entries(parser_data.content)) {
        console.log("DATA", data)
        let index = data.findIndex(el => {
          return el.uid === payload.product_request_id
        });
        if (index > -1) {
          commit('removeProduct',
            {
              parser: parser_signature,
              cache: cache,
              item: {uid: payload.product_request_id}
            })
          /*          commit('removeProductByUID',
                      {
                        parser: parser_signature,
                        cache: cache,
                        index: index
                      }
                    );*/
          flag = true;
          break;
        }
      }
      if (flag) {
        break;
      }
    }

  },
  moveProduct({commit}, {parser, cache, toCache, item}) {
    console.log("[action] [moveProduct]");
    commit('addProduct', {parser, cache: toCache, item});
    commit('removeProduct', {parser, cache, item});
  },
  moveProductSSE({state, commit}, payload) {
    console.log("[action] [moveProductSSE]");
    let item;
    let flag = false;
    let parser_;
    let cache_;
    let index_;
    let uid = payload.product_request_id;
    let to_level = payload.to_level;

    for (let [parser_signature, parser_data] of Object.entries(state.parsers)) {
      for (let [cache, data] of Object.entries(parser_data.content)) {
        let index = data.findIndex(el => el.uid === uid);
        if (index > -1) {
          console.log("[action] [moveProductSSE] FOUND!!");
          item = data[index];
          parser_ = parser_signature;
          cache_ = cache;
          index_ = index;
          flag = true;
          break;
        }
      }
      if (flag) {
        break;
      }
    }
    console.log("[action] [moveProductSSE] FOUND!!", parser_, cache_, item);
    commit('removeProduct', {parser: parser_, cache: cache_, item: {uid: uid}});
    commit('addProduct', {parser: parser_, cache: to_level, item});

  },

  updateProductSSE({state, commit}, {product, product_request}) {
    let flag = false;
    console.log("----[UPDATE SSE] ", product)
    for (let [parser_signature, parser_data] of Object.entries(state.parsers)) {
      for (let [cache, data] of Object.entries(parser_data.content)) {
        // console.log("---- ----[UPDATE SSE DATA CACHE] ", data)
        let index = data.findIndex(el => el.uid === product.uid);
        if (index > -1) {
          console.log("---- ----[UPDATE SSE DATA CACHE] FOUND!!!!", data)
          commit('removeProduct', {
            parser: parser_signature,
            cache: cache,
            item: product
          })
          commit('addProduct', {
            parser: parser_signature,
            cache: cache,
            item: product
          })
          /*commit('updateProductByIndex',
            {
              parser: parser_signature,
              cache: cache,
              index: index,
              product: product
            }
          );*/
          flag = true;
          break;
        }
      }
      if (flag) {
        break;
      }
    }
  },
  updateProduct({commit}, payload) {
    console.log("[action] [updateProduct]");
    commit('updateProduct', payload);
  },

  initStore({commit}, payload) {
    commit('initStore', payload);
  },

  async updateStore({commit}) {
    console.log("[vuex action][updateStore] run")
    await this.$axios.$get('api/v2/getProducts')
      .then(res => {
        commit('setMonitor', res.params.monitor);
        commit('initStore', res.token);
        console.log("[vuex action][updateStore] done")
      });
    console.log("[vuex action][updateStore] END OF FUNC")
  },
};


/*
export const state = () => ({
    L1: [{
        name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
        price: 22990,
        status: 1,
        sku: "sku",
        resource: "bask",
        image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
    }],
    L2: [],
    L3: [],
    counter: 3,
})


export const mutations = {
    addNewProductL1(state) {
        state.L1.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001${state.counter++})`,
                price: 1111,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        )
    },
    addNewProductL2(state) {
        state.L2.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-002${state.counter++})`,
                price: 2222,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        )
    },
    addNewProductL3(state) {
        state.L3.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-003${state.counter++})`,
                price: 3333,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        )
    },
    addProductL1(state, item) {
        state.L1.push(item)
    },
    addProductL2(state, item) {
        state.L2.push(item)
    },
    addProductL3(state, item) {
        state.L3.push(item)
    },
    removeProductL1(state, item) {
        console.log("L1 remove", item)
        console.log("EQ: ", item.name === item.name)
        state.L1 = state.L1.filter(el => el.name !== item.name);

    },
    removeProductL2(state, item) {
        state.L2 = state.L2.filter(el => el.name !== item.name);
    },
    removeProductL3(state, item) {
        state.L3 = state.L3.filter(el => el.name !== item.name);
    },
    initStore(state, payload) {
        state.L1 = payload.L1;
        state.L2 = payload.L2;
        state.L3 = payload.L3;
    }
}

export const actions = {
    addProductL1({commit}, item) {
        commit('addProductL1', item)
    },
    addProductL2({commit}, item) {
        commit('addProductL2', item)
    },
    addProductL3({commit}, item) {
        commit('addProductL3', item)
    },
    addNewProductL1({commit}) {
        commit('addNewProductL1')
    },
    addNewProductL2({commit}) {
        commit('addNewProductL2')
    },
    addNewProductL3({commit}) {
        commit('addNewProductL3')
    },

    removeProductL1({commit}, item) {
        commit('removeProductL1', item)

    },
    removeProductL2({commit}, item) {
        commit('removeProductL2', item)
        console.log("L2 remove", item)
    },
    removeProductL3({commit}, item) {
        commit('removeProductL3', item)
        console.log("L3 remove", item)
    },

    initStore({commit}, payload) {
        commit('initStore', payload)
    }
}

*/


/////////////////


/*
import addProductModal from "../../../components/UI/Modals/AddProductModal";

export const state = () => ({
    counter: 0,
    modals: {
        ProductDescriptionModal: {
            status: false,
            parser: "",
            cache: "",
            data: {}
        },
        showAddProductModal: {
            status: false,
            parser: "",
            cache: "",
            data: {}
        },
    },
    avaibleParsers: [
        "basketshop",
        "lamoda"
    ],
    parsers: {
        basketshop: {
            name: "basketshop",
            caches: {
                L1: [/!*{
                    name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
                    price: 22990,
                    status: 1,
                    sku: "sku",
                    resource: "bask",
                    image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
                },*!/ {
                    name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
                    brand: "basketshop",
                    price: 22990,
                    sku: "sku",
                    status: 1,
                    link: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
                    image_link: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
                    sizes: [37, 38, 38.5, 39.5],
                }
                ], L2: [], L3: []
            }
        },
        lamoda: {
            name: "lamoda",
            caches: {L1: [], L2: [], L3: []}
        },
        /!*L1: [{
            name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
            price: 22990,
            status: 1,
            sku: "sku",
            resource: "bask",
            image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
        }],
        L2: [],
        L3: [],
        counter: 3,*!/
    }

});


export const mutations = {
    changeAddProductModal(state, {status, parser, cache}) {
        state.modals.showAddProductModal.status = status;
        state.modals.showAddProductModal.parser = parser;
        state.modals.showAddProductModal.cache = cache;
    },
    changeProductDescriptionModal(state, {status, parser, cache, data}) {
        console.log(data);
        state.modals.ProductDescriptionModal.status = status;
        state.modals.ProductDescriptionModal.parser = parser;
        state.modals.ProductDescriptionModal.cache = cache;
        state.modals.ProductDescriptionModal.data = data;
    },
    AddDataToAddProductModal(state, data) {
        state.modals.showAddProductModal.data = data;
    },
    addNewProductL1(state, parser) {
        state.parsers[parser].caches.L1.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001${state.counter++})`,
                price: 1111,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        );
    },
    addNewProductL2(state, parser) {
        state.parsers[parser].caches.L2.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-002${state.counter++})`,
                price: 2222,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        );
    },
    addNewProductL3(state, parser) {
        state.parsers[parser].caches.L3.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-003${state.counter++})`,
                price: 3333,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        );
    },

    /!*addProduct(state, ){

    },*!/

    /!*addProductL1(state, {parser, item}) {
        console.log(item);
        state.parsers[parser].caches.L1.push(item);
    },
    addProductL2(state, {parser, item}) {
        console.log(item);
        state.parsers[parser].caches.L2.push(item);
    },
    addProductL3(state, {parser, item}) {
        console.log(item);
        state.parsers[parser].caches.L3.push(item);
    },*!/

    /!*    removeProductL1(state, {parser, item}) {
            console.log(item);
            state.parsers[parser].caches.L1 = state.parsers[parser].caches.L1.filter(el => el.name !== item.name);
        },
        removeProductL2(state, {parser, item}) {
            console.log(item);
            state.parsers[parser].caches.L2 = state.parsers[parser].caches.L2.filter(el => el.name !== item.name);
        },
        removeProductL3(state, {parser, item}) {
            console.log(item);
            state.parsers[parser].caches.L3 = state.parsers[parser].caches.L3.filter(el => el.name !== item.name);
        },*!/

    initStore(state, payload) {
        for (let parser in payload) {
            state.parsers[parser.name] = parser;
            state.avaibleParsers.push(parser.name);
        }
    },

    addProduct(state, {parser, cache, item}) {
        console.log("[mutation] [addProduct] ", parser, cache, item);
        state.parsers[parser].caches[cache].push(item);
    },
    removeProduct(state, {parser, cache, item}) {
        console.log("[mutation] [removeProduct] ", parser, cache, item);
        state.parsers[parser].caches[cache] = state.parsers[parser].caches[cache]
            .filter(el => el.name !== item.name);
    }
};

/!*

[{name: "basketshop", caches:{L1:[], L2:[], L3:[]} ,
{name: "lamoda", caches:{L1:[], L2:[], L3:[]} ]
*!/

export const actions = {
    /!*addProduct({commit}, {parser, cache, item}) {
        console.log("[action] [addProduct] ", parser, cache, item);
    },*!/
    addProduct({commit}, payload) {
        console.log("[action] [addProduct] ", payload);
        commit('addProduct', payload);
    },
    removeProduct({commit}, payload) {
        console.log("[action] [removeProduct] ", payload);
        commit('removeProduct', payload);
    },


    /!* addProductL1({commit}, {parser, item}) {
         console.log("L3 ADD", parser, item);
         commit('addProductL1', {parser, item});

     },
     addProductL2({commit}, {parser, item}) {
         console.log("L3 ADD", parser, item);
         commit('addProductL2', {parser, item});
     },
     addProductL3({commit}, {parser, item}) {
         console.log("L3 ADD", parser, item);
         commit('addProductL3', {parser, item});
     },*!/

    addNewProductL1({commit}, parser) {
        commit('addNewProductL1', parser);
    },
    addNewProductL2({commit}, parser) {
        commit('addNewProductL2', parser);
    },
    addNewProductL3({commit}, parser) {
        commit('addNewProductL3', parser);
    },

    /!* removeProductL1({commit}, {parser, item}) {
         commit('removeProductL1', {parser, item});
     },
     removeProductL2({commit}, {parser, item}) {
         commit('removeProductL2', {parser, item});
         console.log("L2 remove", parser, item);
     },
     removeProductL3({commit}, {parser, item}) {
         commit('removeProductL3', {parser, item});
         console.log("L3 remove", parser, item);
     },*!/

    initStore({commit}, payload) {
        commit('initStore', payload);
    }
};


/!*
export const state = () => ({
    L1: [{
        name: "Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001)",
        price: 22990,
        status: 1,
        sku: "sku",
        resource: "bask",
        image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
    }],
    L2: [],
    L3: [],
    counter: 3,
})


export const mutations = {
    addNewProductL1(state) {
        state.L1.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-001${state.counter++})`,
                price: 1111,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        )
    },
    addNewProductL2(state) {
        state.L2.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-002${state.counter++})`,
                price: 2222,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        )
    },
    addNewProductL3(state) {
        state.L3.push(
            {
                name: `Мужские баскетбольные кроссовки Jordan XXXVI (CZ2650-003${state.counter++})`,
                price: 3333,
                status: 1,
                sku: "sku",
                resource: "bask",
                image: "https://www.basketshop.ru/files/catalog/42173/CZ2650-001(5).JPG",
            }
        )
    },
    addProductL1(state, item) {
        state.L1.push(item)
    },
    addProductL2(state, item) {
        state.L2.push(item)
    },
    addProductL3(state, item) {
        state.L3.push(item)
    },
    removeProductL1(state, item) {
        console.log("L1 remove", item)
        console.log("EQ: ", item.name === item.name)
        state.L1 = state.L1.filter(el => el.name !== item.name);

    },
    removeProductL2(state, item) {
        state.L2 = state.L2.filter(el => el.name !== item.name);
    },
    removeProductL3(state, item) {
        state.L3 = state.L3.filter(el => el.name !== item.name);
    },
    initStore(state, payload) {
        state.L1 = payload.L1;
        state.L2 = payload.L2;
        state.L3 = payload.L3;
    }
}

export const actions = {
    addProductL1({commit}, item) {
        commit('addProductL1', item)
    },
    addProductL2({commit}, item) {
        commit('addProductL2', item)
    },
    addProductL3({commit}, item) {
        commit('addProductL3', item)
    },
    addNewProductL1({commit}) {
        commit('addNewProductL1')
    },
    addNewProductL2({commit}) {
        commit('addNewProductL2')
    },
    addNewProductL3({commit}) {
        commit('addNewProductL3')
    },

    removeProductL1({commit}, item) {
        commit('removeProductL1', item)

    },
    removeProductL2({commit}, item) {
        commit('removeProductL2', item)
        console.log("L2 remove", item)
    },
    removeProductL3({commit}, item) {
        commit('removeProductL3', item)
        console.log("L3 remove", item)
    },

    initStore({commit}, payload) {
        commit('initStore', payload)
    }
}

*!/

*/
